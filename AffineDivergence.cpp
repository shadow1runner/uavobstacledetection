#include "AffineDivergence.h"

#include <iostream>

using namespace hw;
using namespace std;
using namespace cv;

using Eigen::MatrixXd;

hw::BaseAffineDivergence::BaseAffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings)
    : _flowPatch(opticalFlow(cv::Rect(
    							cv::Point2i(max(origin.x-settings.DivergencePatchSize, 0), max(origin.y-settings.DivergencePatchSize, 0)),
							    cv::Point2i(min(origin.x+settings.DivergencePatchSize, opticalFlow.cols-1), min(origin.y+settings.DivergencePatchSize, opticalFlow.rows-1))
					        )))
    , _origin(origin)
	, _settings(settings)
{
}

void hw::BaseAffineDivergence::fillEvenRow(Eigen::MatrixXd& A, const int rowIndex, double x, double y)
{
	A(rowIndex,0) = 1.0;
	A(rowIndex,1) = x;
	A(rowIndex,2) = y;
	A(rowIndex,3) = 0;
	A(rowIndex,4) = 0;
	A(rowIndex,5) = 0;
}

void hw::BaseAffineDivergence::fillOddRow(Eigen::MatrixXd& A, const int rowIndex, double x, double y)
{
	A(rowIndex,0) = 0;
	A(rowIndex,1) = 0;
	A(rowIndex,2) = 0;
	A(rowIndex,3) = 1.0;
	A(rowIndex,4) = x;
	A(rowIndex,5) = y;
}


hw::AffineDivergence::AffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings)
    : BaseAffineDivergence(opticalFlow, origin, settings)
    , _row_distribution(0, _flowPatch.rows-1) // ranges are inclusive, cf. http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
    , _col_distribution(0, _flowPatch.cols-1)
{
}

double hw::AffineDivergence::getDivergence()
{
	auto numberOfVectors = _flowPatch.cols*_flowPatch.rows / 2;

	MatrixXd A(numberOfVectors*2, 6);
	MatrixXd b(numberOfVectors*2, 1);

	auto xCenter = _flowPatch.cols/2;
    auto yCenter = _flowPatch.rows/2;

	for(auto i=0;i<numberOfVectors;++i) {
        auto rndX = _col_distribution(_generator);
        auto rndY = _row_distribution(_generator);

        auto flow = _flowPatch.at<cv::Point2f>(rndY,rndX);
        b(2*i,0) = flow.x;
        b(2*i+1,0) = flow.y;

		fillEvenRow(A, 2*i, rndX-xCenter, rndY-yCenter);
		fillOddRow(A, 2*i+1, rndX-xCenter, rndY-yCenter);
    }

    // don't use auto with Eigen, always explicitly state the type!
    Eigen::VectorXd x(A.colPivHouseholderQr().solve(b));

    // Eigen::VectorXd sebastiansWrongVersion = ((A.transpose()*A).inverse()*A.transpose()) * b;
    // cout << endl << "Sebastian: " << sebastiansWrongVersion(1) + sebastiansWrongVersion(5) << endl; <- even though it is the same formula the results are still different than `AffineMotionModleTest.scala` shows; anyway, the above qr decomposition is correct, I've checked with Matlab :P

    return x(1) + x(5); // c2 + c6
}


hw::FullAffineDivergence::FullAffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings)
    : BaseAffineDivergence(opticalFlow, origin, settings)
{
}

double hw::FullAffineDivergence::getDivergence()
{
    auto numberOfVectors = _flowPatch.cols * _flowPatch.rows;

    MatrixXd A(numberOfVectors*2, 6);
	MatrixXd b(numberOfVectors*2, 1);

	auto xCenter = _flowPatch.cols/2;
    auto yCenter = _flowPatch.rows/2;

    for(auto y=0;y<_flowPatch.rows;++y) {
		for(auto x=0;x<_flowPatch.cols;++x) {
            auto i = y*_flowPatch.cols + x;
	        auto flow = _flowPatch.at<cv::Point2f>(y,x);
	        b(2*i,0) = flow.x;
	        b(2*i+1,0) = flow.y;

            fillEvenRow(A, 2*i, x-xCenter, y-yCenter);
            fillOddRow(A, 2*i+1, x-xCenter, y-yCenter);
	    }
    }

    // don't use auto with Eigen, always explicitly state the type!
    Eigen::VectorXd x(A.colPivHouseholderQr().solve(b));

    return x(1) + x(5); // c2 + c6
}
