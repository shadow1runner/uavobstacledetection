#ifndef OPTICALFLOW_AffineDivergence_H
#define OPTICALFLOW_AffineDivergence_H

#include <opencv2/core.hpp>
#include <random>
#include <Eigen>

#include "CollisionAvoidanceSettings.h"

namespace hw {
	class BaseAffineDivergence
    {
	protected:
        cv::Mat _flowPatch;
        const cv::Point2i& _origin;
        const CollisionAvoidanceSettings& _settings;

        void fillEvenRow(Eigen::MatrixXd& A, const int rowIndex, double x, double y);
        void fillOddRow(Eigen::MatrixXd& A, const int rowIndex, double x, double y);
	    
	public:
		BaseAffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings);
        virtual double getDivergence() = 0;
	};

	class AffineDivergence : public BaseAffineDivergence
	{
	private:
        // aso http://stackoverflow.com/a/10710875
        // std::mt19937 generator;            // takes per-frame avg 83,926 ms on ahead.avi (subsampling 4) with almost same results
        std::default_random_engine _generator; // takes per-frame avg 63,306 ms on ahead.avi (subsampling 4) with almost same results
        std::uniform_int_distribution<> _row_distribution;
        std::uniform_int_distribution<> _col_distribution;

	public:
		AffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings);
		double getDivergence();
	};

	class FullAffineDivergence : public BaseAffineDivergence
	{
	public:
		FullAffineDivergence(const cv::Mat& opticalFlow, const cv::Point2i& origin, CollisionAvoidanceSettings& settings);
		double getDivergence();
	};
}
#endif // OPTICALFLOW_AffineDivergence_H
