#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "BufferedFrameGrabber.h"
#include "config.h"
#include "OwnFlow.h"
#include "Converter.h"
#include "FocusOfExpansionDto.h"
#include "FramePersister.h"
#include "CollisionAvoidanceSettings.h"
#include "RoiBuilder.h"
#include "AffineDivergence.h"

#include "helper/AvgWatch.h"
#include "helper/Macros.h"
#include "helper/Displayer.h"
#include "helper/ConsoleDisplayer.h"
#include "helper/QtHelper.h"

#include <QThread>
#include <QApplication>
#include <QDebug>

using namespace std;
using namespace hw;

int main(int argc, char *argv[]) {
    const int SUBSAMPLE_AMOUNT = 8;

    auto fst = cv::imread("res/illustrator/go5_0.jpg");
    cv::resize(fst, fst, cv::Size(fst.cols / (2 * SUBSAMPLE_AMOUNT), fst.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(fst, fst, CV_BGR2GRAY);
    cv::equalizeHist(fst, fst);

    auto snd = cv::imread("res/illustrator/go5_1.jpg");
    cv::resize(snd, snd, cv::Size(snd.cols / (2 * SUBSAMPLE_AMOUNT), snd.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(snd, snd, CV_BGR2GRAY);
    cv::equalizeHist(snd, snd);

    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

    auto& settings = CollisionAvoidanceSettings::getInstance();
    settings.DivergencePatchSize = 30;
    for(auto i=0; i<1000; ++i)
    {
        AffineDivergence AffineDivergence(flow, cv::Point2i(33,33), settings);
        auto div = AffineDivergence.getDivergence(i*10);
        cout << i*10 << ":" << div << endl;
    }

    int a;
    cin >> a;
    return 0;
}
