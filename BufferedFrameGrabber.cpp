//
// Created by Helmut on 12/18/2015.
//

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "BufferedFrameGrabber.h"
#include "CollisionAvoidanceSettings.h"

using namespace std;
using namespace hw;
using namespace cv;

hw::BufferedFrameGrabber::BufferedFrameGrabber(CollisionAvoidanceSettings& settings, const unsigned int capacity, std::function<cv::Mat(cv::Mat)> convert_lambda, QObject* parent)
: QObject(parent)
, _settings(settings)
, buffer(capacity)
,  convert_lambda(convert_lambda)
{
	connect(&_settings, &CollisionAvoidanceSettings::useRecordedVideoInsteadOfDeviceChanged,
		this, &hw::BufferedFrameGrabber::useRecordedVideoInsteadOfDeviceChanged);
	connect(&_settings, &CollisionAvoidanceSettings::fileNameChanged,
		this, &hw::BufferedFrameGrabber::fileNameChanged);
	connect(&_settings, &CollisionAvoidanceSettings::deviceChanged,
		this, &hw::BufferedFrameGrabber::deviceChanged);

	initializeVideoCapture();
	fill_buffer();
}

hw::BufferedFrameGrabber::~BufferedFrameGrabber()
{
	if(video_capture!=nullptr) 
	{
		video_capture->release();
		delete video_capture;
	}
}

void hw::BufferedFrameGrabber::initializeVideoCapture()
{
	if(video_capture!=nullptr) 
	{
		video_capture->release();
		delete video_capture;
		video_capture=nullptr;
	}

	if(_settings.getUseRecordedVideoInsteadOfDevice())
		video_capture = new cv::VideoCapture(_settings.getFileName().toStdString());
	else
		video_capture = new cv::VideoCapture(_settings.getDevice());

	if(!video_capture->isOpened())
		throw std::invalid_argument("FileName or Device");
}

void hw::BufferedFrameGrabber::useRecordedVideoInsteadOfDeviceChanged(bool value)
{
	Q_UNUSED(value);
	initializeVideoCapture();    
}

void hw::BufferedFrameGrabber::fileNameChanged(QString value)
{
	Q_UNUSED(value);
	initializeVideoCapture();
}

void hw::BufferedFrameGrabber::deviceChanged(int value)
{
	Q_UNUSED(value);
	initializeVideoCapture();
}

void hw::BufferedFrameGrabber::fill_buffer() {
	while(buffer.size() < buffer.capacity()) {
		cv::Mat frame;

		for(auto i=0; i<_settings.SkipFrames;++i){
			auto success = video_capture->read(frame);
			if(!success)
				return;
		}
		
		buffer.send(convert_lambda(frame));
	}
}

// design pattern @ http://stackoverflow.com/q/4232843
const hw::CircularBuffer<cv::Mat>& hw::BufferedFrameGrabber::get_data()
{
	return buffer;
}

cv::Mat hw::BufferedFrameGrabber::next() {
	auto ret = buffer.receive();
	emit newRawFrame(ret);
	fill_buffer();
	return ret;
}

bool hw::BufferedFrameGrabber::has_next() {
    fill_buffer();
	return buffer.size() > 0;
}

int hw::BufferedFrameGrabber::get_length_in_frames() {
    if(_settings.getUseRecordedVideoInsteadOfDevice())
        return (int)video_capture->get(CAP_PROP_FRAME_COUNT);
	else
		return -1;
}

void hw::BufferedFrameGrabber::reset() {
	initializeVideoCapture();
}
