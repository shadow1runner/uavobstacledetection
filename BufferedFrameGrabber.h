//
// Created by Helmut on 12/18/2015.
//

#ifndef OPTICALFLOW_BUFFEREDFRAMEGRABBER_H
#define OPTICALFLOW_BUFFEREDFRAMEGRABBER_H

#include <memory>
#include <functional>
#include <string>

#include <opencv2/core.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include <QObject>

#include "CircularBuffer.h"

class CollisionAvoidanceSettings;
class OwnFlowWorker;

namespace hw
{
    class BufferedFrameGrabber : public QObject
    {
        Q_OBJECT

    private:
        CollisionAvoidanceSettings& _settings;
        hw::CircularBuffer<cv::Mat> buffer;
        cv::VideoCapture* video_capture = nullptr;
        std::function<cv::Mat(cv::Mat)> convert_lambda;
        void initializeVideoCapture();
        bool _isPaused=false;

    public:
        BufferedFrameGrabber(CollisionAvoidanceSettings& settings, const unsigned int capacity,
                             std::function<cv::Mat(cv::Mat)> convert_lambda,
                             QObject* parent = 0);

        ~BufferedFrameGrabber();
        const hw::CircularBuffer<cv::Mat>& get_data();
        cv::Mat next();
        bool has_next();
        int get_length_in_frames();

        void reset();

    private slots:
        void useRecordedVideoInsteadOfDeviceChanged(bool value);
        void fileNameChanged(QString value);
        void deviceChanged(int value);

    signals:
        void newRawFrame(const cv::Mat& frame);

    private:
        void fill_buffer();
    };
}

#endif //OPTICALFLOW_BUFFEREDFRAMEGRABBER_H
