//
// Created by Helmut on 12/19/2015.
//

#ifndef OPTICALFLOW_CIRCULARBUFFER_H
#define OPTICALFLOW_CIRCULARBUFFER_H

#include <iostream>
// #include <boost/thread/condition.hpp>
// #include <boost/thread/mutex.hpp>
// #include <boost/thread/thread.hpp>
#include <boost/circular_buffer.hpp>
#include <boost/core/noncopyable.hpp>

namespace hw {
    // Thread safe circular buffer http://stackoverflow.com/q/9743605
    template <typename T>
    class CircularBuffer : private boost::noncopyable
    {
    private:
        boost::circular_buffer<T> cb;

    public:

        CircularBuffer(unsigned long long n)
        { cb.set_capacity(n); }

        void send(T imdata)
        {
            cb.push_back(imdata);
        }

        T receive()
        {
            T imdata = cb.front();
            cb.pop_front();
            return imdata;
        }

        unsigned long long int size()
        {
            return cb.size();
        }

        unsigned long long int capacity() {
            return cb.capacity();
        }

        const boost::circular_buffer<T>& get_buffer() const {
            return cb;
        }
    };
}

#endif //OPTICALFLOW_CIRCULARBUFFER_H
