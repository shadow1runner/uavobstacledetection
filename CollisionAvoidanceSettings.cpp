#include "CollisionAvoidanceSettings.h"

CollisionAvoidanceSettings::CollisionAvoidanceSettings(QObject *parent)
    : QObject(parent)
    , _ocamModel(*this)
{
    loadSettings();
}

CollisionAvoidanceSettings& CollisionAvoidanceSettings::getInstance()
{
    static CollisionAvoidanceSettings    instance; // Guaranteed to be destroyed.
                          // Instantiated on first use.
    return instance;
}

void CollisionAvoidanceSettings::loadSettings() 
{
    QSettings settings;
    settings.beginGroup("CollisionAvoidance");

    // Data Source
    useRecordedVideoInsteadOfDevice = settings.value("UseRecordedVideoInsteadOfDevice", true).toBool();
    fileName                        = settings.value("FileName", "/home/helli/d/m/qgroundcontrol/src/CollisionAvoidance/opticalflow/OCamCalib/calibrationFrames/cheapFisheye/a3/a3.avi").toString();
    device                          = settings.value("Device", 0).toInt();
    SkipFrames                      = settings.value("SkipFrames", 5).toInt();
    RawFrameRotation                = settings.value("RawFrameRotation", 0).toInt();

    // OwnFlow Details
    SubsampleAmount                  = settings.value("SubsampleAmount", 5).toInt();
    Particles                        = settings.value("Particles", 200000).toInt();
    WindowSize                       = settings.value("WindowSize", 4).toInt();
    InlierProportionThresholdEnabled = settings.value("InlierProportionThresholdEnabled", true).toBool();
    InlierProportionThreshold        = settings.value("InlierProportionThreshold", 0.003).toDouble();
    DivergencePatchSize              = settings.value("DivergencePatchSize", 20).toInt();
    DivergenceThreshold              = settings.value("DivergenceThreshold", 0.05).toDouble();
    UseAvgDivergenceThreshold        = settings.value("UseAvgDivergenceThreshold", true).toBool();
    AvgDivergenceThreshold           = settings.value("AvgDivergenceThreshold", 0.16).toDouble();
    DivergenceHistoryBufferSize      = settings.value("DivergenceHistoryBufferSize", 15).toInt();
    UndistortFrames                  = settings.value("UndistortFrames", true).toBool();
    UseLegacyDivergence              = settings.value("UseLegacyDivergence", false).toBool();
    UseCollisionLevelDegradation     = settings.value("UseCollisionLevelDegradation", true).toBool();

    // Ocam Model
    OcamModelPath                    = settings.value("OcamModelPath", "/home/helli/d/m/qgroundcontrol/src/CollisionAvoidance/opticalflow/OCamCalib/undistortFunctions/calib_results_fisheye.txt").toString();
    OcamLutScaleFactor               = settings.value("OcamLutScaleFactor", 6.9).toDouble();
    OcamCropEnabled                  = settings.value("OcamCropEnabled", true).toBool();
    OcamRoiX                         = settings.value("OcamRoiX", 164).toInt();
    OcamRoiY                         = settings.value("OcamRoiY", 60).toInt();
    OcamRoiWidth                     = settings.value("OcamRoiWidth", 426).toInt();
    OcamRoiHeight                    = settings.value("OcamRoiHeight", 420).toInt();
    OcamRecticleCenterX              = settings.value("OcamRecticleCenterX", 44).toInt(); // boscam specific settings
    OcamRecticleCenterY              = settings.value("OcamRecticleCenterY", 90).toInt();
    OcamReticleWidth                 = settings.value("OcamReticleWidth", 97.0/2).toInt();
    OcamReticleHeight                = settings.value("OcamReticleHeight", 93.0/2).toInt();

    WithholdCollisionAction          = settings.value("WithholdCollisionAction", false).toBool();
    AutoResumeAfterCollision         = settings.value("AutoResumeAfterCollision", false).toBool();

    // UI Settings
    DisplayBadFramesInUi                 = settings.value("DisplayBadFramesInUi", true).toBool();
    OpticalFlowVectorVisualizationFactor = settings.value("OpticalFlowVectorVisualizationFactor", 5.0).toDouble();

    // Frame Persistence Settings
    ClearOldFramesEnabled  = settings.value("ClearOldFramesEnabled", true).toBool();
    WriteRawFrames         = settings.value("WriteRawFrames", true).toBool();
    WriteBadFrames         = settings.value("WriteBadFrames", true).toBool();
    WriteGoodFrames        = settings.value("WriteGoodFrames", true).toBool();
    WriteOpticalFlowFrames = settings.value("WriteOpticalFlowFrames", true).toBool();
    WriteToOutputEnabled   = settings.value("WriteToOutputEnabled", true).toBool();
    WriteHistogramFrames   = settings.value("WriteHistogramFrames", true).toBool();
    WriteUiFrames          = settings.value("WriteUiFrames", true).toBool();
    BaseFramesDirPath      = settings.value("BaseFramesDirPath", "./ca/").toString();
    BadFramesPath          = settings.value("BadFramesPath", "./ca/BadFrames/").toString();
    GoodFramesPath         = settings.value("GoodFramesPath", "./ca/GoodFrames/").toString();
    HistogramFramesPath    = settings.value("HistogramFramesPath", "./ca/HistogramFrames/").toString();
    OpticalFlowFramesPath  = settings.value("OpticalFlowFramesPath", "./ca/OpticalFlowFrames/").toString();
    RawFramesPath          = settings.value("RawFramesPath", "./ca/RawFrames/").toString();
    UiFramesPath           = settings.value("UiFramesPath", "./ca/UiFrames/").toString();

    RoiEnabled              = settings.value("RoiEnabled", true).toBool();
    MaxPitchAngleDegrees    = settings.value("MaxPitchAngleDegrees", 45).toDouble();
    MaxRollAngleDegrees     = settings.value("MaxRollAngleDegrees", 45).toDouble();
    RoiWidthPx              = settings.value("RoiWidthPx", 45).toInt();
    RoiHeightPx             = settings.value("RoiHeightPx", 45).toInt();
    UseRoiSolelyForDescendDetection= settings.value("UseRoiSolelyForDescendDetection", true).toBool();
    ClimbRateValueThreshold = settings.value("ClimbRateValueThreshold", 0.3).toDouble();

    // log file
    CsvFilePath            = settings.value("CsvFilePath", "./ca/logFile.csv").toString();

    settings.endGroup();

    // load ocam model (if possible and necessary)
    if(UndistortFrames) {
        reloadOcamModel();
    }
}   

void CollisionAvoidanceSettings::storeSettings() 
{
    QSettings settings;
    settings.beginGroup("CollisionAvoidance");

    settings.setValue("UseRecordedVideoInsteadOfDevice", useRecordedVideoInsteadOfDevice);
    settings.setValue("FileName", fileName);
    settings.setValue("Device", device);
    settings.setValue("SkipFrames", SkipFrames);
    settings.setValue("RawFrameRotation", RawFrameRotation);

    settings.setValue("SubsampleAmount", SubsampleAmount);
    settings.setValue("Particles", Particles);
    settings.setValue("WindowSize", WindowSize);
    settings.setValue("InlierProportionThresholdEnabled", InlierProportionThresholdEnabled);
    settings.setValue("InlierProportionThreshold", InlierProportionThreshold);
    settings.setValue("DivergencePatchSize", DivergencePatchSize);
    settings.setValue("DivergenceThreshold", DivergenceThreshold);
    settings.setValue("UseAvgDivergenceThreshold", UseAvgDivergenceThreshold);
    settings.setValue("AvgDivergenceThreshold", AvgDivergenceThreshold);
    settings.setValue("DivergenceHistoryBufferSize", DivergenceHistoryBufferSize);
    settings.setValue("UndistortFrames", UndistortFrames);
    settings.setValue("UseLegacyDivergence", UseLegacyDivergence);
    settings.setValue("UseCollisionLevelDegradation", UseCollisionLevelDegradation);

    settings.setValue("OcamModelPath", OcamModelPath);
    settings.setValue("OcamLutScaleFactor", OcamLutScaleFactor);
    settings.setValue("OcamCropEnabled", OcamCropEnabled);
    settings.setValue("OcamRoiX", OcamRoiX);
    settings.setValue("OcamRoiY", OcamRoiY);
    settings.setValue("OcamRoiWidth", OcamRoiWidth);
    settings.setValue("OcamRoiHeight", OcamRoiHeight);
    settings.setValue("OcamRecticleCenterX", OcamRecticleCenterX);
    settings.setValue("OcamRecticleCenterY", OcamRecticleCenterY);
    settings.setValue("OcamReticleWidth", OcamReticleWidth);
    settings.setValue("OcamReticleHeight", OcamReticleHeight);
    
    settings.setValue("WithholdCollisionAction", WithholdCollisionAction);
    settings.setValue("AutoResumeAfterCollision", AutoResumeAfterCollision);

    settings.setValue("DisplayBadFramesInUi", DisplayBadFramesInUi);
    settings.setValue("OpticalFlowVectorVisualizationFactor", OpticalFlowVectorVisualizationFactor);

    settings.setValue("ClearOldFramesEnabled", ClearOldFramesEnabled);
    settings.setValue("WriteBadFrames", WriteBadFrames);
    settings.setValue("WriteGoodFrames", WriteGoodFrames);
    settings.setValue("WriteHistogramFrames", WriteHistogramFrames);
    settings.setValue("WriteOpticalFlowFrames", WriteOpticalFlowFrames);
    settings.setValue("WriteRawFrames", WriteRawFrames);
    settings.setValue("WriteToOutputEnabled", WriteToOutputEnabled);
    settings.setValue("WriteUiFrames", WriteUiFrames);
    settings.setValue("BaseFramesDirPath", BaseFramesDirPath);
    settings.setValue("BadFramesPath", BadFramesPath);
    settings.setValue("GoodFramesPath", GoodFramesPath);
    settings.setValue("HistogramFramesPath", HistogramFramesPath);
    settings.setValue("OpticalFlowFramesPath", OpticalFlowFramesPath);
    settings.setValue("RawFramesPath", RawFramesPath);
    settings.setValue("UiFramesPath", UiFramesPath);

    settings.setValue("RoiEnabled", RoiEnabled);
    settings.setValue("MaxPitchAngleDegrees", MaxPitchAngleDegrees);
    settings.setValue("MaxRollAngleDegrees", MaxRollAngleDegrees);
    settings.setValue("RoiWidthPx", RoiWidthPx);
    settings.setValue("RoiHeightPx", RoiHeightPx);
    settings.setValue("UseRoiSolelyForDescendDetection", UseRoiSolelyForDescendDetection);
    settings.setValue("ClimbRateValueThreshold", ClimbRateValueThreshold);

    settings.setValue("CsvFilePath", CsvFilePath);

    settings.endGroup();
}

bool CollisionAvoidanceSettings::getUseRecordedVideoInsteadOfDevice()
{ 
	return useRecordedVideoInsteadOfDevice; 
}

void CollisionAvoidanceSettings::setUseRecordedVideoInsteadOfDevice(bool value)
{ 
	useRecordedVideoInsteadOfDevice=value; emit useRecordedVideoInsteadOfDeviceChanged(value); 
}

QString& CollisionAvoidanceSettings::getFileName()
{ 
	return fileName; 
}

void CollisionAvoidanceSettings::setFileName(QString value)
{ 
	fileName = value;
	emit fileNameChanged(value); 
}

int  CollisionAvoidanceSettings::getDevice()
{ 
	return device; 
}

void CollisionAvoidanceSettings::setDevice(int value)
{ 
	device=value;
	deviceChanged(value);
}

void CollisionAvoidanceSettings::reloadOcamModel()
{
    auto ret = get_ocam_model_cpp(_ocamModel, OcamModelPath.toStdString());
    if(ret!=0)
        throw std::invalid_argument("OcamModelPath invalid: unable to load ocam model");

    _ocamModel.initialize();
    _ocamModelValid = true;
}

bool CollisionAvoidanceSettings::ocamModelValid() const
{
    return _ocamModelValid;
}

struct ocam_model& CollisionAvoidanceSettings::getOcamModel()
{
    if(!_ocamModelValid) {
        throw std::invalid_argument("No OcamModel existent");
    }

    return _ocamModel;
}
