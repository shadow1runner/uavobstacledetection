#ifndef COLLISIONAVOIDANCE_COLLISIONAVOIDANCESETTINGS_H
#define COLLISIONAVOIDANCE_COLLISIONAVOIDANCESETTINGS_H

#include "ocam_functions.h"

#include <string>

#include <QObject>
#include <QSettings>
#include <QDebug>

class CollisionAvoidanceSettings : public QObject
{
    Q_OBJECT

private:
    CollisionAvoidanceSettings(QObject *parent=NULL);

    bool    useRecordedVideoInsteadOfDevice;
    QString fileName;
    int     device;
    bool    _ocamModelValid = false;
    struct ocam_model _ocamModel;

signals:
    void useRecordedVideoInsteadOfDeviceChanged(bool value);
    void fileNameChanged(QString value);
    void deviceChanged(int value);

public:

    static CollisionAvoidanceSettings& getInstance();

    // aso http://stackoverflow.com/a/1008289
    CollisionAvoidanceSettings(CollisionAvoidanceSettings const&) = delete;
    void operator=(CollisionAvoidanceSettings const&)  = delete;

    void loadSettings();
    void storeSettings();

    bool getUseRecordedVideoInsteadOfDevice();
    void setUseRecordedVideoInsteadOfDevice(bool value);
    QString& getFileName();
    void setFileName(QString value) ;
    int  getDevice();
    void setDevice(int value);

    int     SkipFrames;
    int     RawFrameRotation;
    int     SubsampleAmount;
    int     Particles;
    int     WindowSize;
    bool    InlierProportionThresholdEnabled;
    double  InlierProportionThreshold;
    int     DivergencePatchSize;
    double  DivergenceThreshold;
    bool    UseAvgDivergenceThreshold;
    double  AvgDivergenceThreshold;
    int     DivergenceHistoryBufferSize;
    bool    UndistortFrames;
    bool    UseLegacyDivergence;
    bool    UseCollisionLevelDegradation;

    QString OcamModelPath;
    double OcamLutScaleFactor;
    bool OcamCropEnabled;
    int OcamRoiX;
    int OcamRoiY;
    int OcamRoiWidth;
    int OcamRoiHeight;
    int OcamRecticleCenterX;
    int OcamRecticleCenterY;
    int OcamReticleWidth;
    int OcamReticleHeight;

    bool    WithholdCollisionAction;
    bool    AutoResumeAfterCollision;

    bool    DisplayBadFramesInUi;
    double  OpticalFlowVectorVisualizationFactor;
    
    bool    ClearOldFramesEnabled;
    bool    WriteBadFrames;
    bool    WriteGoodFrames;
    bool    WriteHistogramFrames;
    bool    WriteOpticalFlowFrames;
    bool    WriteRawFrames;
    bool    WriteToOutputEnabled;
    bool    WriteUiFrames;
    QString BaseFramesDirPath;
    QString BadFramesPath;
    QString GoodFramesPath;
    QString HistogramFramesPath;
    QString OpticalFlowFramesPath;
    QString RawFramesPath;
    QString UiFramesPath;
    
    bool   RoiEnabled;
    double MaxPitchAngleDegrees;
    double MaxRollAngleDegrees;
    int    RoiWidthPx;
    int    RoiHeightPx;
    bool   UseRoiSolelyForDescendDetection;
    double ClimbRateValueThreshold;

    QString CsvFilePath;

    void reloadOcamModel();
    bool ocamModelValid() const;
    struct ocam_model& getOcamModel();
};

#endif // COLLISIONAVOIDANCE_COLLISIONAVOIDANCESETTINGS_H

