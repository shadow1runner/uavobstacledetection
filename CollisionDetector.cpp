#include "CollisionDetector.h"
#include "RoiBuilder.h"
#include "AffineDivergence.h"

#include <QDebug>

using namespace std;
using namespace hw;

hw::CollisionDetector::CollisionDetector(CollisionAvoidanceSettings& settings, const RoiBuilder* const roiBuilder, QObject* parent)
: QObject(parent)
    , _settings(settings)
    , _divergenceHistory(settings)
{
    connect(roiBuilder, &RoiBuilder::roiReady,
            this, &hw::CollisionDetector::_roiReady);
}

void hw::CollisionDetector::_roiReady(const cv::Rect& roi, double rollDegree, double pitchDegree, double climbRate)
{
    _roi = roi;
    _rollDegree = rollDegree;
    _pitchDegree = pitchDegree;
    _climbRate = climbRate;
}

std::shared_ptr<hw::CollisionDetectorResult> hw::CollisionDetector::detectCollision(const cv::Point2i& collidee, const cv::Mat& opticalFlow, unsigned long long frameCount)
{
    if(_settings.UseLegacyDivergence) {
        // the following divergence has been used until 08-29-2016 and performed ok however, in comparisons to `AffineDivergence` I noticed severe differences in results also, Stabinger hardly (if at all) used this divergence approach, therefore I consider this unreliable
        hw::Divergence div(_settings, collidee);
        auto unreliableDiv = div.calculateDivergence(opticalFlow);
        _lastDiv = unreliableDiv;        
    } else {
        hw::AffineDivergence AffineDivergence(opticalFlow, collidee, _settings);
        _lastDiv = AffineDivergence.getDivergence();
    }

    auto detectorResult = CollisionDetectorResultEnum::None;

    qInfo() << "Climbing rate: " << _climbRate << " m/s";
    if(_settings.RoiEnabled && !_settings.UseRoiSolelyForDescendDetection && !_roi.contains(collidee)) {
        detectorResult = CollisionDetectorResultEnum::OutOfRoi;
        qInfo() << "Point is not contained in ROI, collision level stays at " << _collisionLevel;
    } else if(_climbRate < _settings.ClimbRateValueThreshold ) { // && _settings.RoiEnabled  && _roi.contains(_settings.getOcamModel().getLenseCenter())) {
        detectorResult = CollisionDetectorResultEnum::DescendingAndIgnored;
        qInfo() << "Vehicle is DESCENDing at " << _climbRate << " m/s which is < " << _settings.ClimbRateValueThreshold << " m/s -> IGNORED.";
    } else {
        // first detect if we reached a collision level over time
        detectorResult = checkCollisionOverTime();

        if(_settings.UseAvgDivergenceThreshold) {
            // second (as a backup check) check if the (moving) average divergence is higher than the threshold
            auto avgDiv = _divergenceHistory.addDivergence(_lastDiv);
            if(avgDiv>=_settings.AvgDivergenceThreshold && frameCount > 20) // prevent triggering too early as the first few frames tend to have a high inlier ratio and divergence
            {
                _collisionLevel = CollisionLevel::high;
                detectorResult = CollisionDetectorResultEnum::TooHighAvgDivThreshold;
                qInfo() << "Raised collisionLevel to high - avgDiv >= Threshold: " << avgDiv << " >= " << _settings.AvgDivergenceThreshold;
            }
        }
    }

    _collisionLevel = CollisionLevelHelper::clampCollisionLevel(_collisionLevel);

    return std::shared_ptr<CollisionDetectorResult>(new CollisionDetectorResult(
        static_cast<hw::CollisionLevel>((int)_collisionLevel), detectorResult));
}

CollisionDetectorResultEnum hw::CollisionDetector::checkCollisionOverTime() {
    if(_lastDiv >= _settings.DivergenceThreshold) {
        ++_collisionLevel;
        qDebug() << "Collision level raised by 1 to " << _collisionLevel << "(" << QString::fromStdString(CollisionLevelHelper::toString(_collisionLevel)) << "), div >= threshold: " << _lastDiv << " >= " << _settings.DivergenceThreshold;
        return CollisionDetectorResultEnum::BiggerThanDivThreshold;
    } else {
        --_collisionLevel;
        qDebug() << "Collision level lowered by 1 to " << _collisionLevel << "(" << QString::fromStdString(CollisionLevelHelper::toString(_collisionLevel)) << "), div < threshold: " << _lastDiv << " > " << _settings.DivergenceThreshold;
        return CollisionDetectorResultEnum::SmallerThanDivThreshold;
    }
}

const hw::DivergenceHistory* hw::CollisionDetector::getDivergenceHistory() const
{
    return &_divergenceHistory;
}

void hw::CollisionDetector::reset()
{
    _divergenceHistory.reset();
    _collisionLevel = hw::CollisionLevel::low;
}

double hw::CollisionDetector::getLastDivergence()
{
    return _lastDiv;
}

void hw::CollisionDetector::reportSkippedFrame()
{
    if(!_settings.UseCollisionLevelDegradation)
        return;

    // each failed frame reduces the collisionLevel, 20 successive frames remove one whole level...
    if(_collisionLevel>=0.05)
        _collisionLevel -= 0.05;

    // ... and same amount from avg divergence
    _divergenceHistory.degradeAvgDivergence(_settings.AvgDivergenceThreshold/50);
}
