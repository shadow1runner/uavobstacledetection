#ifndef OPTICALFLOW_COLLISIONDETECTOR_H
#define OPTICALFLOW_COLLISIONDETECTOR_H

#include "CollisionAvoidanceSettings.h"
#include "DivergenceHistory.h"
#include "CollisionLevel.h"
#include "CollisionDetectorResult.h"

#include <opencv2/core.hpp>

#include <memory>

#include <QObject>

class RoiBuilder;

namespace hw {
    class CollisionDetector : public QObject {
        Q_OBJECT

    private:
        CollisionAvoidanceSettings& _settings;
        DivergenceHistory _divergenceHistory;
        double _collisionLevel = (double)hw::CollisionLevel::low;
        cv::Rect _roi;
        double _rollDegree;
        double _pitchDegree;
        double _climbRate;
        double _lastDiv;

        CollisionDetectorResultEnum checkCollisionOverTime();

    private slots:
        void _roiReady(const cv::Rect& roi, double rollDegree, double pitchDegree, double climbRate);

    public:
        CollisionDetector(CollisionAvoidanceSettings& settings, const RoiBuilder* const roiBuilder, QObject* parent = NULL);
        std::shared_ptr<CollisionDetectorResult> detectCollision(const cv::Point2i& collidee, const cv::Mat& opticalFlow, unsigned long long frameCount);
        const DivergenceHistory* getDivergenceHistory() const;
        void reset();
        double getLastDivergence();
        void reportSkippedFrame();
    };
}

#endif // OPTICALFLOW_COLLISIONDETECTOR_H
