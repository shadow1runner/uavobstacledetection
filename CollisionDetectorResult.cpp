#include "CollisionDetectorResult.h"

using namespace hw;
using namespace std;

hw::CollisionDetectorResult::CollisionDetectorResult(hw::CollisionLevel collisionLevel, hw::CollisionDetectorResultEnum evaluationResult)
    : _collisionLevel(collisionLevel)
    , _evaluationResult(evaluationResult)
    {
    }

hw::CollisionLevel hw::CollisionDetectorResult::getCollisionLevel() {
    return _collisionLevel;
}

hw::CollisionDetectorResultEnum hw::CollisionDetectorResult::getEvaluationResult() {
    return _evaluationResult;
}

std::string hw::CollisionDetectorResult::getEvaluationResultText() {
	switch(_evaluationResult) {
		case hw::CollisionDetectorResultEnum::None:                    return "None";
		case hw::CollisionDetectorResultEnum::OutOfRoi:                return "OutOfRoi";
		case hw::CollisionDetectorResultEnum::DescendingAndIgnored:    return "DescendingAndIgnored";
		case hw::CollisionDetectorResultEnum::BiggerThanDivThreshold:  return "BiggerThanDivThreshold";
		case hw::CollisionDetectorResultEnum::SmallerThanDivThreshold: return "SmallerThanDivThreshold";
		case hw::CollisionDetectorResultEnum::TooHighAvgDivThreshold:  return "TooHighAvgDivThreshold";
		default:                                                       return "Error";
	}
}
