#ifndef COLLISIONAVOIDANCE_COLLISIONDETECTORRESULT_H
#define COLLISIONAVOIDANCE_COLLISIONDETECTORRESULT_H

#include "CollisionLevel.h"

namespace hw {
    enum CollisionDetectorResultEnum {
        None,
        OutOfRoi,
        DescendingAndIgnored,
        BiggerThanDivThreshold,
        SmallerThanDivThreshold,
        TooHighAvgDivThreshold
    };

    class CollisionDetectorResult
    {
        hw::CollisionLevel _collisionLevel;
        hw::CollisionDetectorResultEnum _evaluationResult;

    public:
        CollisionDetectorResult(hw::CollisionLevel collisionLevel, hw::CollisionDetectorResultEnum evaluationResult);        
        hw::CollisionLevel getCollisionLevel();
        hw::CollisionDetectorResultEnum getEvaluationResult();
        std::string getEvaluationResultText();
    };
}

#endif // COLLISIONAVOIDANCE_COLLISIONDETECTORRESULT_H
