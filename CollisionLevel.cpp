#include "CollisionLevel.h"

using namespace std;
using namespace hw;

// limits `level` to [CollisionLevel::low, CollisionLevel::high]
double hw::CollisionLevelHelper::clampCollisionLevel(double level)
{
	auto levelInt = (int)level;
    if(levelInt >= CollisionLevel::high)
        return CollisionLevel::high;
    if(levelInt <= CollisionLevel::low)
    	return CollisionLevel::low;

    return level;
}

// normalizes `level` to one of the existing `CollisionLevel` enum values
hw::CollisionLevel hw::CollisionLevelHelper::normalizeCollisionLevelToEnumValues(int level)
{
    level = clampCollisionLevel(level);

	if(level >= (int)CollisionLevel::high)
        return CollisionLevel::high;
	if(level >= (int)CollisionLevel::warn)
        return CollisionLevel::warn;
	if(level >= (int)CollisionLevel::medium)
        return CollisionLevel::medium;

	return CollisionLevel::low;
}

std::string hw::CollisionLevelHelper::toString(const int level)
{
	auto clamped = normalizeCollisionLevelToEnumValues(level);
	switch(clamped) {
		case CollisionLevel::low:    return "low";
		case CollisionLevel::medium: return "medium";
		case CollisionLevel::warn:   return "WARN";
		case CollisionLevel::high:   return "HIGH";
	}
}	
