#ifndef OPTICALFLOW_COLLISIONLEVEL_H
#define OPTICALFLOW_COLLISIONLEVEL_H

#include <string>

namespace hw {
	enum CollisionLevel
	{
		// intermediate values to allow `CollisionLevel` as a proper parameter
		// which can be passed everywhere, for normalization the functions
		// provided by `CollisionLevelHelper` should be used
		low    = 1,
		two    = 2,
		medium = 3,
		four   = 4,
		warn   = 5,
		six    = 6,
		high   = 7
	};
		
	class CollisionLevelHelper
	{
	public:
		// limits `level` to [CollisionLevel::low, CollisionLevel::high]
        static double clampCollisionLevel(double level);

		// normalizes `level` to one of the existing `CollisionLevel` enum values
        static CollisionLevel normalizeCollisionLevelToEnumValues(int level);

        static std::string toString(const int level);		
	};
}

#endif
