#include "Converter.h"
#include "CollisionAvoidanceSettings.h"

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

hw::Converter::Converter(CollisionAvoidanceSettings& settings)
    : _settings(settings)
{
}

cv::Mat hw::Converter::undistort(const cv::Mat& frame)
{
    if(!_settings.UndistortFrames || !_settings.ocamModelValid())
        return frame;

    if(!_lutsInitialized)
    {
        initializeLuts(frame);
        _lutsInitialized = true;
    }

    /* --------------------------------------------------------------------*/  
    /* Undistort using specified interpolation method                      */
    /* Other possible values are (see OpenCV doc):                         */
    /* CV_INTER_NN - nearest-neighbor interpolation,                       */
    /* CV_INTER_LINEAR - bilinear interpolation (used by default)          */
    /* CV_INTER_AREA - resampling using pixel area relation. It is the preferred method for image decimation that gives moire-free results. In case of zooming it is similar to CV_INTER_NN method. */
    /* CV_INTER_CUBIC - bicubic interpolation.                             */
    /* --------------------------------------------------------------------*/
    // in legacy c syntax:
    // cvRemap(src1, undistortedIpl, mapx_persp, mapy_persp, CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS, cvScalarAll(0));
    cv::Mat undistorted;
    remap(frame, undistorted, _xOcamLut, _yOcamLut, cv::INTER_LINEAR); // int borderMode=BORDER_CONSTANT, const Scalar& borderValue=Scalar())


//    cv::imshow("undistorted", undistorted);
//    cv::waitKey(25);

    return undistorted;
}

cv::Mat hw::Converter::crop(const cv::Mat& frame)
{
    // the values have been found empirically by saving (`cv::imsave`) a undistorted frame (without any other conversions like rotation etc.)
    // in gimp the actual pinhole was measured with the measure tool (Strg+Shift+M)
    // this determinesimsave the topleft point as well as the width+height which should be cropped

    cv::Mat roi(frame.rows, frame.cols, frame.type(), cv::Scalar::all(0));
    ellipse(roi, Point(_settings.OcamRecticleCenterX, _settings.OcamRecticleCenterY), cv::Size(_settings.OcamReticleWidth, _settings.OcamReticleHeight), 0, 0, 360, Scalar::all(255), -1, 8);

    cv::Mat ret;
    bitwise_and(frame, roi, ret);

    // cv::imwrite("./undistorted.jpg", frame);
    // cv::imwrite("./masked.jpg", ret);

    cv::Mat smallRoi(ret(cv::Rect(_settings.OcamRoiX, _settings.OcamRoiY, _settings.OcamRoiWidth, _settings.OcamRoiHeight)));
    // cv::imwrite("./cropped.jpg", smallRoi);
    return smallRoi;
}

cv::Mat hw::Converter::resize(const cv::Mat& colorFrame) 
{
    cv::Mat tmp, gray, equalized;
    if(_settings.SubsampleAmount!=0)
        cv::resize(colorFrame,
                   tmp,
                   cv::Size(colorFrame.cols / (2 * _settings.SubsampleAmount),
                            colorFrame.rows / (2 * _settings.SubsampleAmount)),
                   0, 0, cv::INTER_NEAREST);
    else
        tmp=colorFrame;

    return tmp;
}

void hw::Converter::initializeLuts(const cv::Mat& frame)
{
    CvMat* mapxPersp = cvCreateMat(frame.rows, frame.cols, CV_32FC1);
    CvMat* mapyPersp = cvCreateMat(frame.rows, frame.cols, CV_32FC1);
    
    /* --------------------------------------------------------------------  */  
    /* Create Look-Up-Table for perspective undistortion                     */
    /* SF is kind of distance from the undistorted image to the camera       */
    /* (it is not meters, it is justa zoom fator)                            */
    /* Try to change SF to see how it affects the result                     */
    /* The undistortion is done on a  plane perpendicular to the camera axis */
    /* --------------------------------------------------------------------  */
    float sf = _settings.OcamLutScaleFactor;
    create_perspecive_undistortion_LUT(mapxPersp, mapyPersp, &(_settings.getOcamModel()), sf);

    _xOcamLut = cvarrToMat(mapxPersp, true); // true: copy data
    _yOcamLut = cvarrToMat(mapyPersp, true); // true: copy data

    cvReleaseMat(&mapxPersp);
    cvReleaseMat(&mapyPersp);
}

        
void hw::Converter::convertImage(const cv::Mat& colorFrame) 
{
    // static unsigned long frame = 0;
    // imwrite("ca/raw_" + std::to_string(frame) + ".jpg", colorFrame);

    cv::Mat mat = undistort(colorFrame);
    // imwrite("ca/undistorted_" + std::to_string(frame++) + ".jpg", undistorted);

    mat = resize(mat);
    cv::Mat tmp;
    bilateralFilter(mat, tmp, 5, 50, 50); // http://docs.opencv.org/3.0-beta/modules/imgproc/doc/filtering.html#bilateralfilter
    auto rotated = mat = rotate(tmp);
    if(_settings.OcamCropEnabled)
        mat = crop(mat);
    auto gray = grayscaleAndEqualize(mat);

    if(!_ocamModelInitialized) {
        initializeOcamModel(gray.size());
    }


    // static unsigned long frame = 0;
    // imwrite("ca/convert_" + std::to_string(frame++) + ".jpg", mat);

    emit imageConverted(gray, mat);
}

void hw::Converter::initializeOcamModel(const cv::Size& frameSize) 
{
    struct ocam_model& ocamModel = _settings.getOcamModel();
    ocamModel.setCroppedFrameSize(frameSize);
    _ocamModelInitialized = true;
}


cv::Mat hw::Converter::rotate(const cv::Mat& frame)
{
    switch(_settings.RawFrameRotation) {
        case 0: return rotate90n(frame, 0);
        case 1: return rotate90n(frame, 90);
        case 2: return rotate90n(frame, 180);
        case 3: return rotate90n(frame, 270);
    }
}

// aso http://stackoverflow.com/a/16278334/2559632
cv::Mat hw::Converter::rotate90n(const cv::Mat& src, const int angle)
{
    cv::Mat dst;

    if(angle == 270 || angle == -90){
        // Rotate clockwise 270 degrees
        cv::transpose(src, dst);
        cv::flip(dst, dst, 0);
    }else if(angle == 180 || angle == -180){
        // Rotate clockwise 180 degrees
        cv::flip(src, dst, -1);
    }else if(angle == 90 || angle == -270){
        // Rotate clockwise 90 degrees
        cv::transpose(src, dst);
        cv::flip(dst, dst, 1);
    }else if(angle == 360 || angle == 0 || angle == -360){
        if(src.data != dst.data){
            src.copyTo(dst);
        }
    }

    return dst;
}

cv::Mat hw::Converter::grayscaleAndEqualize(const cv::Mat& frame)
{
    cv::Mat gray, equalized;
    cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
    cv::equalizeHist(gray, equalized);
    return equalized;
}
