//
// Created by Helmut on 1/5/2016.
//

#ifndef OPTICALFLOW_CONVERTER_H
#define OPTICALFLOW_CONVERTER_H

#include <opencv2/core.hpp>

#include <QObject>

class CollisionAvoidanceSettings;

namespace hw {
    class Converter : public QObject
    {
        Q_OBJECT

        cv::Mat _xOcamLut;
        cv::Mat _yOcamLut;
        bool _lutsInitialized = false;
        void initializeLuts(const cv::Mat& frame);

        bool _ocamModelInitialized = false;
        void initializeOcamModel(const cv::Size& frameSize);

        cv::Mat undistort(const cv::Mat& frame);
        cv::Mat crop(const cv::Mat& frame);
        cv::Mat resize(const cv::Mat& colorFrame);
        cv::Mat rotate(const cv::Mat& frame);
        cv::Mat rotate90n(const cv::Mat& src, const int angle);
        cv::Mat grayscaleAndEqualize(const cv::Mat& frame);

    public:
        Converter(CollisionAvoidanceSettings& settings);
        
        Q_INVOKABLE void convertImage(const cv::Mat& colorFrame);
        
    signals:
        void imageConverted(const cv::Mat& frame, const cv::Mat& convertedColorFrame);
    private:
        CollisionAvoidanceSettings& _settings;

    };
}

#endif //OPTICALFLOW_OWNFLOW_H
