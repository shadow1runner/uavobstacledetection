#include "Divergence.h"

#include <stdexcept>

hw::Divergence::Divergence(const CollisionAvoidanceSettings& settings, const cv::Point2i& referencePoint)
    : _settings(settings)
    , _referencePoint(referencePoint)
{}


/*
 * Returns the divergence at point x,y
 * div = ux+vy where ux and vy are the partial derivatives of the two flow components in their respective directions
 * Info from: http://www.nicta.com.au/pub?doc=4025
 */
double hw::Divergence::calculateDivergence(const cv::Mat& opticalFlow, int stepSize)
{
    auto ux = 0.0;
    auto vy = 0.0;

    const auto& referenceFlow = opticalFlow.at<cv::Point2f>(_referencePoint.y, _referencePoint.x);

    for(int i=1; i<=_settings.DivergencePatchSize; i+=stepSize)
    {
        if(_referencePoint.x+i >= opticalFlow.cols || _referencePoint.y+i >= opticalFlow.rows)
            break;

        const auto& right = opticalFlow.at<cv::Point2f>(_referencePoint.y  , _referencePoint.x+i); // ATTENTION matrix indexing: != .at<cv::Point2f>(_referencePoint.x+i,_referencePoint.y) but is equivalent to .at<cv::Point2f>(cv::Point2i(_referencePoint.x+i,_referencePoint.y)
        const auto& below = opticalFlow.at<cv::Point2f>(_referencePoint.y+i, _referencePoint.x);

        ux += right.x - referenceFlow.x;
        vy += below.y - referenceFlow.y;
    }

    divergence = (ux + vy) / (_settings.DivergencePatchSize / stepSize);
    hasDivergence = true;
    return divergence;
}

double hw::Divergence::getDivergence() const
{
    if(!hasDivergence)
        throw std::logic_error("divergence");

    return divergence;
}

const cv::Point2i& hw::Divergence::getReferencePoint()
{
    return _referencePoint;
}
