//
// Created by Helmut on 1/2/2016.
//

#ifndef OPTICALFLOW_DIVERGENCE_H
#define OPTICALFLOW_DIVERGENCE_H

#include "CollisionAvoidanceSettings.h"

#include <opencv2/core.hpp>

namespace hw {
    class Divergence
    {
        const CollisionAvoidanceSettings& _settings;
        const cv::Point2i _referencePoint;
        bool hasDivergence = false;
        double divergence;
    public:
        Divergence(const CollisionAvoidanceSettings& settings, const cv::Point2i& referencePoint);
        double calculateDivergence(const cv::Mat& opticalFlow, int stepSize = 1);
        double getDivergence() const;
        const cv::Point2i& getReferencePoint();
    };
}
#endif //OPTICALFLOW_DIVERGENCE_H
