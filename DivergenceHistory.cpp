#include "DivergenceHistory.h"

#include <algorithm>

#include <QDebug>
#include <iostream>

using namespace std;
using namespace hw;

hw::DivergenceHistory::DivergenceHistory(const CollisionAvoidanceSettings& settings, QObject* parent)
	: QObject(parent)
	, _settings(settings)
	, _buffer(settings.DivergenceHistoryBufferSize)
{
}

void hw::DivergenceHistory::calculateAverageDivergence() 
{
    _avgDivergence = std::accumulate(_buffer.begin(), _buffer.end(), 0.0) / _settings.DivergenceHistoryBufferSize; // in order to allow for comparisons on the avg divergence, it's important that we use the same amount as denominator
	emit avgDivergenceChanged(_avgDivergence);
}

double hw::DivergenceHistory::addDivergence(double div)
{
    _buffer.push_back(div);
	emit divergenceAdded(div);

	calculateAverageDivergence();
	return _avgDivergence;
}

double hw::DivergenceHistory::getMovingAverageDivergence() const
{
    return _avgDivergence;
}

double hw::DivergenceHistory::getLastDivergence() const
{
	return _buffer.front();
}

void hw::DivergenceHistory::reset()
{
	_buffer.clear();
	_avgDivergence=0.0;
}

// subtracts `amount` from AVG divergence if AVG divergence > 0; otherwise `-amount` is subtracted 
// -- used for avg divergence degradation over time
void hw::DivergenceHistory::degradeAvgDivergence(double amount)
{
	if(_buffer.empty())
		return;

	if(_avgDivergence<=0.0) {
		amount = (-1)*abs(amount); // subtract `-amount` from avg divergence 
		amount = max(_avgDivergence, amount);
	} else {
		amount = min(_avgDivergence, amount);
	}
	
//    auto numElements = min((int)_buffer.size(), _settings.DivergenceHistoryBufferSize);
    auto numElements = _settings.DivergenceHistoryBufferSize; // because of the implementation of `calculateAverageDivergence` we have to subtract the whole buffer size from the average value, otherwise this value would be spoiled

    auto lastDiv = _buffer.back();

	lastDiv -= amount*numElements; // degrade last divergence over time by 0.05 for each divergence in the buffer -> degrades average divergence over time (easier than iterating over each element and subtracting 0.05 from there...)
    _buffer.pop_back();
    _buffer.push_back(lastDiv);
    calculateAverageDivergence();
}

const int hw::DivergenceHistory::getBufferSize() const {
	return (int)_buffer.size();
}

