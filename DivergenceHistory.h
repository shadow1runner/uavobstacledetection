#ifndef OPTICALFLOW_DIVERGENCEHISTORY_H
#define OPTICALFLOW_DIVERGENCEHISTORY_H

#include "CollisionAvoidanceSettings.h"
#include "Divergence.h"

#include <boost/circular_buffer.hpp>

#include <QObject>

namespace hw {
	class DivergenceHistory : public QObject 
	{
		Q_OBJECT

		const CollisionAvoidanceSettings& _settings;
        boost::circular_buffer<double> _buffer;
		double _avgDivergence;

		void calculateAverageDivergence();

	public:
		DivergenceHistory(const CollisionAvoidanceSettings& settings, QObject* parent = NULL);
		
		double addDivergence(double div);

        double getMovingAverageDivergence() const;

		double getLastDivergence() const;

		void reset();

		void degradeAvgDivergence(double amount=0.05);

		const int getBufferSize() const;

	signals:
		void avgDivergenceChanged(double avgDivergence);
		void divergenceAdded(double divergence);
	};
}

#endif
