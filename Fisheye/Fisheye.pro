QT += core
#QT += widgets
QT -= gui

CONFIG += c++11

TARGET = Fisheye
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app


##################
# opencv - http://stackoverflow.com/a/15890266
#INCLUDEPATH += "E:\\temp\\opencv\\minGwBuild\\install\\include" \
#               "E:\\temp\\opencv\\minGwBuild\\install\\include\\opencv" \
#               "E:\\temp\\opencv\\minGwBuild\\install\\include\\opencv2"

INCLUDEPATH += \
    "/usr/include/" \
    "/usr/include/opencv" \
    "/usr/include/opencv2" \
    "/usr/local/include/" \
    "/usr/local/include/opencv" \
    "/usr/local/include/opencv2"

LIBS += \
    -L/usr/local/lib/ \
    -lopencv_core \
    -lopencv_highgui \
    -lopencv_imgproc \
    -lopencv_features2d \
    -lopencv_calib3d \
    -lopencv_flann \
    -lopencv_imgcodecs \
    -lopencv_ml \
    -lopencv_objdetect \
    -lopencv_photo \
    -lopencv_shape \
    -lopencv_stitching \
    -lopencv_superres \
    -lopencv_video \
    -lopencv_videoio \
    -lopencv_videostab
    #-lopencv_optflow


##################
# boost - http://stackoverflow.com/a/16998798
DEFINES += BOOST_THREAD_USE_LIB
#DEFINES += BOOST_THREAD_POSIX #https://svn.boost.org/trac/boost/ticket/5964
#DEFINES += BOOST_THREAD_LINUX
DEFINES += BOOST_SYSTEM_NO_DEPRECATED # http://stackoverflow.com/a/18877

INCLUDEPATH += /usr/include/boost   #E:\\dev\\boost_1_60_0
LIBS += -L/usr/lib/ \ #"-LE:/dev/boost_1_60_0/stage/lib/" \
    -lboost_system \
    -lboost_thread \
    -lboost_date_time \
    -lboost_program_options \
    -lboost_filesystem \
    -lboost_regex

# http://stackoverflow.com/a/1782435; winsock aso http://stackoverflow.com/a/18445, needed for boost::asio::io_service (used in multi-threading)
# LIBS += -lws2_32

##################

SOURCES += \
    camera_calibration.cpp

#HEADERS += \

copydata.commands = $(COPY_DIR) $$PWD/settings.xml $$PWD/images.xml $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata
    

INCLUDEPATH += \
    $$PWD/
    
