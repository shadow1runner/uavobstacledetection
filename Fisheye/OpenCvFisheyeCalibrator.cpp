#include "OpenCvFisheyeCalibrator.h"

#include <iostream>

using namespace std;
using namespace cv;

OpenCvFisheyeCalibrator::OpenCvFisheyeCalibrator(std::string globToCheckerboardFiles)
	: _globToCheckerboardFiles(globToCheckerboardFiles)
{

}

OpenCvFisheyeCalibrator::~OpenCvFisheyeCalibrator()
{

}	

void OpenCvFisheyeCalibrator::calibrate(cv::Size& const checkerBoardSize)
{
	extractCorners(checkerBoardSize);

	cv::fisheye::calibrate(
	     _objectPoints, _imagePoints, img.size(), 
	     _K, _D, _rvec, _tvec,
	     fisheye::CALIB_RECOMPUTE_EXTRINSIC, // http://docs.opencv.org/3.0-beta/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#fisheye-calibrate 
	     cv::TermCriteria(3, 20, 1e-6)
	);
}

void extractCorners(cv::Size& const checkerBoardSize)
{
	int i=0;
	for(auto fileInfo : QDir::entryInfoList(_globToCheckerboardFiles))
	{
		Mat img = imread(fileInfo.absoluteFilePath().toStdString());
		imshow("img", img);

		std::vector<cv::Point2f> corners;

		bool found = findChessboardCorners(img, checkerBoardSize, corners); // http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#bool findChessboardCorners(InputArray image, Size patternSize, OutputArray corners, int flags)

		drawChessboardCorners(img, checkerBoardSize, Mat(corners), found);
		imshow("checkerBoard", img);
		waitKey(0);
		
		if(!found) {
			cout << "No Checkerboard found in Frame #" << i++ << endl;
			continue;
		} else {
			// cornerSubPix(img, corners, Size(11, 11), Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1)); // and to determine their positions more accurately, the function calls cornerSubPix(). You also may use the function cornerSubPix() with different parameters if returned coordinates are not accurate enough. - http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html#bool findChessboardCorners(InputArray image, Size patternSize, OutputArray corners, int flags)
			cout << "Checkerboard found in Frame #" << i++ < endl;
		}
	}
}

void OpenCvFisheyeCalibrator::saveCalibrationData(std::string dest)
{

}

void OpenCvFisheyeCalibrator::loadCalibrationData(std::string src)
{

}
