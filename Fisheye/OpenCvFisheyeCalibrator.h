#ifndef OPENCVFISHEYECALIBRATOR_H
#define OPENCVFISHEYECALIBRATOR_H

#include <string>
#include <vector>
#include <opencv2/core.hpp>

class OpenCvFisheyeCalibrator
{
public:
	OpenCvFisheyeCalibrator(std::string globToCheckerboardFiles);
	~OpenCvFisheyeCalibrator();	
	void calibrate(cv::Size& const checkerBoardSize);
	void saveCalibrationData(std::string dest);
	void loadCalibrationData(std::string src);

private:
	std::string _globToCheckerboardFiles;
	std::vector<std::vector<cv::Point2d>> _imagePoints;  // 2d points in image plane
    std::vector<std::vector<cv::Point3d>> _objectPoints; // 3d point in real world space

    cv::Matx33d _K;
    cv::Vec4d _D;
    std::vector<cv::Vec3d> _rvec;
    std::vector<cv::Vec3d> _tvec;
};

#endif