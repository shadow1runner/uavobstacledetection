//
// Created by Helmut on 12/19/2015.
//

#include "FocusOfExpansionDto.h"
#include "FocusOfExpansionCalculator.h"
#include "Divergence.h"

#include <opencv2/imgproc.hpp>

#include <iostream>
#include <stdexcept>

using namespace std;

hw::FocusOfExpansionCalculator::FocusOfExpansionCalculator(const cv::Mat& collisionHistogram, const int windowSize, const int particles)
  : collisionHistogram(collisionHistogram)
  , windowSize(windowSize)
  , particles(particles) {

    if(windowSize < 2 || windowSize >= collisionHistogram.cols || windowSize >= collisionHistogram.rows) {
      throw std::invalid_argument("windowSize");
    }

    cv::integral(collisionHistogram, integralHistogram, CV_64F);
}


// -------------------------------------------------------------> x
// |
// |              B-A+1 = windowSize
// |               A         B
// |                x--------
// |                |       |  C-A+1 = windowSize
// |                |       |
// |                |       |
// |                ---------
// |               C        D
// |
// | cf. https://en.wikipedia.org/wiki/Summed_area_table
// |
// |
// |
// |
// y
double hw::FocusOfExpansionCalculator::getSumOfWindow(const int x, const int y, const int width, const int height) const {
    // the integral image has one additional zero row (first row) and one additional zero column (first column)
    // this is mainly for OPENCV performance reasons, see http://stackoverflow.com/a/30208807 for further information
    // therefore in order to query the integral image for indexes based on `collisionHistogram`, I have to add 1 on both query indexes
    auto idx = x+1;
    auto idy = y+1;

    auto D = integralHistogram.at<double>(cv::Point2i(idx + width, idy + height));
    auto B = integralHistogram.at<double>(cv::Point2i(idx + width, idy));
    auto C = integralHistogram.at<double>(cv::Point2i(idx      , idy + height));
    auto A = integralHistogram.at<double>(cv::Point2i(idx      , idy));

    return  D
          - B
          - C
          + A;
}

std::shared_ptr<hw::FocusOfExpansionDto> hw::FocusOfExpansionCalculator::determineFoe(){
  double maxSum = 0.0;
  int x = 0;
  int y = 0;

  for (int i = 0; i < collisionHistogram.rows - windowSize; ++i)
  {
    for (int j = 0; j < collisionHistogram.cols - windowSize; ++j)
    {
      auto sum = getSumOfWindow(j, i, windowSize, windowSize);
      if(sum > maxSum) {
        maxSum = sum;
        x = j;
        y = i;
      }
    }
  }

  auto foe = cv::Point2i(x + windowSize / 2, y + windowSize / 2);
  double numberOfInliers = getSumOfWindow(foe.x - windowSize / 2, foe.y - windowSize / 2, windowSize, windowSize);
  double numberOfParticles = integralHistogram.at<double>(cv::Point2i(integralHistogram.cols - 1, integralHistogram.rows - 1));
  
  return std::shared_ptr<hw::FocusOfExpansionDto>(new hw::FocusOfExpansionDto(foe, numberOfInliers, particles)); //numberOfParticles));
}
