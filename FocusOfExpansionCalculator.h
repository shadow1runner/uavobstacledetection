//
// Created by Helmut on 12/19/2015.
//

#ifndef OPTICALFLOW_FOCUSOFEXPANSION_H
#define OPTICALFLOW_FOCUSOFEXPANSION_H

#include <memory>
#include <opencv2/core.hpp>


namespace hw
{
    class FocusOfExpansionDto;

    class FocusOfExpansionCalculator
    {
    private:
        const cv::Mat& collisionHistogram;
        cv::Mat integralHistogram;
        const int windowSize;
        const int particles;

    public:
        FocusOfExpansionCalculator(const cv::Mat& collisionHistogram, const int windowSize, const int particles);
        std::shared_ptr<hw::FocusOfExpansionDto> determineFoe();
    private:
        double getSumOfWindow(const int x, const int y, const int width, const int height) const;
    };
}


#endif //OPTICALFLOW_FOCUSOFEXPANSION_H
