#include "FocusOfExpansionDto.h"

hw::FocusOfExpansionDto::FocusOfExpansionDto(const cv::Point2i& focusOfExpansion, double numberOfInliers, double numberOfParticles)
    : foe(focusOfExpansion)
    , numberOfInliers(numberOfInliers)
    , numberOfParticles(numberOfParticles) 
{}

const cv::Point2i& hw::FocusOfExpansionDto::getFoE() const {
    return foe;
}

double hw::FocusOfExpansionDto::getNumberOfInliers() const {
    return numberOfInliers;
}

double hw::FocusOfExpansionDto::getNumberOfParticles() const {
    return numberOfParticles;
}

/**
 * Returns the proportion of particles in the detection window and the total
 * amount of particles.
 * This is a very coarse approximation of the variance but by using the
 * already existing integral histogram it can be computed very fast.
 */
double hw::FocusOfExpansionDto::getInlierProportion() const {
    return numberOfInliers/numberOfParticles;
}
