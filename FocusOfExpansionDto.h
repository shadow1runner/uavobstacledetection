#ifndef OPTICALFLOW_FOCUSOFEXPANSIONDTO_H
#define OPTICALFLOW_FOCUSOFEXPANSIONDTO_H

#include <opencv2/core.hpp>

namespace hw {
    class FocusOfExpansionDto {
        cv::Point2i foe;
        double numberOfInliers;
        double numberOfParticles;

    public:
        FocusOfExpansionDto(const cv::Point2i& focusOfExpansion, double numberOfInliers, double numberOfParticles);

        const cv::Point2i& getFoE() const;
        double getNumberOfInliers() const;
        double getNumberOfParticles() const;
        double getInlierProportion() const;
    };
}


#endif // OPTICALFLOW_FOCUSOFEXPANSIONDTO_H
