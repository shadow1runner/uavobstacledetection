#include "FramePersister.h"
#include "CollisionAvoidanceSettings.h"
#include "DrawHelper.h"
#include "HeatMap.h"

#include <QDir>
#include <QDateTime>
#include <QDirIterator>

#include <string>

using namespace std;
using namespace cv;

FramePersister::FramePersister(CollisionAvoidanceSettings& settings, QObject *parent)
    : QObject(parent)
    , _settings(settings)
{
    clearFrameDirectoryIfSettingsEnabled();
}

void FramePersister::clearFrameDirectoryIfSettingsEnabled()
{
 	if(_settings.WriteToOutputEnabled && _settings.ClearOldFramesEnabled)
	{
		initDirectory();
	}
}

void FramePersister::findLastResetCount()
{
    QDirIterator it(QString(_settings.BaseFramesDirPath), QDir::Dirs | QDir::NoSymLinks | QDir::NoDot | QDir::NoDotDot);
	while (it.hasNext()) {
		try {
            QString str(it.next());
            QString subString(str.right(str.length()-_settings.BaseFramesDirPath.length())); // only get suffix

			auto integer = subString.toInt();
			if(integer > _lastResetCount){
				_lastResetCount = integer;
			}
        } catch(const std::exception&) {
		}
	}
}

void FramePersister::increaseResetCount()
{
	findLastResetCount();
    _lastResetCount++;
	initDirectory();
}

void FramePersister::initDirectory() {
    QString dir(_settings.BaseFramesDirPath + QString::number(_lastResetCount) + "/");
	checkOrMakeDirectory(dir);

	if(_settings.WriteRawFrames)
	{
		checkOrMakeDirectory(dir + _settings.RawFramesPath);
		// clearDirectory(dir + _settings.RawFramesPath);
	}
	if(_settings.WriteBadFrames)
	{
		checkOrMakeDirectory(dir + _settings.BadFramesPath);
		// clearDirectory(dir + _settings.BadFramesPath);
	}
	if(_settings.WriteGoodFrames)
	{
		checkOrMakeDirectory(dir + _settings.GoodFramesPath);
		// clearDirectory(dir + _settings.GoodFramesPath);
	}
	if(_settings.WriteHistogramFrames)
	{
		checkOrMakeDirectory(dir + _settings.HistogramFramesPath);
		// clearDirectory(dir + _settings.HistogramFramesPath);
	}
	if(_settings.WriteOpticalFlowFrames)
	{
		checkOrMakeDirectory(dir + _settings.OpticalFlowFramesPath);
		// clearDirectory(dir + _settings.OpticalFlowFramesPath);
	}
	if(_settings.WriteUiFrames)
	{
		checkOrMakeDirectory(dir + _settings.UiFramesPath);
		// clearDirectory(dir + _settings.UiFramesPath);
	}
}

QString FramePersister::getCurrentBaseString() {
	return _settings.BaseFramesDirPath + QString::number(_lastResetCount) + "/";
}

void FramePersister::rawFrameReady(const cv::Mat& rawFrame)
{
	_rawFrame = rawFrame;

	if(!_settings.WriteRawFrames)
		return;

	auto fileName = getCurrentBaseString() + _settings.RawFramesPath + QString::number(_rawCount++) + ".jpg";
    persistFrame(rawFrame, fileName);
}

void FramePersister::collisionLevelRatingReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence)
{
	Q_UNUSED(foeFiltered);
    Q_UNUSED(foe);
    Q_UNUSED(detectorResult);
	Q_UNUSED(lastDivergence);
	Q_UNUSED(avgDivergence);

	if(!_settings.WriteGoodFrames)
		return;

    auto fileName = getCurrentBaseString() + _settings.GoodFramesPath + QString::number(frameNumber) + ".jpg";
	persistFrame(frame, fileName);
}

void FramePersister::badFrameReady(const cv::Mat& badFrame, unsigned long long skipFrameCount, unsigned long long totalFrameCount, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured, double avgDivergence, const cv::Mat& convertedColorFrame)
{
	Q_UNUSED(convertedColorFrame);
	Q_UNUSED(skipFrameCount);
	Q_UNUSED(totalFrameCount);
	Q_UNUSED(foeMeasured);
	Q_UNUSED(avgDivergence);

	if(!_settings.WriteBadFrames)
		return;

	auto fileName = getCurrentBaseString() + _settings.BadFramesPath + QString::number(totalFrameCount) + ".jpg";
	persistFrame(badFrame, fileName);
}

void FramePersister::opticalFlowReady(const cv::Mat& opticalFlow)
{
	if(!_settings.WriteOpticalFlowFrames)
		return;

	// convert
    cv::Mat smallRawFrame = _rawFrame;
    if(_settings.SubsampleAmount!=0)
        cv::resize(_rawFrame,
                       smallRawFrame,
                       cv::Size(_rawFrame.cols / (2 * _settings.SubsampleAmount),
                                _rawFrame.rows / (2 * _settings.SubsampleAmount)),
                       0, 0, cv::INTER_NEAREST);

	auto fileName = getCurrentBaseString() + _settings.OpticalFlowFramesPath + QString::number(_opticalFlowCount) + ".jpg";
	auto rawOverlayFileName = _settings.OpticalFlowFramesPath + "raw_" + QString::number(_opticalFlowCount++) + ".jpg";
	auto hsvFileName = _settings.OpticalFlowFramesPath + "hsv_" + QString::number(_opticalFlowCount++) + ".jpg";
	
    cv::Mat flowOverlay(opticalFlow.size(), CV_8UC3, cv::Scalar(0,0,0));;
    DrawHelper::drawOpticalFlowMap(opticalFlow, flowOverlay, cv::Scalar(0, 255, 0), 16, _settings.OpticalFlowVectorVisualizationFactor, _settings.OpticalFlowVectorVisualizationFactor); // GREEN

    auto hsv = DrawHelper::visualiceFlowAsHsv(opticalFlow);
    DrawHelper::drawOpticalFlowMap(opticalFlow, smallRawFrame, cv::Scalar(0, 255, 0), 16, _settings.OpticalFlowVectorVisualizationFactor, _settings.OpticalFlowVectorVisualizationFactor); // GREEN

	persistFrame(flowOverlay, fileName);
	persistFrame(hsv, hsvFileName);
	persistFrame(smallRawFrame, fileName);
}

void FramePersister::histogramReady(const cv::Mat& histogram)
{
	if(!_settings.WriteHistogramFrames)
		return;

	auto fileName = getCurrentBaseString() + _settings.HistogramFramesPath + QString::number(_hCount++) + ".jpg";
	persistFrame(HeatMap::createHeatMap(histogram), fileName);
}

void FramePersister::uiFrameReady(const cv::Mat& frame, unsigned long long frameNumber)
{
	if(!_settings.WriteUiFrames)
		return;

	auto fileName = getCurrentBaseString() + _settings.UiFramesPath + QString::number(frameNumber) + ".jpg";
	persistFrame(frame, fileName);
}


void FramePersister::clearDirectory(const QString& path) const
{
	// aso http://stackoverflow.com/a/18409626
	QDir dir(path);
	dir.setNameFilters(QStringList() << "*.jpg");
	dir.setFilter(QDir::Files);
	foreach(QString dirFile, dir.entryList())
	{
	    dir.remove(dirFile);
	}
}

void FramePersister::checkOrMakeDirectory(const QString& path) const
{
    QDir dir(path);
	if (!dir.exists()) 
	{
		dir.mkpath(".");
	}
}   

void FramePersister::persistFrame(const cv::Mat& frame, QString& path)
{
	if(!_settings.WriteToOutputEnabled)
		return;

    cv::imwrite(path.toStdString(), frame);
}

void FramePersister::persistFrame(const cv::Mat& frame, QString& path, std::vector<std::string>& lines)
{
	Mat tmp = frame;
	if(lines.size()>0)
		tmp = DrawHelper::renderText(frame, lines);

    persistFrame(tmp, path);
}
