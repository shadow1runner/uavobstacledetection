#ifndef FRAMEPERSISTER_H
#define FRAMEPERSISTER_H

#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include <memory>
#include <string>
#include <vector>

#include <QObject>

#include "FocusOfExpansionDto.h"
#include "Divergence.h"
#include "CollisionDetectorResult.h"

class CollisionAvoidanceSettings;

class FramePersister : public QObject
{
    Q_OBJECT
public:
    explicit FramePersister(CollisionAvoidanceSettings& settings, QObject *parent = 0);
    void clearFrameDirectoryIfSettingsEnabled();
    void increaseResetCount();
    void initDirectory();


public slots:
	void rawFrameReady(const cv::Mat& rawFrame);

    void collisionLevelRatingReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence);

    void badFrameReady(const cv::Mat& badFrame, unsigned long long skipFrameCount, unsigned long long totalFrameCount, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured, double avgDivergence, const cv::Mat& convertedColorFrame);

    void opticalFlowReady(const cv::Mat& opticalFlow);

    void histogramReady(const cv::Mat& histogram);

    void uiFrameReady(const cv::Mat& frame, unsigned long long frameNumber);

private:
	CollisionAvoidanceSettings& _settings;

    unsigned long _lastResetCount = 0;

    unsigned long long _rawCount=0;    
    unsigned long long _opticalFlowCount=0;
    unsigned long long _hCount=0;  

    cv::Mat _rawFrame;

    QString getCurrentBaseString();
    void findLastResetCount();
    void clearDirectory(const QString& path) const;
    void checkOrMakeDirectory(const QString& path) const;
    void persistFrame(const cv::Mat& frame, QString& fileName);
    void persistFrame(const cv::Mat& frame, QString& fileName, std::vector<std::string>& lines);
};

#endif // FRAMEPERSISTER_H
