//
// Created by Helmut on 1/4/2016.
//

#include "Kalman.h"

#include <iostream>
#include <sstream> 

hw::Kalman::Kalman()
    : filter(2,2,0) // state: (x,y) -> 2-d; measurement: (x,y) -> 2-d; 0 -> no control unit needed (no feedback control)
    , _isInitialized(false)
{
    // the measurement gets incorporated 1:1, i.e. no calculations (like derivatives for velocity) are necessary
    // | 1  0 | * (x, y)' = (x, y)`
    // | 0  1 |             
    cv::setIdentity(filter.transitionMatrix); // A
    cv::setIdentity(filter.measurementMatrix); // H

    cv::setIdentity(filter.processNoiseCov); // Q
    cv::setIdentity(filter.measurementNoiseCov); // R

    cv::setIdentity(filter.errorCovPost, cv::Scalar::all(0.)); // P <- shouldn't be necessary, or?
}


cv::Point2f hw::Kalman::Mat2Point(const cv::Mat& mat) {
    return cv::Point2f(mat.at<float>(0),mat.at<float>(1));
}

cv::Mat hw::Kalman::Point2Mat(const cv::Point2f& pt) {
    return (cv::Mat_<float>(2,1) << pt.x, pt.y);
}

cv::Point2f hw::Kalman::predict() {
    auto predicted = filter.predict();
    return Mat2Point(predicted);
}

cv::Point2f hw::Kalman::correct(const cv::Point2f& measurement, const double inlierProportion, const double covScale) {
    // taken from Sebastian's source:
    cv::setIdentity(filter.measurementNoiseCov, cv::Scalar::all(inlierProportion/covScale)); // R
    cv::Mat estimatedMat = filter.correct(Point2Mat(measurement));
    auto estimated = Mat2Point(estimatedMat);

    return estimated;
}

void hw::Kalman::setState(const cv::Point2f& newState) {
    auto mat = Point2Mat(newState);
    filter.statePre = mat; // x'
    filter.statePost = mat; // x
    _isInitialized = true;
}

bool hw::Kalman::isInitialized() const {
    return _isInitialized;
}

void hw::Kalman::reset() {
    _isInitialized = false;
}