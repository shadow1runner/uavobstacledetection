//
// Created by Helmut on 1/4/2016.
//

#ifndef OPTICALFLOW_KALMAN_H
#define OPTICALFLOW_KALMAN_H

#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>


namespace hw {
    // see http://www.bzarg.com/p/how-a-kalman-filter-works-in-pictures/#mathybits
    // and https://en.wikipedia.org/wiki/Kalman_filter
    // as well as https://en.wikipedia.org/wiki/Kalman_filter
    class Kalman
    {
    private:
        cv::KalmanFilter filter;
        bool _isInitialized;

        cv::Point2f Mat2Point(const cv::Mat& mat);
        cv::Mat Point2Mat(const cv::Point2f& pt);

    public:
        Kalman();
        cv::Point2f predict();
        cv::Point2f correct(const cv::Point2f& measurement, const double inlierProportion, const double covScale = 0.2);
        void setState(const cv::Point2f& newState);
        bool isInitialized() const;
        void reset();
    };
}

#endif //OPTICALFLOW_KALMAN_H
