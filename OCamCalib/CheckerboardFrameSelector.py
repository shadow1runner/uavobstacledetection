import numpy as np
import cv2

# source settings
# use an integer (0, 1 etc.) to specify a capture device (camera), a string to specify a recorded video
source = "../Fisheye/temp/ir.avi"
# source = 1
# destination settings
targetPath = "./Scaramuzza_OCamCalib_v3.0_win/fisheyeA3_" # dir has to exist!
targetPath = "./video/a3/analogFisheyeA3_" # dir has to exist!
extension  = ".jpg"

frameCount = 0

cap = cv2.VideoCapture(source)

print('Usage:\nESC/q: quit\ns: save frame')

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    if ret==False:
        print('No more frames to read from, exiting.')
        break;

    # Display the resulting frame
    cv2.imshow('frame',frame)

    key = cv2.waitKey(20) # -1)
    if key in [ord('q'), 1048689, 1048603]:
        break
    else: #if key in [ord('s'), 1048691]:
        print('Saving frame #' + str(frameCount) + '.')
        cv2.imwrite(targetPath + str(frameCount) + extension, frame);
        frameCount = frameCount+1
    # else:
    #     print('Invalid input: ' + str(key) + "\n")

cap.release()
cv2.destroyAllWindows()