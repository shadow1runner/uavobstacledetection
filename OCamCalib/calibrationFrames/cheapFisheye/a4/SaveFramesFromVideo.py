import numpy as np
import cv2

# source settings
# use an integer (0, 1 etc.) to specify a capture device (camera), a string to specify a recorded video
source = "./a4.avi"
# destination settings
targetPath = "./fisheyeA4_" # dir has to exist!
extension  = ".jpg"

frameCount = 0

cap = cv2.VideoCapture(source)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    if ret==False:
        print('No more frames to read from, exiting.')
        break;

    print('Saving frame #' + str(frameCount) + '.')
    cv2.imwrite(targetPath + str(frameCount) + extension, frame);
    frameCount = frameCount+1

cap.release()
cv2.destroyAllWindows()