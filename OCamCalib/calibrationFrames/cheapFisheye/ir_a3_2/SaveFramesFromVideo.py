import numpy as np
import cv2

# source settings
# use an integer (0, 1 etc.) to specify a capture device (camera), a string to specify a recorded video
source = "./ir_a3.avi"
# destination settings
targetPath = "./extracted/" # dir has to exist!
extension  = ".jpg"

# resize settings
# the boscam records to the SD card in 1280 x 720 -- these frames are used for calibration
# FPV has a resolution of               720 x 480 -- undistort does only work with the same resolutions
resizeSize = (720, 480) # (width, height)

frameCount = 0

cap = cv2.VideoCapture(source)

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    if ret==False:
        print('No more frames to read from, exiting.')
        break;

    frame = cv2.resize(frame, resizeSize)

    print('Saving frame #' + str(frameCount) + '.')
    cv2.imwrite(targetPath + str(frameCount) + extension, frame);
    frameCount = frameCount+1

cap.release()
cv2.destroyAllWindows()