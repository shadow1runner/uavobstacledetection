QT += core
QT += widgets
#QT -= gui

CONFIG += c++11

TARGET = OpticalFlow
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app


##################
# opencv - http://stackoverflow.com/a/15890266
#INCLUDEPATH += "E:\\temp\\opencv\\minGwBuild\\install\\include" \
#               "E:\\temp\\opencv\\minGwBuild\\install\\include\\opencv" \
#               "E:\\temp\\opencv\\minGwBuild\\install\\include\\opencv2"

INCLUDEPATH += \
    "/usr/include/" \
    "/usr/include/opencv" \
    "/usr/include/opencv2" \
    "/usr/local/include/" \
    "/usr/local/include/opencv" \
    "/usr/local/include/opencv2"

LIBS += \
    -L/usr/local/lib/ \
    -lopencv_core \
    -lopencv_highgui \
    -lopencv_imgproc \
    -lopencv_features2d \
    -lopencv_calib3d \
    -lopencv_flann \
    -lopencv_imgcodecs \
    -lopencv_ml \
    -lopencv_objdetect \
    -lopencv_photo \
    -lopencv_shape \
    -lopencv_stitching \
    -lopencv_superres \
    -lopencv_video \
    -lopencv_videoio \
    -lopencv_videostab
    #-lopencv_optflow


##################
# boost - http://stackoverflow.com/a/16998798
DEFINES += BOOST_THREAD_USE_LIB
#DEFINES += BOOST_THREAD_POSIX #https://svn.boost.org/trac/boost/ticket/5964
#DEFINES += BOOST_THREAD_LINUX
DEFINES += BOOST_SYSTEM_NO_DEPRECATED # http://stackoverflow.com/a/18877

INCLUDEPATH += /usr/include/boost   #E:\\dev\\boost_1_60_0
LIBS += -L/usr/lib/ \ #"-LE:/dev/boost_1_60_0/stage/lib/" \
    -lboost_system \
    -lboost_thread \
    -lboost_date_time \
    -lboost_program_options \
    -lboost_filesystem \
    -lboost_regex

# http://stackoverflow.com/a/1782435; winsock aso http://stackoverflow.com/a/18445, needed for boost::asio::io_service (used in multi-threading)
# LIBS += -lws2_32

##################
INCLUDEPATH += \
    $$PWD/libs/Eigen
##################

SOURCES += \
#    main.cpp \
     # AffineDivergenceTest.cpp \
    DivergenceComparisonTests.cpp \
    BufferedFrameGrabber.cpp \
    FocusOfExpansionCalculator.cpp \
    FocusOfExpansionDto.cpp \
    FramePersister.cpp \
    Kalman.cpp \
    OwnFlow.cpp \
    RandomCollider.cpp \
    helper/DrawHelper.cpp \
    helper/HeatMap.cpp \
    helper/QtHelper.cpp \
    Converter.cpp \
    Divergence.cpp \
    AffineDivergence.cpp \
    DivergenceHistory.cpp \
    CollisionDetector.cpp \
    CollisionAvoidanceSettings.cpp \
    OCamCalib/undistortFunctions/ocam_functions.cpp \
    CollisionLevel.cpp \
    CollisionDetectorResult.cpp

HEADERS += \
    BufferedFrameGrabber.h \
    CircularBuffer.h \
    config.h \
    FocusOfExpansionCalculator.h \
    FocusOfExpansionDto.h \
    FramePersister.h \
    Kalman.h \
    OwnFlow.h \
    RandomCollider.h \
    helper/DrawHelper.h \
    helper/HeatMap.h \
    helper/Macros.h \
    helper/StopWatch.h \
    helper/AvgWatch.h \
    helper/QtHelper.h \
    Converter.h \
    helper/Displayer.h \
    helper/ConsoleDisplayer.h \
    helper/ThreadWarmupHelper.h \
    Divergence.h \
    AffineDivergence.h \
    DivergenceHistory.h \
    CollisionDetector.h \
    CollisionLevel.h \
    CollisionAvoidanceSettings.h \
    OCamCalib/undistortFunctions/ocam_functions.h \
    RoiBuilder.h \
    CollisionLevel.h \
    CollisionDetectorResult.h


INCLUDEPATH += \
    $$PWD/ \
    $$PWD/helper \
    $$PWD/OCamCalib/undistortFunctions 
