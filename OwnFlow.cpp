//
// Created by Helmut on 1/5/2016.
//

#include "OwnFlow.h"
#include "RandomCollider.h"
#include "FocusOfExpansionDto.h"
#include "FocusOfExpansionCalculator.h"
#include "CollisionAvoidanceSettings.h"

#include "helper/StopWatch.h"
#include "helper/ThreadWarmupHelper.h"

//#include <opencv2/optflow.hpp>
#include <opencv2/video.hpp>

#include <QDebug>
#include <QThread>
#include <QThreadPool>
#include <QDateTime>

using namespace std;

hw::OwnFlow::OwnFlow(CollisionAvoidanceSettings& settings, const RoiBuilder* const roiBuilder)
    : QObject(NULL)
    , _settings              (settings)
    , threadPool             (new QThreadPool(this))
    , collisionDetector      (settings, roiBuilder)
    , opticalFlowWatch       (std::shared_ptr<AvgWatch> (new AvgWatch ("Farneback")))
    , colliderWatch          (std::shared_ptr<AvgWatch> (new AvgWatch ("Colliding")))
    , foeWatch               (std::shared_ptr<AvgWatch> (new AvgWatch ("FoE")))
    , kalmanWatch            (std::shared_ptr<AvgWatch> (new AvgWatch ("Kalman")))
    , collisionDetectorWatch (std::shared_ptr<AvgWatch> (new AvgWatch ("Divergence")))
    , allWatch               (std::shared_ptr<AvgWatch> (new AvgWatch ("ALL")))
{
    // warm up thread pool/set preferred thread size
    threadPool->setExpiryTimeout(-1); // threads should never time out
    for(auto i=0; i<QThread::idealThreadCount(); ++i) {
        threadPool->start(new ThreadWarmupHelper());
    }
    threadPool->waitForDone();

    divergenceHistory = collisionDetector.getDivergenceHistory();

    // kfFile.open("/home/helli/d/m/MscThesis/images/results/kalman/mscKfComparison.csv", ofstream::out | ofstream::trunc);
    // if(!kfFile.is_open())
    // {
    //     throw new std::invalid_argument("./ca/peformance.csv");
    // }
}

hw::OwnFlow::~OwnFlow() {
    threadPool->waitForDone(10); // grace period
//    delete threadPool; // forces ASSERT failure in QCoreApplication::sendEvent: "Cannot send events to objects owned by a different thread. Current thread 1a0fc4d8. Receiver '' (of type 'hw::OwnFlow') was created in thread 96fde4", file kernel\qcoreapplication.cpp, line 553 ?!

//    kfFile.close();
}

void hw::OwnFlow::reset() {
    totalFrameCount = 0;
    skipFrameCount = 0;
    kalman.reset();
    collisionDetector.reset();
    processIncomingFrames = true;
}


void hw::OwnFlow::step() {
    qDebug() << "Frame #" << totalFrameCount;
    histogram = cv::Mat(currentFrame.rows, currentFrame.cols, CV_64F, double(0));

    // a note on multi-threading:
    //   utilizing multiple CPU cores only makes sense if `frameGrabber` has a buffer size > 1
    //   otherwise multiple threads have a bigger overhead and it's not worth the effort, i.e. if one
    //   processes frame by frame, multi-threading makes not that much sense :(
    //   Furthermore, parallelizing multiple threads for collisions (in `RandomCollider`) doesn't bring any improvement
    //   either, see cset 34e15aba0c8c12907b5038a3a37d70a75e2f42a5
//  threadPool.push_job(boost::bind(calculateFlowAndCollide, this, boost::cref(currentFrame), frame)); // <- multithreaded
    calculateFlow(currentFrame); // single-threaded
    collide();
//  threadPool.waitForAll(); // <- multithreaded

    foeWatch->startTick();
    FocusOfExpansionCalculator foeCalculator(histogram, _settings.WindowSize, _settings.Particles);
    measuredFoe = foeCalculator.determineFoe();
    foeWatch->stopTick();

    qDebug() << "FoE: (" << measuredFoe->getFoE().x << ", " << measuredFoe->getFoE().y << "), \tInliers: " << measuredFoe->getNumberOfInliers() << " / " << measuredFoe->getNumberOfParticles() << "  " << measuredFoe->getInlierProportion()*100 << "\%";

//    kfFile << "Unfiltered;0;" << totalFrameCount << ";" << measuredFoe->getFoE().x/(double)currentFrame.cols << ";" << measuredFoe->getFoE().y/(double)currentFrame.rows << ";" << endl;
    
    if(_settings.InlierProportionThresholdEnabled && measuredFoe->getInlierProportion() < _settings.InlierProportionThreshold) {
        ++skipFrameCount;
        qDebug() << "Skipping due to low inlier ratio: " << measuredFoe->getInlierProportion() << " < " << _settings.InlierProportionThreshold;
        collisionDetector.reportSkippedFrame();
        emit frameSkipped(currentFrame, skipFrameCount, totalFrameCount, measuredFoe, divergenceHistory->getMovingAverageDivergence(), convertedColorFrame);
        return; // skip the rest (KF, div etc.)
    }

    kalmanWatch->startTick();
    cv::Point2i kalmanFoe;
    if(!kalman.isInitialized()) {
        kalman.setState(measuredFoe->getFoE());
        kalmanSebastian.setState(measuredFoe->getFoE());
        // return;
        kalmanFoe = measuredFoe->getFoE();
        kalmanWatch->stopTick();
        return;
    }

    kalman.predict();
    kalmanFoe = kalman.correct(measuredFoe->getFoE(), 1e5*measuredFoe->getInlierProportion(), 1);
    kalmanWatch->stopTick();

    // kalmanSebastian.predict();
    // auto kalmanFoeSebastian = kalmanSebastian.correct(measuredFoe->getFoE(), 1/measuredFoe->getInlierProportion(), 1);
    // kfFile << "Proposed;2;" << totalFrameCount << ";" << kalmanFoe.x/(double)currentFrame.cols << ";" << kalmanFoe.y/(double)currentFrame.rows << ";" << endl;
    // kfFile << "Stabinger;1;" << totalFrameCount << ";" << kalmanFoeSebastian.x/(double)currentFrame.cols << ";" << kalmanFoeSebastian.y/(double)currentFrame.rows << ";" << endl;

    collisionDetectorWatch->startTick();
    auto detectorResult = collisionDetector.detectCollision(kalmanFoe, opticalFlow, totalFrameCount);
    collisionDetectorWatch->stopTick();

    if(detectorResult->getCollisionLevel() >= CollisionLevel::high)
    {
        qDebug() << "emitting collisionImmanent -- collisionLevel is too high";
        emit collisionImmanent(currentFrame, totalFrameCount, std::shared_ptr<cv::Point2i>(new cv::Point2i(kalmanFoe)), measuredFoe, detectorResult, collisionDetector.getLastDivergence(), divergenceHistory->getMovingAverageDivergence());
        processIncomingFrames = false;
    }

    emit collisionLevelRatingReady(currentFrame, totalFrameCount, std::shared_ptr<cv::Point2i>(new cv::Point2i(kalmanFoe)), measuredFoe, detectorResult, collisionDetector.getLastDivergence(), divergenceHistory->getMovingAverageDivergence(), convertedColorFrame);
}

void hw::OwnFlow::calculateFlow(const cv::Mat& frame) {
    // calculate using a Mat instead of a UMat - GPU version yields different results, cf. flow_test optical_flow_farneback_gpu_yields_different_results_than_cpu <-> optical_flow_farneback_cpu and check-in e1b770d2a072ecd50b2eecab44cdb2f1d9804ca9
    opticalFlowWatch->startTick();
    cv::calcOpticalFlowFarneback(baseFrame, frame, opticalFlow, 0.5, 3, 10, 10, 7, 1.5, 0);
    opticalFlowWatch->stopTick();
    emit opticalFlowChanged(opticalFlow);
}


void hw::OwnFlow::collide() {
    colliderWatch->startTick();
    for(auto i=0; i<QThread::idealThreadCount(); ++i) {
        auto* collider = new hw::RandomCollider(opticalFlow, _settings.Particles/QThread::idealThreadCount(), histogram);
        threadPool->start(collider);
    }
    threadPool->waitForDone();
    colliderWatch->stopTick();
    emit histogramChanged(histogram);
}

const cv::Mat& hw::OwnFlow::getHistogram() const{
    return histogram;
}

const cv::Mat& hw::OwnFlow::getCurrentFrame() const{
    return currentFrame;
}

const cv::Mat& hw::OwnFlow::getOpticalFlow() const {
    return opticalFlow;
}

bool hw::OwnFlow::processesIncomingFrames() const {
    return processIncomingFrames;
}


void hw::OwnFlow::processImage(const cv::Mat& frame, const cv::Mat& convertedColorFrame) {
    if(!processIncomingFrames){
        qWarning() << "OwnFlow is paused - no incoming frames are processed";
        return;
    }

    allWatch->startTick();

    qDebug() << "#" << totalFrameCount;
    
    if(!hasBaseFrame) {
        baseFrame = frame;
        hasBaseFrame = true;
        allWatch->stopTick();
        return;
    }
    currentFrame = frame;
    this->convertedColorFrame = convertedColorFrame;
    step();
    baseFrame = frame;
    
    ++totalFrameCount;

    allWatch->stopTick();

    emit collisionAvoidanceFrameTimingsReady(allWatch, colliderWatch, collisionDetectorWatch, foeWatch, kalmanWatch, opticalFlowWatch);
}
