//
// Created by Helmut on 1/5/2016.
//

#ifndef OPTICALFLOW_OWNFLOW_H
#define OPTICALFLOW_OWNFLOW_H

#include "BufferedFrameGrabber.h"
//#include "ThreadPool.h"
#include "Kalman.h"
#include "Divergence.h"
#include "CollisionDetector.h"
#include "helper/AvgWatch.h"

#include <memory>

#include <opencv2/core.hpp>

#include <QObject>
#include <QThreadPool>

#include <iostream>
#include <fstream>

class CollisionAvoidanceSettings;
class QGCToolbox;
class RoiBuilder;

namespace hw {
    class FocusOfExpansionDto;

    class OwnFlow : public QObject
    {
        Q_OBJECT
        const CollisionAvoidanceSettings& _settings;
        
        QThreadPool* const threadPool;
        hw::Kalman kalman;
        hw::Kalman kalmanSebastian;
        hw::CollisionDetector collisionDetector;
        const hw::DivergenceHistory* divergenceHistory = NULL;

        std::shared_ptr<hw::FocusOfExpansionDto> measuredFoe;
        std::shared_ptr<hw::FocusOfExpansionDto> foe;
        std::shared_ptr<hw::Divergence> divergence;

        cv::Mat histogram;
        bool hasBaseFrame=false;
        cv::Mat baseFrame;
        cv::Mat currentFrame;
        cv::Mat convertedColorFrame;
        cv::Mat opticalFlow;

        // performance measurements
        std::shared_ptr<AvgWatch> opticalFlowWatch;
        std::shared_ptr<AvgWatch> colliderWatch;
        std::shared_ptr<AvgWatch> foeWatch;
        std::shared_ptr<AvgWatch> kalmanWatch;
        std::shared_ptr<AvgWatch> collisionDetectorWatch;
        std::shared_ptr<AvgWatch> allWatch;

        // statistics
        unsigned long long skipFrameCount = 0;
        unsigned long long totalFrameCount = 0;

//        hw::ThreadPool<void> threadPool;

        void calculateFlow(const cv::Mat& frame);
        void collide();

        bool processIncomingFrames = true; // false if e.g. paused

        // std::ofstream kfFile;
    public:
        OwnFlow(CollisionAvoidanceSettings& settings, const RoiBuilder* const roiBuilder);
        ~OwnFlow();
        Q_INVOKABLE void reset();

        void step();

        const cv::Mat& getHistogram() const;
        const cv::Mat& getCurrentFrame() const;
        const cv::Mat& getOpticalFlow() const;

        bool processesIncomingFrames() const;

    public slots:
        void processImage(const cv::Mat& frame, const cv::Mat& convertedColorFrame);

    signals:
        void opticalFlowChanged(const cv::Mat& opticalFlow);
        
        void histogramChanged(const cv::Mat& histogram);

        void frameSkipped(const cv::Mat& currentFrame, unsigned long long skipFrameCount, unsigned long long totalFrameCount, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured, double avgDivergence, const cv::Mat& convertedColorFrame);

        void collisionLevelRatingReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence, const cv::Mat& convertedColorFrame);

        void collisionImmanent(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence);

        void collisionAvoidanceFrameTimingsReady(std::shared_ptr<AvgWatch> allWatch, std::shared_ptr<AvgWatch> colliderWatch, std::shared_ptr<AvgWatch> collisionDetectorWatch, std::shared_ptr<AvgWatch> foeWatch, std::shared_ptr<AvgWatch> kalmanWatch, std::shared_ptr<AvgWatch> opticalFlowWatch);

    };
}

#endif //OPTICALFLOW_OWNFLOW_H
