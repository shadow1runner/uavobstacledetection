//
// Created by Helmut on 12/21/2015.
//

#include "RandomCollider.h"

#include <iostream>

#include <QThreadPool>

hw::BaseCollider::BaseCollider(const cv::Mat& opticalFlow, cv::Mat& collidingHistogram)
    : opticalFlow(opticalFlow)
    , collidingHistogram(collidingHistogram)
{
}

// adapted from http://stackoverflow.com/a/1968345/2559632
void hw::BaseCollider::collidePoints(const cv::Point2i& fst, const cv::Point2i& snd, cv::Mat& collidingHistogram) {
    const cv::Point2f& v1 = opticalFlow.at<cv::Point2f>(fst);
    const cv::Point2f& v2 = opticalFlow.at<cv::Point2f>(snd);

    double s, t;
    auto denominator = -v2.x * v1.y + v1.x * v2.y;                        // might be 0...
    s = (-v1.y * (fst.x - snd.x) + v1.x * (fst.y - snd.y)) / denominator; // ... if so, this is NaN
    t = ( v2.x * (fst.y - snd.y) - v2.y * (fst.x - snd.x)) / denominator; // ... this as well
    if(std::isnan(s) || std::isnan(t)) 
    {
        return; // ... division through zero, http://stackoverflow.com/a/4745343
    }

    if (s > 0 || t > 0)
    {
        // Due to the way the optical flow vectors are determined, only 'negative' collisions are of interest
        // e.g. if we move forward to an object, the collision (if any) has to be in front
        // without this check, the actual direction of the vectors would not be considered (as in Sebastian's original version)
        return;
    }

    cv::Point2i inc((int)(fst.x + t * v1.x), (int)(fst.y + t * v1.y));

    if (inc.x >= 0 && inc.x < opticalFlow.cols &&
        inc.y >= 0 && inc.y < opticalFlow.rows) 
    {
        collidingHistogram.at<double>(inc) += 1; // thread safety is provided by OpenCV: http://stackoverflow.com/a/10667611
    }
}

hw::RandomCollider::RandomCollider(const cv::Mat& opticalFlow, const unsigned long long numberOfParticles, cv::Mat& collidingHistogram)
    : BaseCollider(opticalFlow, collidingHistogram)
    , numberOfParticles(numberOfParticles)
    , row_distribution(0, opticalFlow.rows-1) // ranges are inclusive, cf. http://en.cppreference.com/w/cpp/numeric/random/uniform_int_distribution
    , col_distribution(0, opticalFlow.cols-1)
{}

void hw::RandomCollider::run() {
    for(unsigned long long i=0; i<numberOfParticles; ++i) {
        auto fst = cv::Point2i(col_distribution(generator), row_distribution(generator));
        auto snd = cv::Point2i(col_distribution(generator), row_distribution(generator));
     
        collidePoints(fst, snd, collidingHistogram);
    }
}

hw::FullCollider::FullCollider(const cv::Mat& opticalFlow, cv::Mat& collidingHistogram)
    : BaseCollider(opticalFlow, collidingHistogram)
{}

void hw::FullCollider::run() {
    const auto width = opticalFlow.cols;
    const auto height = opticalFlow.rows;

    for (int y1 = 0; y1 < height; ++y1)
    {
        for (int x1 = 0; x1 < width; ++x1)
        {
            for (int y2 = 0; y2 < height; ++y2)
            {
                for (int x2 = 0; x2 < width; ++x2)
                {
                    auto fst = cv::Point2i(x1, y1);
                    auto snd = cv::Point2i(x2, y2);

                    collidePoints(fst, snd, collidingHistogram);
                }
            }
        }
    }
}
