//
// Created by Helmut on 12/21/2015.
//

#ifndef OPTICALFLOW_RANDOMCOLIDER_H
#define OPTICALFLOW_RANDOMCOLIDER_H


#include <opencv2/core.hpp>

#include <random>

#include <QRunnable>

namespace hw {

    template <class T>
    class ThreadPool;

    class BaseCollider : public QRunnable
    {
    protected:
        const cv::Mat& opticalFlow;
        cv::Mat& collidingHistogram;

        void collidePoints(const cv::Point2i& fst, const cv::Point2i& snd, cv::Mat& collidingHistogram);
        
    public:
        BaseCollider(const cv::Mat& opticalFlow, cv::Mat& collidingHistogram);
        virtual void run()=0; // result is stored in @collidingHistogram, for multi-threading
    };

    class RandomCollider : public BaseCollider
    {
    private:
        const unsigned long long numberOfParticles;

        // aso http://stackoverflow.com/a/10710875
        // std::mt19937 generator;            // takes per-frame avg 83,926 ms on ahead.avi (subsampling 4) with almost same results
        std::default_random_engine generator; // takes per-frame avg 63,306 ms on ahead.avi (subsampling 4) with almost same results
        std::uniform_int_distribution<> row_distribution;
        std::uniform_int_distribution<> col_distribution;

    public:
        RandomCollider(const cv::Mat& opticalFlow, const unsigned long long numberOfParticles, cv::Mat& collidingHistogram);
        void run();
    };

    // just a class used for debugging, it collides all possible positions;
    // this ensures the best comparability to the scala code by Sebastian
    // note, however, that this code is very imperformant (O(N^4)) and thus should be used with caution in production code
    class FullCollider : public BaseCollider
    {
    public:
        FullCollider(const cv::Mat& opticalFlow, cv::Mat& collidingHistogram);
        void run();
    };
}



#endif //OPTICALFLOW_RANDOMCOLIDER_H
