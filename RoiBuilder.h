#ifndef OPTICALFLOW_ROIBUILDER_H
#define OPTICALFLOW_ROIBUILDER_H

#include <opencv2/core.hpp>
#include <QObject>

// beware: DUMMY IMPLEMENTATION ONLY!
class RoiBuilder : public QObject
{
	Q_OBJECT
	
public:
	RoiBuilder(QObject *parent=NULL)
		: QObject(parent)
	{
	}
signals:
    void roiReady(const cv::Rect& roi, double rollDegree, double pitchDegree, double climbRate);
};

#endif // OPTICALFLOW_ROIBUILDER_H
