#include "UiFramePreparer.h"

#include "MultiVehicleManager.h"
#include "Vehicle.h"
#include "QGCApplication.h"

#include <string>
#include <sstream>

using namespace std;
using namespace hw;

hw::UiFramePreparer::UiFramePreparer(QObject* parent)
    : QObject(parent)
    , _settings(CollisionAvoidanceSettings::getInstance()){
}

void hw::UiFramePreparer::collisionLevelRatingReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence, const cv::Mat& convertedColorFrame)
{
  Q_UNUSED(divergence);

  _uiMat = renderGoodFrame(frame, convertedColorFrame, frameNumber, foeFiltered, foe, detectorResult, lastDivergence, avgDivergence);

  emit uiFrameReady(_uiMat, frameNumber);
}    

void hw::UiFramePreparer::badFrame(const cv::Mat& badFrame, unsigned long long skipFrameCount, unsigned long long totalFrameCount, std::shared_ptr<hw::FocusOfExpansionDto> foe, double avgDivergence, const cv::Mat& convertedColorFrame)
{
  Q_UNUSED(skipFrameCount);
  Q_UNUSED(totalFrameCount);
  Q_UNUSED(avgDivergence);

  if(!_settings.DisplayBadFramesInUi)
    return;

  _uiMat = renderBadFrame(badFrame, convertedColorFrame, totalFrameCount, foe);

  emit uiFrameReady(_uiMat, totalFrameCount);
}

void hw::UiFramePreparer::opticalFlowReady(const cv::Mat& opticalFlow)
{
  this->opticalFlow = opticalFlow;
}

void hw::UiFramePreparer::histogramReady(const cv::Mat& histogram)
{
  heatMap = HeatMap::createHeatMap(histogram);
}

void hw::UiFramePreparer::collisionImmanent(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence)
{
}

void hw::UiFramePreparer::roiReady(const cv::Rect& roi, double rollDegree, double pitchDegree, double climbRate)
{
  _roi = roi;
  _rollDegree = rollDegree;
  _pitchDegree = pitchDegree;
  _climbRate = climbRate;
}

cv::Mat hw::UiFramePreparer::renderGoodFrame(const cv::Mat& procFrame, const cv::Mat& convertedColorFrame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence)
{
  std::vector<cv::Mat> canvas;
  auto collisionLevel = detectorResult->getCollisionLevel();

  cv::Scalar color = MEASURED_FOE_COLOR;

  // 0.) convertedColorFrame
  auto frame = convertedColorFrame;
  cv::Mat empty(frame.size(), frame.type(), cv::Scalar::all(64));
  canvas.push_back(empty);

  // 1.) grayscale raw with image and lense center and actual FOE, ROI(?)
  if(_settings.RoiEnabled) {
    if(_settings.ocamModelValid()) {
      auto& ocamModel = _settings.getOcamModel();
      DrawHelper::drawCross(frame, ocamModel.getCroppedFrameCenter(), "I");
      DrawHelper::drawCross(frame, ocamModel.getCroppedLensCenter(), "L", cv::Scalar(122, 160, 255));
    }

    DrawHelper::visualizeRoi(frame, _roi);

    if(!_settings.UseRoiSolelyForDescendDetection && !_roi.contains(*foeFiltered)) {
        DrawHelper::drawRings(frame, *foeFiltered, cv::Scalar::all(255), (int)collisionLevel);
    } else {
        DrawHelper::drawRings(frame, *foeFiltered, collisionLevel);
    }
  } else {
    DrawHelper::drawRings(frame, *foeFiltered, collisionLevel);
  }
  DrawHelper::drawRings(frame, foe->getFoE(), color, 1);
  canvas.push_back(frame);

  // 2.) heatmap with OF overlay
  DrawHelper::drawRings(heatMap, foe->getFoE(), color);
  DrawHelper::drawOpticalFlowMap(opticalFlow, heatMap, GREEN, 16, _settings.OpticalFlowVectorVisualizationFactor, _settings.OpticalFlowVectorVisualizationFactor);
  canvas.push_back(heatMap);

  // 3.) empty
  canvas.push_back(empty);

  auto combined = DrawHelper::makeRowCanvas(canvas, cv::Scalar(64, 64, 64));

  std::vector<std::string> lines;
  std::stringstream ss;
  ss << "#" << frameNumber << ": " << detectorResult->getEvaluationResultText();
  lines.push_back(ss.str()); ss.str(std::string());

  ss << hw::CollisionLevelHelper::toString(collisionLevel) << " (" << std::setprecision(3) << lastDivergence*100 << "\% @ (" << foeFiltered->x << "," << foeFiltered->y << ")/" << std::setprecision(3) << avgDivergence*100 << "\% avg)";
  lines.push_back(ss.str()); ss.str(std::string());

  ss << "Climb Rate: " << std::setprecision(3) << _climbRate << "m/s, Threshold: " << std::setprecision(3) << _settings.ClimbRateValueThreshold;
  lines.push_back(ss.str()); ss.str(std::string());

  ss << "Inlier Ratio: " << std::setprecision(3) << foe->getInlierProportion()*100 << "\%";
  lines.push_back(ss.str()); ss.str(std::string());

  return DrawHelper::renderText(combined, lines);
}  

cv::Mat hw::UiFramePreparer::renderBadFrame(
  const cv::Mat& badFrame,
  const cv::Mat& convertedColorFrame,
  unsigned long long frameNumber,
  std::shared_ptr<hw::FocusOfExpansionDto> foe)
{
  std::vector<cv::Mat> canvas;
  cv::Scalar color = MEASURED_FOE_COLOR;

  // 0.) convertedColorFrame
  auto frame = convertedColorFrame;
  cv::Mat empty(frame.size(), frame.type(), cv::Scalar::all(64));
  canvas.push_back(empty);

  // 1.) grayscale raw with image and lense center and actual FOE, ROI(?)
  if(_settings.RoiEnabled) {
    auto& ocamModel = _settings.getOcamModel();
    DrawHelper::drawCross(frame, ocamModel.getCroppedFrameCenter(), "I");
    DrawHelper::drawCross(frame, ocamModel.getCroppedLensCenter(), "L", cv::Scalar(122, 160, 255));

    DrawHelper::visualizeRoi(frame, _roi);

  }
  canvas.push_back(frame);

  // 2.) heatmap with OF overlay
  DrawHelper::drawRings(heatMap, foe->getFoE(), color);
  DrawHelper::drawOpticalFlowMap(opticalFlow, heatMap, GREEN, 16, _settings.OpticalFlowVectorVisualizationFactor, _settings.OpticalFlowVectorVisualizationFactor);
  canvas.push_back(heatMap);

  // 3.) empty
  canvas.push_back(empty);

  auto combined = DrawHelper::makeRowCanvas(canvas, cv::Scalar(64, 64, 64));

  std::vector<std::string> lines;
  std::stringstream ss;
  ss << "#" << frameNumber << ": skipped";
  lines.push_back(ss.str()); ss.str(std::string());

  ss << "Weak inlier ratio " << std::setprecision(3) << foe->getInlierProportion()*100 << "\% @ (" << foe->getFoE().x << ", " << foe->getFoE().y << ")";
  lines.push_back(ss.str()); ss.str(std::string());

  ss << "Climb Rate: " << std::setprecision(3) << _climbRate << "m/s, Threshold: " << std::setprecision(3) << _settings.ClimbRateValueThreshold;
  lines.push_back(ss.str()); ss.str(std::string());

  ss << "Inlier Ratio: " << std::setprecision(3) << foe->getInlierProportion()*100 << "\%";
  lines.push_back(ss.str()); ss.str(std::string());
  
  return DrawHelper::renderText(combined, lines);
}
