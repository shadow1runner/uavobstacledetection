#ifndef OPTICALFLOW_UIPREPARER_H
#define OPTICALFLOW_UIPREPARER_H

#include <vector>
#include <memory>
#include <iostream>

#include <QObject>
#include <QQmlListProperty>
#include <QQuickImageProvider>

#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#include "QGCToolbox.h"
#include "DrawHelper.h"
#include "HeatMap.h"
#include "OwnFlow.h"
#include "FocusOfExpansionDto.h"
#include "CollisionAvoidanceSettings.h"
#include "CollisionLevel.h"
#include "helper/AvgWatch.h"

class Vehicle;

namespace hw {
	class UiFramePreparer : public QObject
	{
		Q_OBJECT
	public:
		UiFramePreparer(QObject* parent=NULL);
		
	signals:
	    void uiFrameReady(const cv::Mat& frame, unsigned long long frameNumber);    

	public slots:
	    void collisionLevelRatingReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence, const cv::Mat& convertedColorFrame);
	    void badFrame(const cv::Mat& badFrame, unsigned long long skipFrameCount, unsigned long long totalFrameCount, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured, double avgDivergence, const cv::Mat& convertedColorFrame);
	    void opticalFlowReady(const cv::Mat& opticalFlow);
	    void histogramReady(const cv::Mat& histogram);
	    void collisionImmanent(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence);

	    void roiReady(const cv::Rect& roi, double rollDegree, double pitchDegree, double climbRate);

	private:
		QImage  cvMatToQImage(const cv::Mat& mat);
		cv::Mat renderGoodFrame(const cv::Mat& goodFrame, const cv::Mat& convertedColorFrame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence);
        cv::Mat renderBadFrame(const cv::Mat& badFrame, const cv::Mat& convertedColorFrame, unsigned long long frameNumber, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured);

		cv::Mat _uiMat;
		std::shared_ptr<hw::FocusOfExpansionDto> foeFiltered;
		std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured;
		std::shared_ptr<hw::Divergence> divergence;

		cv::Mat opticalFlow;
		cv::Mat heatMap;
		const cv::Scalar GREEN = cv::Scalar(0, 255, 0);
		const cv::Scalar MEASURED_FOE_COLOR = cv::Scalar(255, 0, 0);

		// ROI
		cv::Rect _roi;
		double _rollDegree = 0.0;
		double _pitchDegree = 0.0;
		double _climbRate = 0.0;
		
		CollisionAvoidanceSettings& _settings;
	};	
}
#endif
