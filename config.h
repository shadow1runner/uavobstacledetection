//
// Created by Helmut on 12/18/2015.
//

#ifndef OPTICALFLOW_CONFIG_H
#define OPTICALFLOW_CONFIG_H

#include <string>

const std::string FILENAME = "res/rl/go5.mp4"; // input file to determine FOE from
const std::string OUTPUT_DIR = "res/out/RandomCollider/cpp/frame_"; // output file where the individual canvases are persisted, make sure it exists, use procmon

const int SUBSAMPLE_AMOUNT = 2; //34 // scale factor for each frame; n = 1 --> half resolution n = 2 --> 1/4 resolution

const int PARTICLES = 20000; // should be divisible by the amount of cores, the reminder will be silently ignored...
const int WINDOW_SIZE = 5; // 1 -> maximum element, 2 -> 4 neighborhood to the lower right; 3 -> 9 neighborhood etc

const int INLIER_PROPORTION_THRESHOLD = 0.01; // any frame with a lower inlier proportion will be discarded
const int DIVERGENCE_PATCHSIZE = 20; // patch size used for determining the divergence
const double DIVERGENCE_THRESHOLD = 0.08; //

#endif //OPTICALFLOW_CONFIG_H
