//
// Created by Helmut on 02/02/2015.
//

#ifndef OPTICALFLOW_AVGWATCH_H
#define OPTICALFLOW_AVGWATCH_H

#include <chrono>
#include <string>
#include <stdexcept>
#include <climits>
#include <sstream>

// usage:
// AvgWatch sw;
// functionToBeTimed(arg1, arg2);
// sw.stop();
// std::cout << "functionToBeTimed took " << sw.elapsedTotalSoFar() << " ns\n";
// http://codereview.stackexchange.com/a/48884
class AvgWatch
{
private:
    bool tickStarted = false;
    std::string name;
    std::chrono::time_point<std::chrono::high_resolution_clock> tickStart;
    unsigned long long sumElapsed = 0;

    unsigned long long maxElapsed = 0;
    unsigned long maxTickNumber = 0;

    unsigned long long minElapsed = ULLONG_MAX;
    unsigned long minTickNumber = 0;
    
    unsigned long long lastElapsed = 0;
    
    unsigned long tickCount = 0;
public:
    AvgWatch(std::string name="", bool autoStart=false) 
        : name(name)
    { 
        if(autoStart)
            startTick();
    }

    std::string getName() const {
        return name;
    }

    void startTick() {
        if(tickStarted)
            throw std::logic_error("tickStarted");

        tickStart = std::chrono::high_resolution_clock::now();
        tickStarted = true;
    }

    void stopTick() {
        if(!tickStarted)
            throw std::logic_error("tickStarted");
        
        std::chrono::time_point<std::chrono::high_resolution_clock> tickEnd = std::chrono::high_resolution_clock::now();
        auto delta = std::chrono::duration_cast<std::chrono::nanoseconds>(tickEnd-tickStart);

        lastElapsed = delta.count();
        
        sumElapsed += lastElapsed;
        ++tickCount;

        if(lastElapsed>maxElapsed) {
            maxTickNumber = tickCount;
            maxElapsed = lastElapsed;
        }
        else if(lastElapsed<minElapsed) {
            minTickNumber = tickCount;
            minElapsed = lastElapsed;
        }

        tickStarted = false;
    }

    bool isRunning() {
        return tickStarted;
    }

    double elapsedAvg() {
        return sumElapsed/tickCount;
    }

    double elapsedMin() {
        return minElapsed;
    }

    double elapsedMax() {
        return maxElapsed;
    }

    double elapsedLast() {
        return lastElapsed;
    }

    std::string prettyFormat(unsigned long divisor=1e6, std::string unit="ms") {
        std::ostringstream os;
        os << "AVG/MIN (#" << minTickNumber << ")/MAX (#" << maxTickNumber << ") in "<< unit << " FOR `" << getName() << "` after " << tickCount << " iterations:\t";
        os << elapsedAvg()/divisor << " " << unit << "\t";
        os << elapsedMin()/divisor << " " << unit << "\t";
        os << elapsedMax()/divisor << " " << unit << std::endl;
        return os.str();
    }

    std::string prettyFormatLast(unsigned long divisor=1e6, std::string unit="ms") {
        std::ostringstream os;
        os << "`" << getName() << "`: " << elapsedLast()/divisor << " " << unit << std::endl;
        return os.str();
    }
};

#endif //OPTICALFLOW_AVGWATCH_H
