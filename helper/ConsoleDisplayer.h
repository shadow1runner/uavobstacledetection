#ifndef OPTICALFLOW_CONSOLEConsoleDISPLAYER_H
#define OPTICALFLOW_CONSOLEConsoleDISPLAYER_H

#include "helper/DrawHelper.h"
#include "helper/HeatMap.h"
#include "OwnFlow.h"
#include "FocusOfExpansionDto.h"

#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include <vector>
#include <memory>
#include <iostream>

#include <QObject>

namespace hw {

  class ConsoleDisplayer : public QObject
  {
    Q_OBJECT

    cv::Mat opticalFlow;
    cv::Mat heatMap;


  public:
    ConsoleDisplayer(const OwnFlow& ownFlow) {      
      QObject::connect(&ownFlow, &OwnFlow::collisionLevelRatingReady,
                       this, &ConsoleDisplayer::foeReady);
    }

  public slots:
    void foeReady(const cv::Mat& frame, unsigned long long frameNumber, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, std::shared_ptr<hw::CollisionDetectorResult> detectorResult, double lastDivergence, double avgDivergence)
    {
      Q_UNUSED(frame);
      Q_UNUSED(frameNumber);
      Q_UNUSED(detectorResult);

      std::cout << "Foe: " << foeFiltered << std::endl;
      std::cout << "divergence: " << lastDivergence << std::endl;
      std::cout << "Avg Divergence: " << avgDivergence << std::endl;
      std::cout << "measuredFoe: " << foe->getFoE() << std::endl;
      std::cout << "inlierProportion: " << foe->getInlierProportion() << std::endl;
      std::cout << "numberOfInliers: " << foe->getNumberOfInliers() << std::endl;
      std::cout << "numberOfParticles: " << foe->getNumberOfParticles() <<std::endl;
    }    
  };
}

#endif //OPTICALFLOW_CONSOLEConsoleDISPLAYER_H
