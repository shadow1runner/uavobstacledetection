#ifndef OPTICALFLOW_DISPLAYER_H
#define OPTICALFLOW_DISPLAYER_H

#include "helper/DrawHelper.h"
#include "helper/HeatMap.h"
#include "OwnFlow.h"
#include "FocusOfExpansionDto.h"

#include <opencv2/core.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/imgproc.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

#include <vector>
#include <memory>

#include <QObject>

namespace hw {

//  class Displayer : public QObject
//  {
//    Q_OBJECT

//    cv::Mat opticalFlow;
//    cv::Mat heatMap;
//    const double inlierProportion;
//    const cv::Scalar GREEN = cv::Scalar(0, 255, 0);
//    const cv::Scalar GOOD_CONFIDENCE = cv::Scalar(255, 0, 0);
//    const cv::Scalar BAD_CONFIDENCE = cv::Scalar(0, 255, 0);

//  public:
//    Displayer(const OwnFlow& ownFlow, double inlierProportion)
//      : inlierProportion(inlierProportion)
//    {
//      QObject::connect(&ownFlow, &OwnFlow::opticalFlowChanged,
//                       this, &Displayer::opticalFlowReady,
//                       Qt::BlockingQueuedConnection);
      
//      QObject::connect(&ownFlow, &OwnFlow::histogramChanged,
//                       this, &Displayer::histogramReady,
//                       Qt::BlockingQueuedConnection);
      
//      QObject::connect(&ownFlow, &OwnFlow::collisionLevelRatingReady,
//                       this, &Displayer::collisionLevelRatingReady,
//                       Qt::BlockingQueuedConnection);
//    }

//  public slots:
//    void opticalFlowReady(const cv::Mat& opticalFlow) {
//      this->opticalFlow = opticalFlow;
//    }

//    void histogramReady(const cv::Mat& histogram) {
//      heatMap = HeatMap::createHeatMap(histogram);
//    }

//    void collisionLevelRatingReady(const cv::Mat& frame, std::shared_ptr<cv::Point2i> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foe, const hw::CollisionLevel collisionLevel, double lastDivergence, double avgDivergence)
//    {
//      Q_UNUSED(lastDivergence);
//      Q_UNUSED(avgDivergence);

//      // ========================== DRAW =====================
//      std::vector<cv::Mat> canvas;

//      cv::Scalar color = foe->getInlierProportion() > inlierProportion ? GOOD_CONFIDENCE : BAD_CONFIDENCE;

//      DrawHelper::drawRings(heatMap, foe->getFoE(), color);
//      DrawHelper::drawRings(heatMap, *foeFiltered, collisionLevel);
//      canvas.push_back(heatMap);

//      cv::Mat bgr;
//      cv::cvtColor(frame, bgr, cv::COLOR_GRAY2BGR);
//      DrawHelper::drawRings(bgr, foe->getFoE(), color);
//      DrawHelper::drawRings(bgr, *foeFiltered, collisionLevel);
//      canvas.push_back(bgr);

//      cv::Mat flowOverlay;
//      cv::cvtColor(frame, flowOverlay, cv::COLOR_GRAY2BGR);
//      DrawHelper::drawOpticalFlowMap(opticalFlow, flowOverlay, GREEN);
//      DrawHelper::drawRings(flowOverlay, foe->getFoE(), color);
//      DrawHelper::drawRings(flowOverlay, *foeFiltered, collisionLevel);
//      canvas.push_back(flowOverlay);

//      auto hsv = DrawHelper::visualiceFlowAsHsv(opticalFlow);
//      DrawHelper::drawRings(hsv, foe->getFoE(), color);
//      DrawHelper::drawRings(hsv, *foeFiltered, collisionLevel);
//      canvas.push_back(hsv);

//      auto combined = DrawHelper::makeRowCanvas(canvas, cv::Scalar(84, 72, 2));
//  //    cv::imwrite(OUTPUT_DIR + std::to_string(frame_number) + ".jpg", combined);
//      imshow("combined", combined);
//      cv::waitKey(30);
//      // ========================== DRAW - END =====================
//    }
//  };
}

#endif //OPTICALFLOW_DISPLAYER_H
