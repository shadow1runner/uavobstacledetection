#include "DrawHelper.h"

#include <opencv2/highgui.hpp>

#include <vector>

using namespace std;
using namespace cv;

void DrawHelper::drawRings(cv::Mat& img, const cv::Point2i& center, const cv::Scalar& bgrColor, int numberOfRings)
{
    for(int i = 0; i < numberOfRings; ++i)
    {
        cv::circle(img, center, 5+2*i, bgrColor, 1, CV_AA, 0);
    }
}

void DrawHelper::drawRings(cv::Mat& img, const cv::Point2i& center, const hw::CollisionLevel collisionLevel)
{
    cv::Scalar color(0,0,0);
    if(collisionLevel <= 1)
        color=cv::Scalar(50,154,205);
    if(collisionLevel <= hw::CollisionLevel::medium)
        color=cv::Scalar(47,173,255);
    if(collisionLevel <= hw::CollisionLevel::warn)
        color=cv::Scalar(0,255,255);
    if(collisionLevel <= hw::CollisionLevel::high)
        color=cv::Scalar(0,0,255);

    DrawHelper::drawRings(img, center, color, collisionLevel);
}

void DrawHelper::drawCross(cv::Mat& img, const cv::Point2i& center, const std::string& text, const cv::Scalar& bgrColor)
{
    // https://opencvexamples.blogspot.com/2013/10/basic-drawing-examples.html
    line(img, center-Point(5,0), center+Point(5,0), bgrColor, 2, 8);
    line(img, center-Point(0,5), center+Point(0,5), bgrColor, 2, 8);
    if(!text.empty())
    {
        putText(img, text, center+Point(7,7), FONT_HERSHEY_SIMPLEX, 0.4, bgrColor, 4,false);
    }
}

void DrawHelper::visualizeRoi(cv::Mat& img, const cv::Rect& roi, const cv::Scalar& roiColor)
{
    // cv::Mat roiImg(img, roi);
    auto thickness = 3;
    // cv::Rect boundary(roi.tl(), roi.br()-cv::Point(thickness*2, thickness*2));
    rectangle(img, roi, roiColor, 1); // 3 = thickness
}



/**
 * @brief makeCanvas Makes composite image from the given images - taken from http://stackoverflow.com/a/22858549
 * @param vecMat Vector of Images.
 * @param windowHeight The height of the new composite image to be formed.
 * @param nRows Number of rows of images. (Number of columns will be calculated
 *              depending on the value of total number of images).
 * @return new composite image.
 */
 cv::Mat DrawHelper::makeColumnCanvas(const std::vector<cv::Mat>& images, cv::Scalar gapColor, int gap) {
    auto rows = 0;
    auto cols = images[0].cols;
    for (auto& img: images) {
        rows += img.rows;
        rows += gap;
    }

    rows -= gap;
    cv::Mat canvas = cv::Mat(rows, cols, CV_8UC3, gapColor);

    auto rowCounter = 0;
    for (auto& img: images) {
        auto targetRoi = canvas(cv::Rect(0, rowCounter, img.cols, img.rows));

        if (img.channels() != canvas.channels()) {
            if (img.channels() == 1) {
                cv::cvtColor(img, targetRoi, CV_GRAY2BGR);
            }
        } else {
            img.copyTo(targetRoi);
        }

        rowCounter += img.rows;
        rowCounter += gap;
    }

    return canvas;
}

/**
 * @brief makeCanvas Makes composite image from the given images - taken from http://stackoverflow.com/a/22858549
 * @param vecMat Vector of Images.
 * @param windowHeight The height of the new composite image to be formed.
 * @param nRows Number of rows of images. (Number of columns will be calculated
 *              depending on the value of total number of images).
 * @return new composite image.
 */
 cv::Mat DrawHelper::makeRowCanvas(const std::vector<cv::Mat>& images, cv::Scalar gapColor, int gap) {
    const auto& fst = images.front();
    cv::Mat canvas = cv::Mat(fst.rows, (int)(fst.cols*images.size() + (images.size()-1)*gap), CV_8UC3, gapColor);

    for (auto& img: images) {
        auto i = &img - &images[0];
        auto targetRoi = canvas(cv::Rect(fst.cols*i+gap*i, 0, fst.cols, fst.rows));

        if (img.channels() != canvas.channels()) {
            if (img.channels() == 1) {
                cv::cvtColor(img, targetRoi, CV_GRAY2BGR);
            }
        } else {
            img.copyTo(targetRoi);
        }
    }

    return canvas;
}

void DrawHelper::drawOpticalFlowMap(const cv::Mat& flow, cv::Mat& canvas, const cv::Scalar& color, int step, const double factordx, const double factordy)
{
    for(int y = 0; y < canvas.rows; y += step)
        for(int x = 0; x < canvas.cols; x += step)
        {
            const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x);
            cv::line(canvas, cv::Point(x,y), cv::Point(cvRound(x+fxy.x*factordx), cvRound(y+fxy.y*factordy)),
             color);
            cv::circle(canvas, cv::Point(x,y), 2, color, -1);
        }
    }

    cv::Mat DrawHelper::visualiceFlowAsHsv(const cv::Mat& flow) {
    // source code converted from python to c++: official opencv sample @ opencv/samples/python/opt_flow.py
        vector<Mat> flowChannels(2);
        split(flow, flowChannels);
        Mat fx = flowChannels[0];
        Mat fy = flowChannels[1];

        Mat v = cv::Mat(flow.rows, flow.cols, CV_8UC1);
        magnitude(fx, fy, v);
    v *= 4; // scale factor?
    v = min(v, 255);
    v.convertTo(v, CV_8UC1);

    Mat ang = cv::Mat(flow.rows, flow.cols, CV_8UC1);
    phase(fx, fy, ang);
    ang += boost::math::double_constants::pi;
    ang *= 180 / boost::math::double_constants::pi / 2;
    ang.convertTo(ang, CV_8UC1);

    vector<Mat> hsvChannels;
    hsvChannels.push_back(ang);
    hsvChannels.push_back(Mat(flow.rows, flow.cols, CV_8UC1, cv::Scalar(255)));
    hsvChannels.push_back(v);

    auto hsv = cv::Mat(flow.rows, flow.cols, CV_8UC3);

    merge(hsvChannels, hsv);
    cvtColor(hsv, hsv, CV_HSV2BGR);
    return hsv;
}

cv::Mat DrawHelper::renderText(const cv::Mat& frame, std::vector<std::string>& lines, cv::Scalar canvasColor, int fontFace, double fontScale, int thickness)
{
    auto lineCount = lines.size();

    int baseline=0;
    cv::Size textSize = cv::getTextSize(lines[0], fontFace, fontScale, thickness, &baseline);
    auto height = (lineCount+1) * (textSize.height+12);

    cv::Mat canvas(height, frame.cols, CV_8UC3, canvasColor);
    for(auto const& text : lines)
    {
        int i = &text-&lines[0];
        cv::Size textSize = cv::getTextSize(text, fontFace, fontScale, thickness, &baseline);
        cv::Point textOrg(3, (i+1)*(textSize.height+12));

        cv::putText(canvas, text, textOrg, fontFace, fontScale, cv::Scalar::all(255), thickness, 8);
    }

    std::vector<cv::Mat> ret;
    ret.push_back(frame);
    ret.push_back(canvas);
    return DrawHelper::makeColumnCanvas(ret, canvasColor, 0);
}
