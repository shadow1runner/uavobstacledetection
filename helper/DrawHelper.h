//
// Created by Helmut on 12/31/2015.
//

#ifndef OPTICALFLOW_DRAWHELPER_H
#define OPTICALFLOW_DRAWHELPER_H

#include "CollisionLevel.h"

#include <string>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <math.h>
#include <boost/math/constants/constants.hpp>
#include <algorithm>

class DrawHelper
{
public:
    static void drawRings(cv::Mat& img, const cv::Point2i& center, const cv::Scalar& bgrColor, int numberOfRings=3);
    
    static void drawRings(cv::Mat& img, const cv::Point2i& center, const hw::CollisionLevel collisionLevel);

    static void drawCross(cv::Mat& img, const cv::Point2i& center, const std::string& text = "", const cv::Scalar& bgrColor = cv::Scalar(0, 255,255));

    static void visualizeRoi(cv::Mat& img, const cv::Rect& roi, const cv::Scalar& roiColor = cv::Scalar(0,255,0));

    /**
     * @brief make*Canvas Makes composite image from the given images - taken from http://stackoverflow.com/a/22858549
     * @param vecMat Vector of Images.
     * @param windowHeight The height of the new composite image to be formed.
     * @param nRows Number of rows of images. (Number of columns will be calculated
     *              depending on the value of total number of images).
     * @return new composite image.
     */
    static cv::Mat makeColumnCanvas(const std::vector<cv::Mat>& images, cv::Scalar gapColor, int gap=10);

    static cv::Mat makeRowCanvas(const std::vector<cv::Mat>& images, cv::Scalar gapColor, int gap=10);

    static void drawOpticalFlowMap(const cv::Mat& flow, cv::Mat& canvas, const cv::Scalar& color, int step=16, const double factordx=50.0, const double factordy=50.0);

    static cv::Mat visualiceFlowAsHsv(const cv::Mat& flow);

    static cv::Mat renderText(const cv::Mat& frame, std::vector<std::string>& lines, cv::Scalar canvasColor = cv::Scalar::all(64), int fontFace = cv::FONT_HERSHEY_SIMPLEX, double fontScale = 0.6, int thickness = 1);
};


#endif //OPTICALFLOW_DRAWHELPER_H
