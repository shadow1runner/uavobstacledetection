#include "HeatMap.h"

uchar HeatMap::cutOff(double value) {
  if(value < 0)
      return 0;
  if(value > 255)
      return 255;
  return static_cast<uchar>(value);
}

/**
* Returns a value mapped into the range 0 ... RANGE_MAX
* @param min Value which should be mapped to 0
* @param max Value which should be mapped to 767
* @param value The value which should be mapped to the range 0 ... 767
*/
// @SS: bug in original implementation
double HeatMap::normalizeValue(const double min, const double max, const double value) {
  return (value - min)/(max-min) * HeatMap::RANGE_MAX;
}

/**
* Maps the values 0 ... 767 to the according color values in bgr (blue, green, red)
* 0 ... 255   = (0,  0, 0)  ... (  0,  0,255)
* 256 ... 511 = (0,  1,255) ... (  0,255,255)
* 512 ... 767 = (1,255,255) ... (255,255,255)
*/
cv::Vec3b HeatMap::rangeToColor(const double value) {
  auto r = HeatMap::cutOff(value);
  auto g = HeatMap::cutOff(value - 256);
  auto b = HeatMap::cutOff(value - 2 * 256);
  return cv::Vec3b(b, g, r);
}

/** creates a heat map from the data presented in `histogram`
  */
cv::Mat HeatMap::createHeatMap(const cv::Mat& histogram) {
  cv::Mat heatMap(histogram.rows, histogram.cols, CV_8UC3);

  double min, max;
  cv::minMaxLoc(histogram, &min, &max);

  for (int y = 0; y < histogram.rows; ++y)
  {
      for (int x = 0; x < histogram.cols; ++x)
      {
          cv::Point2i pt(x, y);
          const auto& value = histogram.at<double>(pt);
          heatMap.at<cv::Vec3b>(pt) = HeatMap::rangeToColor(HeatMap::normalizeValue(min, max, value));
      }
  }

  return heatMap;
}
