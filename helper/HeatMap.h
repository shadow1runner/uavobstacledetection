//
// Created by Helmut on 12/21/2015.
//

#ifndef OPTICALFLOW_HEATMAP_H
#define OPTICALFLOW_HEATMAP_H

#include <opencv2/core.hpp>
#include <iostream>

class HeatMap
{
private:
  /**
   * Highest value that can be represented
   */
  static constexpr double RANGE_MAX = 3*256.0-1;

  /**
   * Restrict a value between 0 and 255
   */ 
  static uchar cutOff(double value);

  /**
   * Returns a value mapped into the range 0 ... RANGE_MAX
   * @param min Value which should be mapped to 0
   * @param max Value which should be mapped to 767
   * @param value The value which should be mapped to the range 0 ... 767
   */
  // @SS: bug in original implementation
  static double normalizeValue(const double min, const double max, const double value);

  /**
   * Maps the values 0 ... 767 to the according color values in bgr (blue, green, red)
   * 0 ... 255   = (0,  0, 0)  ... (  0,  0,255)
   * 256 ... 511 = (0,  1,255) ... (  0,255,255)
   * 512 ... 767 = (1,255,255) ... (255,255,255)
   */
  static cv::Vec3b rangeToColor(const double value);


public:
    HeatMap() = delete;

    /** creates a heat map from the data presented in `histogram`
      */
    static cv::Mat createHeatMap(const cv::Mat& histogram);
};


#endif //OPTICALFLOW_HEATMAP_H
