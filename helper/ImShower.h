#ifndef OPTICALFLOW_IMSHOWER_H
#define OPTICALFLOW_IMSHOWER_H

//#include "helper/DrawHelper.h"
//#include "helper/HeatMap.h"
//#include "OwnFlow.h"
//#include "FocusOfExpansionDto.h"

//#include <opencv2/core/core.hpp>
//#include <opencv2/video/tracking.hpp>
//#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/videoio/videoio.hpp>
//#include <opencv2/highgui/highgui.hpp>

//#include <vector>

//#include <QObject>

//namespace hw {
//  class ImShower : public QObject
//  {
//    Q_OBJECT

//    cv::Mat opticalFlow;
//    cv::Mat heatMap;


//  public:
//    ImShower(const OwnFlow& ownFlow) {
//      QObject::connect(&ownFlow, &OwnFlow::opticalFlowChanged,
//                       this, &ImShower::opticalFlowReady);
      
//      QObject::connect(&ownFlow, &OwnFlow::histogramChanged,
//                       this, &ImShower::histogramReady);
      
//      QObject::connect(&ownFlow, &OwnFlow::foeChanged,
//                       this, &ImShower::foeReady);
//    }

//  public slots:
//    void opticalFlowReady(const cv::Mat& opticalFlow) {
//      this->opticalFlow = opticalFlow;
//    }

//    void histogramReady(const cv::Mat& histogram) {
//      heatMap = HeatMap::createHeatMap(histogram);
//    }

//    void foeReady(const cv::Mat& frame, std::shared_ptr<hw::FocusOfExpansionDto> foeFiltered, std::shared_ptr<hw::FocusOfExpansionDto> foeMeasured, std::shared_ptr<hw::Divergence> divergence) {
//      Q_UNUSED(divergence);

//      // ========================== DRAW =====================
//      std::vector<cv::Mat> canvas;

//      DrawHelper::drawRings(heatMap, foeMeasured->getFoE(), cv::Scalar(0, 255, 0));
//      DrawHelper::drawRings(heatMap, foeFiltered->getFoE(), cv::Scalar(255, 156, 0));
//      canvas.push_back(heatMap);

//      cv::Mat bgr;
//      cv::cvtColor(frame, bgr, cv::COLOR_GRAY2BGR);
//      DrawHelper::drawRings(bgr, foeMeasured->getFoE(), cv::Scalar(0, 255, 0));
//      DrawHelper::drawRings(bgr, foeFiltered->getFoE(), cv::Scalar(255, 156, 0));
//      canvas.push_back(bgr);

//      cv::Mat flowOverlay;
//      cv::cvtColor(frame, flowOverlay, cv::COLOR_GRAY2BGR);
//      DrawHelper::drawOpticalFlowMap(opticalFlow, flowOverlay, cv::Scalar(0, 255, 0));
//      DrawHelper::drawRings(flowOverlay, foeMeasured->getFoE(), cv::Scalar(0, 255, 0));
//      DrawHelper::drawRings(flowOverlay, foeFiltered->getFoE(), cv::Scalar(255, 156, 0));
//      canvas.push_back(flowOverlay);

//      auto hsv = DrawHelper::visualiceFlowAsHsv(opticalFlow);
//      DrawHelper::drawRings(hsv, foeMeasured->getFoE(), cv::Scalar(0, 255, 0));
//      DrawHelper::drawRings(hsv, foeFiltered->getFoE(), cv::Scalar(255, 156, 0));
//      canvas.push_back(hsv);

//      auto combined = DrawHelper::makeColumnCanvas(canvas, cv::Scalar(84, 72, 2));
//  //    cv::imwrite(OUTPUT_DIR + std::to_string(frame_number) + ".jpg", combined);
//      imshow("combined", combined);
//      cv::waitKey(30);
//      // ========================== DRAW - END =====================
//    }
//  };
//}

#endif //OPTICALFLOW_IMSHOWER_H
