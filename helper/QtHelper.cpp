#include "QtHelper.h"
#include "FocusOfExpansionDto.h"
#include "Divergence.h"
#include "AvgWatch.h"
#include "CollisionLevel.h"
#include "CollisionDetectorResult.h"

#include <memory>
#include <opencv2/core.hpp>

#include <QMetaType>
#include <QImage>

Q_DECLARE_METATYPE(cv::Mat)
Q_DECLARE_METATYPE(cv::Point2f)
Q_DECLARE_METATYPE(cv::Point2i)
Q_DECLARE_METATYPE(cv::Rect)
Q_DECLARE_METATYPE(std::shared_ptr<cv::Point2i>)
Q_DECLARE_METATYPE(hw::CollisionLevel)
Q_DECLARE_METATYPE(std::shared_ptr<hw::CollisionDetectorResult>)
Q_DECLARE_METATYPE(std::shared_ptr<hw::FocusOfExpansionDto>)
Q_DECLARE_METATYPE(std::shared_ptr<hw::Divergence>)
Q_DECLARE_METATYPE(std::shared_ptr<AvgWatch>)
Q_DECLARE_METATYPE(QImage)

void QtHelper::registerMetaTypes() {
    qRegisterMetaType<cv::Mat>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<cv::Point2f>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<cv::Point2i>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<cv::Rect>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<std::shared_ptr<cv::Point2i>>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<hw::CollisionLevel>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<std::shared_ptr<hw::CollisionDetectorResult>>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<std::shared_ptr<hw::FocusOfExpansionDto>>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<std::shared_ptr<hw::Divergence>>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<std::shared_ptr<AvgWatch>>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
    qRegisterMetaType<QImage>(); // necessary - otherwhise queueing (for inter-thread communication fails: QObject::connect: Cannot queue arguments of type 'cv::Mat' (Make sure 'cv::Mat' is registered using qRegisterMetaType().)
}
