//
// Created by Helmut on 12/19/2015.
//

#ifndef OPTICALFLOW_STOPWATCH_H
#define OPTICALFLOW_STOPWATCH_H

#include <chrono>

// usage:
// StopWatch<> sw;
// functionToBeTimed(arg1, arg2);
// sw.stop();
// std::cout << "functionToBeTimed took " << sw.elapsed() << " ns\n";
// http://codereview.stackexchange.com/a/48884
template<typename TimeT = std::chrono::nanoseconds,
    typename ClockT=std::chrono::high_resolution_clock,
    typename DurationT=double>
class StopWatch
{
private:
    bool stopped = false;
    std::chrono::time_point<ClockT> _start, _end;
public:
    StopWatch() { start(); }
    void start() { stopped = false; _start = _end = ClockT::now(); }
    DurationT stop() { _end = ClockT::now(); stopped=true; return elapsed();}
    DurationT elapsed() {
        if(!stopped)
            _end = ClockT::now();
        auto delta = std::chrono::duration_cast<TimeT>(_end-_start);
        stopped = true;
        return delta.count();
    }
};

#endif //OPTICALFLOW_STOPWATCH_H
