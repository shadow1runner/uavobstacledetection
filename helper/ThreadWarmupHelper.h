#ifndef OPTICALFLOW_THREADWARMUPHELPER_H
#define OPTICALFLOW_THREADWARMUPHELPER_H

#include <QRunnable>

class ThreadWarmupHelper : public QRunnable
{
private:
    int countUpTo;
public:
    ThreadWarmupHelper(int countUpTo=2)
        :countUpTo(countUpTo)
    {}

    void run() {
        for(auto i=0;i<countUpTo;++i)
            ;
    }    
};

#endif // OPTICALFLOW_THREADWARMUPHELPER_H
