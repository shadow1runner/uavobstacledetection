#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc.hpp>
//#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <unistd.h>
#include <chrono>
#include <thread>

#include "BufferedFrameGrabber.h"
#include "config.h"
#include "OwnFlow.h"
#include "Converter.h"
#include "FocusOfExpansionDto.h"
#include "FramePersister.h"
#include "CollisionAvoidanceSettings.h"
#include "RoiBuilder.h"

#include "helper/AvgWatch.h"
#include "helper/Macros.h"
#include "helper/Displayer.h"
#include "helper/ConsoleDisplayer.h"
#include "helper/QtHelper.h"

#include <QThread>
#include <QApplication>
#include <QDebug>

using namespace std;
using namespace hw;

int main(int argc, char *argv[]) {
    QApplication a(argc, argv); // to prevent `QEventLoop: Cannot be used without QApplication` - http://stackoverflow.com/a/10698037

    QtHelper::registerMetaTypes();

    auto& settings = CollisionAvoidanceSettings::getInstance(); 
    hw::BufferedFrameGrabber _frameGrabber(settings, 1, [](cv::Mat input) {return input;});

    bool _isPaused = false;

    RoiBuilder roiBuilder;

    hw::Converter _converter(settings);
    hw::OwnFlow _ownFlow(settings, &roiBuilder);
    FramePersister _framePersister(settings);

    QThread _ownFlowThread;
    QThread _converterThread;
    QThread _ioThread;

//    Displayer displayer(_ownFlow, settings.InlierProportionThreshold);
    ConsoleDisplayer consoleDisplayer(_ownFlow);

    _ownFlowThread.setObjectName("OwnFlow");
    _ioThread.setObjectName("OwnFlow I/O");

    _converter.moveToThread(&_converterThread);
    _ownFlow.moveToThread(&_ownFlowThread);
    _framePersister.moveToThread(&_ioThread);

    QObject::connect(&_converter, &hw::Converter::imageConverted,
            &_ownFlow, &hw::OwnFlow::processImage
            ,Qt::BlockingQueuedConnection
           );

    // I/O connections
    QObject::connect(&_frameGrabber, &hw::BufferedFrameGrabber::newRawFrame,
            &_framePersister, &FramePersister::rawFrameReady);

    QObject::connect(&_ownFlow, &hw::OwnFlow::collisionLevelRatingReady,
            &_framePersister, &FramePersister::collisionLevelRatingReady);

    QObject::connect(&_ownFlow, &hw::OwnFlow::frameSkipped,
            &_framePersister, &FramePersister::badFrameReady);

    QObject::connect(&_ownFlow, &hw::OwnFlow::opticalFlowChanged,
            &_framePersister, &FramePersister::opticalFlowReady);
    
    QObject::connect(&_ownFlow, &hw::OwnFlow::histogramChanged,
            &_framePersister, &FramePersister::histogramReady);


    // start
    _ioThread.start();
    _converterThread.start();
    _ownFlowThread.start();

    _isPaused = false;
//    emit isPausedChanged(_isPaused);

    while(!_isPaused && _frameGrabber.has_next()) {
        auto currentFrame = _frameGrabber.next();
        _converter.convertImage(currentFrame);
    }

   _isPaused = true;
//   emit isPausedChanged(_isPaused);

    return 0;
}
