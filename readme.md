# Optical flow-based Obstacle Avoidance for Unmanned Aerial Vehicles
Repository of the Master thesis with the same name to receive the degree Master Of Science at the University of Innsbruck, Austria
Helmut Wolf, &copy; 2016

Supervisors: 
  * Sebastian Stabinger, MSc
  * Prof. Dr. Justus Piater