import bpy
from bpy_extras.object_utils import world_to_camera_view
import copy
import xml.etree.ElementTree as ET


def getCameraPos(frame):
    scene = bpy.data.scenes["Scene"]
    scene.frame_set(frame)
    location = scene.camera.location
    return copy.copy(location)


def getAllCameraPos():
    scene = bpy.data.scenes["Scene"]
    poslist = []
    for frame in range(scene.frame_start, scene.frame_end + 1):
        poslist.append(getCameraPos(frame))
    scene.frame_set(1)
    return poslist


def getFOE():
    camPos = getAllCameraPos()
    scene = bpy.data.scenes["Scene"]
    foeList = []
    for frame in range(scene.frame_start, scene.frame_end):
        scene.frame_set(frame)
        position = camPos[frame]

        foe = world_to_camera_view(scene, scene.camera, position)
        # (foe.x, foe.y) = (1-foe.x, 1-foe.y) <- __happens in the conversion to the csv__
        # https://www.blender.org/api/blender_python_api_2_77_1/bpy_extras.object_utils.html
        # world_to_camera_view:  delivers: Where (0, 0) is the bottom left and (1, 1) is the top right of the camera frame. values outside 0-1 are also supported. A negative ‘z’ value means the point is behind the camera.
        # (0,0) should be upper left corner, (1,1) bottom right, but world_to_camera_view
        # be aware of that aforementioned difference!
        foeList.append(foe)
    return foeList


def writeFoeXml(filename, foeList):
    root = ET.Element("focus_of_expansion_list")
    root.set("project", bpy.path.basename(bpy.data.filepath))
    for i in range(len(foeList)):
        entry = ET.SubElement(root, "foe")
        entry.set("frame", str(i))
        ET.SubElement(entry, 'x').text = str(foeList[i][0])
        ET.SubElement(entry, 'y').text = str(foeList[i][1])
    # Wrap it in an ElementTree instance, and save as XML
    tree = ET.ElementTree(root)
    tree.write(filename, xml_declaration=True)

print(getFOE())
writeFoeXml("/tmp/test.xml", getFOE())
