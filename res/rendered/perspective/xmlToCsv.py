import sys
import xmltodict


def parse(xml_path, out_path):
    with open(xml_path) as fd:
        with open(out_path, "w") as out:
            doc = xmltodict.parse(fd.read())
            for frame in doc['focus_of_expansion_list']['foe']:
                line = to_csv(frame)
                out.write(line)

def to_csv(frame):
    frame_number = frame['@frame']
    x = frame['x']
    y = frame['y']
    # line = 'ground_truth;3;{};{};{};\n'.format(frame_number, 1.0-float(x), 1.0-float(y))
    line = 'ground_truth;3;{};{};{};\n'.format(frame_number, float(x), float(y))
    return line


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print 'Usage: xmlToCsv <xmlFile> <csvFile\nExample ./xmlToCsv S1.xml S1.csv.'
        sys.exit()
    parse(sys.argv[1], sys.argv[2])
    print('Done, `{}` written successfully.'.format(sys.argv[2]))
