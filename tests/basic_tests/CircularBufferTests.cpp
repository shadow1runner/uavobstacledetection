//
// Created by Helmut on 12/21/2015.
//

#include "gtest/gtest.h"
#include "../../CircularBuffer.h"
#include <iostream>

using namespace std;
using namespace hw;

TEST(circular_buffer, adding_more_than_capacity) {
    unsigned long capacity = 2l;
    hw::CircularBuffer<int> uut(capacity);

    uut.send(1); // will get kicked out
    uut.send(2);
    uut.send(3);

    ASSERT_EQ(2, uut.receive());
    ASSERT_EQ(3, uut.receive());
}

TEST(circular_buffer, retrieving_whole_buffer) {
    unsigned long capacity = 2l;
    hw::CircularBuffer<int> uut(capacity);

    uut.send(1); // will get kicked out
    uut.send(2);
    uut.send(3);

    for (auto val : uut.get_buffer()) {
        cout << val << endl;
    }
}

