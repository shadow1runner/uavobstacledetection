#include "gtest/gtest.h"
#include <iostream>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "../../CollisionAvoidanceSettings.h"
#include "../../DivergenceHistory.h"

using namespace std;
using namespace hw;
using namespace cv;

TEST(divergenceHistory, noDivergencesAvg) 
{
    DivergenceHistory divHistory(CollisionAvoidanceSettings::getInstance());
    ASSERT_EQ(0.0, divHistory.getMovingAverageDivergence());
}