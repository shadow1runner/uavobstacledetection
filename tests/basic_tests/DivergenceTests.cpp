//
// Created by Helmut on 12/21/2015.
//

#include "gtest/gtest.h"
#include "../../CircularBuffer.h"
#include "../../RandomCollider.h"
#include "../../helper/HeatMap.h"
#include "../../helper/DrawHelper.h"
#include "../../Divergence.h"
#include "../../CollisionAvoidanceSettings.h"
#include <iostream>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace hw;

TEST(divergence_tests, compare_to_scala_divergence_should_yield_same_results) {
    // values retrieved from scala (cset d966c2c8fd0593c85e474539872044ca4ca9846a)
    std::vector<double> expected;
    expected.push_back(-0.34169630259275435);
    expected.push_back(-0.521149818599224);
    expected.push_back(-0.6121809668838978);
    expected.push_back(-0.648802251368761);
    expected.push_back(-0.7231833226978779);
    expected.push_back(-0.3885768797248602);
    expected.push_back(-0.5585608966648579);
    expected.push_back(-0.6547859661281109);
    expected.push_back(-0.6820267453789711);
    expected.push_back(-0.7381447613239288);
    expected.push_back(-0.4821203492581844);
    expected.push_back(-0.6477559372782707);
    expected.push_back(-0.7084570981562137);
    expected.push_back(-0.698971974849701);
    expected.push_back(-0.7364393100142479);
    expected.push_back(-0.5485595263540745);
    expected.push_back(-0.687281509488821);
    expected.push_back(-0.712124091386795);
    expected.push_back(-0.6986442893743515);
    expected.push_back(-0.7389695443212986);
    expected.push_back(-0.532952906191349);
    expected.push_back(-0.6448004152625799);
    expected.push_back(-0.6770012058317662);
    expected.push_back(-0.6837066195905208);
    expected.push_back(-0.716396302729845);

    const int SUBSAMPLE_AMOUNT = 8;

    auto fst = cv::imread("res/illustrator/go5_0.jpg");
    cv::resize(fst, fst, cv::Size(fst.cols / (2 * SUBSAMPLE_AMOUNT), fst.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(fst, fst, CV_BGR2GRAY);
    cv::equalizeHist(fst, fst);

    auto snd = cv::imread("res/illustrator/go5_1.jpg");
    cv::resize(snd, snd, cv::Size(snd.cols / (2 * SUBSAMPLE_AMOUNT), snd.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(snd, snd, CV_BGR2GRAY);
    cv::equalizeHist(snd, snd);

    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

    std::vector<double> results;
    for (int y = 0; y < 5; ++y) {
        for (int x = 0; x < 5; ++x) {
            auto& pt = flow.at<cv::Point2f>(y, x);
            Divergence div(CollisionAvoidanceSettings::getInstance(), cv::Point2i(x,y));
            results.push_back(div.calculateDivergence(flow, 20));
        }
    }

    Divergence div(CollisionAvoidanceSettings::getInstance(), cv::Point2i(0,0));
    ASSERT_DOUBLE_EQ(div.calculateDivergence(flow, 20), -0.34169630259275435);

    for(auto i = 0;i<expected.size();++i)
        ASSERT_DOUBLE_EQ(expected[i], results[i]);
}

