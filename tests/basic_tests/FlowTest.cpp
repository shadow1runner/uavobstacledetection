//
// Created by Helmut on 12/21/2015.
//

#include "gtest/gtest.h"
#include "../../CircularBuffer.h"
#include "../../RandomCollider.h"
#include "../../helper/HeatMap.h"
#include "../../helper/DrawHelper.h"
#include <iostream>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace hw;

TEST(flow_test, optical_flow_farneback_gpu_yields_different_results_than_cpu) {
    auto tmp = cv::imread("res/illustrator/fst.jpg");
    cv::Mat fst;
    cv::cvtColor(tmp, fst, CV_BGR2GRAY );
//    cv::imwrite("res/illustrator/fst_gray_cpp.jpg", fst);

    tmp = cv::imread("res/illustrator/snd.jpg");
    cv::Mat snd;
    cv::cvtColor(tmp, snd, CV_BGR2GRAY );
//    cv::imwrite("res/illustrator/snd_gray_cpp.jpg", snd);

    cv::imshow("fst", fst);
    cv::imshow("snd", snd);

    // GPU
    cv::UMat uflow;
    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, uflow, 0.5, 3, 10, 10, 7, 1.5, 0);
    uflow.copyTo(flow);

    cout << "C++ - GPU\n=================" << endl;
    cout << "fst:" << endl << fst << endl << endl;
    cout << "snd:" << endl << snd << endl << endl;

    for(auto x = 0; x<snd.cols; ++x)
        for(auto y=0; y<snd.rows; ++y) {
            auto& pt = flow.at<cv::Point2f>(cv::Point(x,y));
            cout << "(" << x << ", " << y << "): ";
            cout << "(" << pt.x << ", " << pt.y << ")" << endl;
        }
}


TEST(flow_test, collission) {
    auto tmp = cv::imread("res/illustrator/fst.jpg");
    cv::Mat fst;
    cv::cvtColor(tmp, fst, CV_BGR2GRAY);

    tmp = cv::imread("res/illustrator/snd.jpg");
    cv::Mat snd;
    cv::cvtColor(tmp, snd, CV_BGR2GRAY);

    // CPU
    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

    for(auto x = 0; x<snd.cols; ++x)
        for(auto y=0; y<snd.rows; ++y) {
            auto& pt = flow.at<cv::Point2f>(cv::Point(x,y));
            cout << "(" << x << ", " << y << "): ";
            cout << "(" << pt.x << ", " << pt.y << ")" << endl;
        }

    cv::Mat histogram;
    hw::RandomCollider randomColider(flow, 200000, histogram);
    randomColider.run();
    cout << "histogram:" << endl << histogram << endl << endl;
}

cv::Mat convert(cv::Mat& input) {
    cv::Mat tmp, gray, equalized;
    cv::resize(input, tmp, cv::Size(input.cols / (2 * 32), input.rows / (2 * 32)), cv::INTER_NEAREST);
    cv::cvtColor(tmp, gray, cv::COLOR_BGR2GRAY);
//    cv::equalizeHist(gray, equalized);
    return gray;
}


TEST(flow_test, create_integral_image) {
    auto tmp = cv::imread("res/illustrator/fst_big.jpg");
    cv::Mat fst = convert(tmp);

    tmp = cv::imread("res/illustrator/snd_big.jpg");
    cv::Mat snd = convert(tmp);

    cout << "C++ - CPU\n=================" << endl;
    cout << "fst:" << endl << fst << endl << endl;
    cout << "snd:" << endl << snd << endl << endl;

    // CPU
    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

    for(auto x = 0; x<snd.cols; ++x)
        for(auto y=0; y<snd.rows; ++y) {
            auto& pt = flow.at<cv::Point2f>(cv::Point(x,y));
            cout << "(" << x << ", " << y << "): ";
            cout << "(" << pt.x << ", " << pt.y << ")" << endl;
        }

    cv::Mat histogram;
    hw::RandomCollider randomColider(flow, 200000, histogram);
    randomColider.run();
    cout << "histogram:" << endl << histogram << endl << endl;

    cv::Mat heatMap = HeatMap::createHeatMap(histogram);
    cout << "heatmap:" << endl << heatMap << endl << endl;

    cv::Mat integral_histogram;
    cv::integral(histogram, integral_histogram, CV_64F);
    for(int y = 0; y < integral_histogram.cols; ++y)
    {
        for(int x = 0; x < integral_histogram.rows; ++x)
        {
            if(integral_histogram.at<double>(cv::Point2i(x,y)) < 0.0)
                FAIL();
        }
    }

    cout << "integral:" << endl << integral_histogram << endl << endl;

}