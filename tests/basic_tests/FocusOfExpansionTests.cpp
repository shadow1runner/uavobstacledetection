#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "gtest/gtest.h"
#include "../../FocusOfExpansionDto.h"
#include "../../FocusOfExpansionCalculator.h"


using namespace std;
using namespace hw;
using namespace cv;

TEST(focus_of_expansion, WindowSizeSmallerEqualOne_Ctor_ThrowsInvalidArgument) {
    ASSERT_THROW({
                      cv::Mat tmp(3,4,CV_8U);
                      hw::FocusOfExpansionCalculator foe(tmp, 1, -1);
                 }, std::invalid_argument);
}

TEST(focus_of_expansion, WindowSizeOk_Ctor_DoesNotThrow) {
    ASSERT_NO_THROW({
                      cv::Mat tmp(3,4,CV_8U);
                      hw::FocusOfExpansionCalculator foe(tmp, 2, -1);
                 });
}

TEST(focus_of_expansion, WindowSizeBiggerThanWidth_Ctor_ThrowsInvalidArgument) {
    ASSERT_THROW({
                      cv::Mat tmp(3,4,CV_8U);
                      hw::FocusOfExpansionCalculator foe(tmp, tmp.cols, -1);
                 }, std::invalid_argument);
}

TEST(focus_of_expansion, WindowSizeBiggerThanHeight_Ctor_ThrowsInvalidArgument) {
    ASSERT_THROW({
                      cv::Mat tmp(3,4,CV_8U);
                      hw::FocusOfExpansionCalculator foe(tmp, tmp.rows, -1);
                 }, std::invalid_argument);
}

TEST(focus_of_expansion, neighborhoodSizeOf2_determineFoe_yieldsMaxOf2Neighborhood) {
    cv::Mat after_colliding = (Mat_<double>(4,4) << 0.0,0.0,1.0,1.0,   0.0,0.0,1.0,1.0,   2.0,2.0,3.0,3.0,  2.0,2.0,3.0,3.0);

    cv::Mat integral_histogram;
    cv::integral(after_colliding, integral_histogram, CV_64F);
    cout << after_colliding << endl << endl;

    hw::FocusOfExpansionCalculator foe(after_colliding, 2, -1);

    auto measuredFoePos = foe.determineFoe()->getFoE();
    ASSERT_EQ(2, measuredFoePos.y);
    ASSERT_EQ(2, measuredFoePos.x);

    auto& actual = after_colliding.at<double>(measuredFoePos);
    ASSERT_EQ(3.0, actual);
}

TEST(focus_of_expansion, neighborhoodSizeOf3determineFoe_yieldsMaxOf3Neighborhood) {
    cv::Mat after_colliding = (Mat_<double>(4,6) << 0.0,0.0,0.0,1.0,1.0,1.0,   0.0,0.0,0.0,1.0,1.0,1.0,   2.0,2.0,2.0,3.0,3.0,3.0,  2.0,2.0,2.0,3.0,3.0,3.0);

    cv::Mat integral_histogram;
    cv::integral(after_colliding, integral_histogram, CV_64F);
    cout << after_colliding << endl << endl;
    cout << integral_histogram << endl;

    hw::FocusOfExpansionCalculator foe(after_colliding, 3, -1);

    auto measuredFoePos = foe.determineFoe()->getFoE();
    ASSERT_EQ(1, measuredFoePos.y); // notice -2 instead of -1, as windowSize/2 = 1.5 -> 1 as int
    ASSERT_EQ(3, measuredFoePos.x);

    auto& actual = after_colliding.at<double>(measuredFoePos);
    ASSERT_EQ(1.0, actual);
}

TEST(focus_of_expansion, validFoe_getNumberOfInliers_ReturnsProperNumberOfInliers) {
    cv::Mat after_colliding = (Mat_<double>(4,6) << 0.0,0.0,0.0,1.0,1.0,1.0,   0.0,0.0,0.0,1.0,1.0,1.0,   2.0,2.0,2.0,3.0,3.0,3.0,  2.0,2.0,2.0,3.0,3.0,3.0);

    cv::Mat integral_histogram;
    cv::integral(after_colliding, integral_histogram, CV_64F);
    cout << after_colliding << endl << endl;
    cout << integral_histogram << endl;

    hw::FocusOfExpansionCalculator foe(after_colliding, 3, -1);
    auto inliers = foe.determineFoe()->getNumberOfInliers();
    ASSERT_EQ(21, inliers);
}

TEST(focus_of_expansion, validFoe_getNumberOfParticles_ReturnsProperNumberOfParticles) {
    cv::Mat after_colliding = (Mat_<double>(4,6) << 0.0,0.0,0.0,1.0,1.0,1.0,   0.0,0.0,0.0,1.0,1.0,1.0,   2.0,2.0,2.0,3.0,3.0,3.0,  2.0,2.0,2.0,3.0,3.0,3.0);

    cv::Mat integral_histogram;
    cv::integral(after_colliding, integral_histogram, CV_64F);
    cout << after_colliding << endl << endl;
    cout << integral_histogram << endl;

    hw::FocusOfExpansionCalculator foe(after_colliding, 3, -1);
    auto particles = foe.determineFoe()->getNumberOfParticles();
    ASSERT_EQ(36, particles);
}

TEST(focus_of_expansion, validFoe_getInlierProportion_ReturnsProperNumberOfParticles) {
    cv::Mat after_colliding = (Mat_<double>(4,6) << 0.0,0.0,0.0,1.0,1.0,1.0,   0.0,0.0,0.0,1.0,1.0,1.0,   2.0,2.0,2.0,3.0,3.0,3.0,  2.0,2.0,2.0,3.0,3.0,3.0);

    cv::Mat integral_histogram;
    cv::integral(after_colliding, integral_histogram, CV_64F);
    cout << after_colliding << endl << endl;
    cout << integral_histogram << endl;

    hw::FocusOfExpansionCalculator foe(after_colliding, 3, -1);
    auto inlierPropotion = foe.determineFoe()->getInlierProportion();
    ASSERT_EQ(21/36.0, inlierPropotion);
}

TEST(focus_of_expansion, cpp_md_subsampling1_INTER_NEAREST_determinFoe_ReturnsSameFoeAsScalaDoes) {

    // fileName needs to be one of:
    //   cpp_go5_subsampling8_INTER_NEAREST.xml
    //   cpp2_go5_subsampling8_INTER_NEAREST.xml
    //   cpp_md_noSubsampling.xml
    //   cpp_md_subsampling1_INTER_NEAREST.xml
    // see FullColliderTestData/README.md for more information!
    // see actual files for expected values
    std::string fileName = "FullColliderTestData/cpp_md_subsampling1_INTER_NEAREST.xml";

    cv::FileStorage file(fileName, cv::FileStorage::READ);
    cv::Mat after_colliding;
    file["hist"] >> after_colliding;
    file.release();

    const int WINSIZE = 10;
    hw::FocusOfExpansionCalculator foe(after_colliding, WINSIZE, -1);
    auto foePt = foe.determineFoe();

    ASSERT_EQ(29, foePt->getFoE().x);
    ASSERT_EQ(14, foePt->getFoE().y);

    ASSERT_EQ(0.27527637891474716, foePt->getInlierProportion());

    ASSERT_EQ(36952, foePt->getNumberOfInliers());
    ASSERT_EQ(134236, foePt->getNumberOfParticles());
}

TEST(focus_of_expansion, cpp_md_noSubsampling_determinFoe_ReturnsSameFoeAsScalaDoes) {

    // fileName needs to be one of:
    //   cpp_go5_subsampling8_INTER_NEAREST.xml
    //   cpp2_go5_subsampling8_INTER_NEAREST.xml
    //   cpp_md_noSubsampling.xml
    //   cpp_md_subsampling1_INTER_NEAREST.xml
    // see FullColliderTestData/README.md for more information!
    // see actual files for expected values
    std::string fileName = "FullColliderTestData/cpp_md_noSubsampling.xml";

    cv::FileStorage file(fileName, cv::FileStorage::READ);
    cv::Mat after_colliding;
    file["hist"] >> after_colliding;
    file.release();

    const int WINSIZE = 10;
    hw::FocusOfExpansionCalculator foe(after_colliding, WINSIZE, -1);
    auto foePt = foe.determineFoe();

    ASSERT_EQ(46, foePt->getFoE().x);
    ASSERT_EQ(11, foePt->getFoE().y);

    ASSERT_EQ(0.07810259534084697, foePt->getInlierProportion());

    ASSERT_EQ(315156, foePt->getNumberOfInliers());
    ASSERT_EQ(4035154, foePt->getNumberOfParticles());
}

TEST(focus_of_expansion, cpp_go5_subsampling8_INTER_NEAREST_determinFoe_ReturnsSameFoeAsScalaDoes) {

    // fileName needs to be one of:
    //   cpp_go5_subsampling8_INTER_NEAREST.xml
    //   cpp2_go5_subsampling8_INTER_NEAREST.xml
    // see FullColliderTestData/README.md for more information!
    // see actual files for expected values
    std::string fileName = "FullColliderTestData/cpp_go5_subsampling8_INTER_NEAREST.xml";

    cv::FileStorage file(fileName, cv::FileStorage::READ);
    cv::Mat after_colliding;
    file["hist"] >> after_colliding;
    file.release();

    const int WINSIZE = 10;
    hw::FocusOfExpansionCalculator foe(after_colliding, WINSIZE, -1);
    auto foePt = foe.determineFoe();

    ASSERT_EQ(68, foePt->getFoE().x);
    ASSERT_EQ(27, foePt->getFoE().y);

    ASSERT_EQ(0.08163836443683074, foePt->getInlierProportion());

    ASSERT_EQ(606182, foePt->getNumberOfInliers());
    ASSERT_EQ(7425210, foePt->getNumberOfParticles());
}

TEST(focus_of_expansion, cpp2_go5_subsampling8_INTER_NEAREST_determinFoe_ReturnsSameFoeAsScalaDoes) {

    // fileName needs to be one of:
    //   cpp_go5_subsampling8_INTER_NEAREST.xml
    //   cpp2_go5_subsampling8_INTER_NEAREST.xml
    // see FullColliderTestData/README.md for more information!
    // see actual files for expected values
    std::string fileName = "FullColliderTestData/cpp2_go5_subsampling8_INTER_NEAREST.xml";

    cv::FileStorage file(fileName, cv::FileStorage::READ);
    cv::Mat after_colliding;
    file["hist"] >> after_colliding;
    file.release();

    const int WINSIZE = 10;
    hw::FocusOfExpansionCalculator foe(after_colliding, WINSIZE, -1);
    auto foePt = foe.determineFoe();

    ASSERT_EQ(68, foePt->getFoE().x);
    ASSERT_EQ(27, foePt->getFoE().y);

    ASSERT_EQ(0.08163836443683074, foePt->getInlierProportion());

    ASSERT_EQ(606182, foePt->getNumberOfInliers());
    ASSERT_EQ(7425210, foePt->getNumberOfParticles());
}