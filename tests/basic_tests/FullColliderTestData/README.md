### Rationale
These data was created by (1) the C++ side ('my' code) and (2) the Scala side (Sebastian's code). The rationale behind these files is to to an element-wise comparison of the collision histogram created by either of the programs. Hereby the following components get tested as well:

  * Frame Reading
    + Reading from file
    + subsampling (if at all)
    + converting from BGR to GRAYSCALE
    + equalizing histogram
  + Dense Optical Flow Generation using Farneback's method
  + Colliding
    + in order to make both approaches comparable, a quadratic approach was chosen; this approach takes ages for 640x480, namely `53 minutes for C++` and `39 minutes for Scala`:

```
#!c++
for (int y1 = 0; y1 < height; ++y1)
    {
        for (int x1 = 0; x1 < width; ++x1)
        {
            for (int y2 = 0; y2 < height; ++y2)
            {
                for (int x2 = 0; x2 < width; ++x2)
                {
                    auto fst = cv::Point2i(x1, y1);
                    auto snd = cv::Point2i(x2, y2);

                    const cv::Point2f& v1 = optical_flow.at<cv::Point2f>(fst);
                    const cv::Point2f& v2 = optical_flow.at<cv::Point2f>(snd);

                    auto denominator = v1.x * v2.y - v1.y * v2.x; // might be 0...
                    auto t = ((fst.y - snd.y) * v2.x - (fst.x - snd.x) * v2.y) / denominator; // ... if so, this is NaN
                    if(std::isnan(t)) {
                        continue; // ... division through zero, http://stackoverflow.com/a/4745343
                    }

                    auto inc = cv::Point2i((int)(fst.x + t * v1.x), (int)(fst.y + t * v1.y));

                    if (inc.x >= 0 && inc.x < width &&
                        inc.y >= 0 && inc.y < height) {
                        histogram.at<double>(inc) += 1;
                    }
                }
            }
//            std::cout << y1 << ", " << x1 << ": " << std::endl << histogram << std::endl;
//            std::cout << std::endl;
        }
    }
```

### Source
The interesting source files can be found in `RandomColiderTests.cpp`: `random_collider, quadratic_collision_cpp_file_generation`, `random_collider, compareCollidingHistogramFiles` and `FlowTest.scala`.

### Results

  + *_go5_noSubsampling - 640x360 - NOT identical
  + *_go5_subsampling4_INTER_NEAREST - 160x90 - NOT identical
  + *_go5_subsampling2_INTER_NEAREST - 320x180 - NOT identical
  + *_go5_subsampling8_INTER_NEAREST - 80x45 - IDENTICAL
  + *2_go5_subsampling8_INTER_NEAREST - 80x45 - (still) IDENTICAL
  + *_go5_subsampling10_INTER_NEAREST - 64x36 - NOT identical
  + *2_go5_subsampling2_INTER_NEAREST - 320x180 - (still) NOT identical
  + *_md_noSubsampling - 80x40 - IDENTICAL
  + *_md_subsampling1_INTER_NEAREST - 40x20 - IDENTICAL