//
// Created by Helmut on 12/21/2015.
//

#include "gtest/gtest.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "../../helper/Macros.h"

using namespace std;
using namespace cv;

TEST(opencv_test, normal_mat_to_integral_mat) {
    Mat m = (Mat_<double>(3,3) << 0, 1, 2, 3, 4, 5, 6, 7, 8);
    cout << endl;
    cout << m << endl;
    cout << m.type() << endl;


    Mat as_integral;
    integral(m, as_integral, CV_64F);
    cout << as_integral;
    for(int y = 0; y < m.rows; ++y)
    {
        for(int x = 0; x < m.cols; ++x)
        {
            auto& value = m.at<double>(cv::Point(x,y));
            assert(value==y*m.rows+x);
        }
    }
}

TEST(opencv_test, zero_initialization) {
    Mat matrix(10000, 20000, CV_64F, double(0));
    for(int y = 0; y < matrix.rows; ++y)
        for(int x = 0; x < matrix.cols; ++x) {
            auto value = matrix.at<double>(cv::Point(x, y));
            ASSERT(value == double(0), "(x,y): " << x << ", " << y);
        }
}

TEST(opencv_test, video_capture) {
    VideoCapture cap("res/rl/go5.mp4");
    if(!cap.isOpened())  // check if we succeeded
        FAIL();

    Mat edges;
    for (int i = 0; i < 2; ++i)
    {
        Mat frame;
        cap >> frame; // get a new frame from camera
        ostringstream os;
        os << "res/illustrator/go5_" << i << ".jpg";
        imwrite(os.str(), frame);
    }
    // the camera will be deinitialized automatically in VideoCapture destructor
    SUCCEED();
}