#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include "gtest/gtest.h"
#include "../../helper/HeatMap.h"


using namespace std;
using namespace cv;

void assert_rgb_equal(const Vec3b& assertee, const uchar& blue, const uchar& green, const uchar& red) {
    ASSERT_EQ(3, assertee.rows);
    ASSERT_EQ(1, assertee.cols);

    auto& assertee_blue = assertee.val[0];
    auto& assertee_green = assertee.val[1];
    auto& assertee_red = assertee.val[2];

    ASSERT_EQ(blue, assertee_blue);
    ASSERT_EQ(green, assertee_green);
    ASSERT_EQ(red, assertee_red);
}

TEST(heat_map, maxElement_getsMaxBGRValueAssigned) {
    cv::Mat hist = (Mat_<double>(1,2) << 2.0, 3.0);
    auto res = HeatMap::createHeatMap(hist);
    
    assert_rgb_equal(res.at<Vec3b>(Point(1,0)), 255, 255, 255);
}

TEST(heat_map, minElement_getsMinBGRValueAssigned) {
    cv::Mat hist = (Mat_<double>(1,2) << 2.0, 3.0);
    auto res = HeatMap::createHeatMap(hist);
    
    assert_rgb_equal(res.at<Vec3b>(Point(0,0)), 0, 0, 0);
}

TEST(heat_map, medianElement_getsMiddleBGRValueAssigned) {
    cv::Mat hist = (Mat_<double>(1,3) << 2.0, 3.0, 4.0);
    auto res = HeatMap::createHeatMap(hist);
    
    assert_rgb_equal(res.at<Vec3b>(Point(1,0)), 0, 127, 255);
}

TEST(heat_map, sameValues_getSameBGRValueAssigned) {
    cv::Mat hist = (Mat_<double>(1,4) << 2.0, 3.0, 3.0, 4.0);
    auto res = HeatMap::createHeatMap(hist);
    
    assert_rgb_equal(res.at<Vec3b>(Point(1,0)), 0, 127, 255);
    assert_rgb_equal(res.at<Vec3b>(Point(2,0)), 0, 127, 255);
}
