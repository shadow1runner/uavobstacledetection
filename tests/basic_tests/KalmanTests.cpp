#include "gtest/gtest.h"
#include "../../Kalman.h"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace cv;

TEST(kalman, givenTrajectory_followsScalaVersion) {
    // data gathered from SCALA using res/rendered/ahead.avi with a subsampling of 4 using INTER_NEAREST
    hw::Kalman kalman;

    kalman.predict();
    auto _0 = kalman.correct(cv::Point2f(51.0, 24.0), 18.02834922506926, 0.2);
    ASSERT_FLOAT_EQ(1.548596, _0.x);
    ASSERT_FLOAT_EQ(1.2523541, _0.y);

    kalman.predict();
    auto _1 = kalman.correct(cv::Point2f(56.0, 15.0), 23.111768003314637, 0.2);
    ASSERT_FLOAT_EQ(2.4699686, _1.x);
    ASSERT_FLOAT_EQ(1.4849782, _1.y);

    kalman.predict();
    auto _2 = kalman.correct(cv::Point2f(41.0, 28.0), 19.352886148179017, 0.2);
    ASSERT_FLOAT_EQ(3.6118739, _2.x);
    ASSERT_FLOAT_EQ(2.2707975, _2.y);

    kalman.predict();
    auto _3 = kalman.correct(cv::Point2f(21.0, 42.0), 28.03753918669542, 0.2);
    ASSERT_FLOAT_EQ(4.078732, _3.x);
    ASSERT_FLOAT_EQ(3.3374975, _3.y);

    kalman.predict();
    auto _4 = kalman.correct(cv::Point2f(21.0, 45.0), 17.301026638442572, 0.2);
    ASSERT_FLOAT_EQ(4.961965, _4.x);
    ASSERT_FLOAT_EQ(5.5121393, _4.y);

    kalman.predict();
    auto _5 = kalman.correct(cv::Point2f(54.0, 30.0), 17.911622113683602, 0.2);
    ASSERT_FLOAT_EQ(7.806697, _5.x);
    ASSERT_FLOAT_EQ(6.932698, _5.y);

    kalman.predict();
    auto _6 = kalman.correct(cv::Point2f(53.0, 33.0), 20.97680931293994, 0.2);
    ASSERT_FLOAT_EQ(10.327304, _6.x);
    ASSERT_FLOAT_EQ(8.386573, _6.y);

    kalman.predict();
    auto _7 = kalman.correct(cv::Point2f(54.0, 13.0), 18.881037043952453, 0.2);
    ASSERT_FLOAT_EQ(13.281715, _7.x);
    ASSERT_FLOAT_EQ(8.698667, _7.y);

    kalman.predict();
    auto _8 = kalman.correct(cv::Point2f(35.0, 23.0), 20.619287651998263, 0.2);
    ASSERT_FLOAT_EQ(14.733707, _8.x);
    ASSERT_FLOAT_EQ(9.654793, _8.y);

    kalman.predict();
    auto _9 = kalman.correct(cv::Point2f(49.0, 19.0), 18.470480819058725, 0.2);
    ASSERT_FLOAT_EQ(17.431595, _9.x);
    ASSERT_FLOAT_EQ(10.390569, _9.y); 
}

TEST(kalman, givenTrajectory_go5_followsScalaVersion) {
    // data gathered from SCALA using res/rendered/go5.mp4 with a subsampling of 4 using INTER_NEAREST
    hw::Kalman kalman;

    kalman.predict();
    auto _0 = kalman.correct(cv::Point2f(301.0, 62.0), 206.13345521023766, 0.2);
    ASSERT_FLOAT_EQ(1.2907914, _0.x);
    ASSERT_FLOAT_EQ(1.0591276, _0.y);

    kalman.predict();
    auto _1 = kalman.correct(cv::Point2f(175.0, 124.0), 232.97566909975666, 0.2);
    ASSERT_FLOAT_EQ(1.5883806, _1.x);
    ASSERT_FLOAT_EQ(1.2697432, _1.y);

    kalman.predict();
    auto _2 = kalman.correct(cv::Point2f(186.0, 120.0), 207.87298747763865, 0.2);
    ASSERT_FLOAT_EQ(2.118355, _2.x);
    ASSERT_FLOAT_EQ(1.6109582, _2.y);

    kalman.predict();
    auto _3 = kalman.correct(cv::Point2f(174.0, 148.0), 239.00000000000003, 0.2);
    ASSERT_FLOAT_EQ(2.689914, _3.x);
    ASSERT_FLOAT_EQ(2.0977464, _3.y);

    kalman.predict();
    auto _4 = kalman.correct(cv::Point2f(168.0, 118.0), 181.42677165354328, 0.2);
    ASSERT_FLOAT_EQ(3.591353, _4.x);
    ASSERT_FLOAT_EQ(2.7297635, _4.y);

    kalman.predict();
    auto _5 = kalman.correct(cv::Point2f(229.0, 34.0), 218.22736418511064, 0.2);
    ASSERT_FLOAT_EQ(4.813155, _5.x);
    ASSERT_FLOAT_EQ(2.8992603, _5.y);

    kalman.predict();
    auto _6 = kalman.correct(cv::Point2f(179.0, 86.0), 211.43097014925374, 0.2);
    ASSERT_FLOAT_EQ(5.945031, _6.x);
    ASSERT_FLOAT_EQ(3.4392538, _6.y);

    kalman.predict();
    auto _7 = kalman.correct(cv::Point2f(175.0, 126.0), 206.6140776699029, 0.2);
    ASSERT_FLOAT_EQ(7.2230787, _7.x);
    ASSERT_FLOAT_EQ(4.365807, _7.y);

    kalman.predict();
    auto _8 = kalman.correct(cv::Point2f(35.0, 109.0), 253.59090909090907, 0.2);
    ASSERT_FLOAT_EQ(7.414746, _8.x);
    ASSERT_FLOAT_EQ(5.087806, _8.y);

    kalman.predict();
    auto _9 = kalman.correct(cv::Point2f(179.0, 29.0), 204.85983263598325, 0.2);
    ASSERT_FLOAT_EQ(9.032478, _9.x);
    ASSERT_FLOAT_EQ(5.3132544, _9.y);

    kalman.predict();
    auto _10 = kalman.correct(cv::Point2f(168.0, 126.0), 236.13443396226413, 0.2);
    ASSERT_FLOAT_EQ(10.454552, _10.x);
    ASSERT_FLOAT_EQ(6.3928795, _10.y);

    kalman.predict();
    auto _11 = kalman.correct(cv::Point2f(175.0, 108.0), 173.92499999999998, 0.2);
    ASSERT_FLOAT_EQ(12.613526, _11.x);
    ASSERT_FLOAT_EQ(7.726051, _11.y);

    kalman.predict();
    auto _12 = kalman.correct(cv::Point2f(174.0, 114.0), 210.69230769230768, 0.2);
    ASSERT_FLOAT_EQ(14.492591, _12.x);
    ASSERT_FLOAT_EQ(8.963426, _12.y);

    kalman.predict();
    auto _13 = kalman.correct(cv::Point2f(182.0, 125.0), 285.0843023255814, 0.2);
    ASSERT_FLOAT_EQ(16.037128, _13.x);
    ASSERT_FLOAT_EQ(10.033365, _13.y);

    kalman.predict();
    auto _14 = kalman.correct(cv::Point2f(180.0, 118.0), 184.34615384615384, 0.2);
    ASSERT_FLOAT_EQ(18.515017, _14.x);
    ASSERT_FLOAT_EQ(11.665011, _14.y);

    kalman.predict();
    auto _15 = kalman.correct(cv::Point2f(230.0, 172.0), 166.54017857142858, 0.2);
    ASSERT_FLOAT_EQ(22.239988, _15.x);
    ASSERT_FLOAT_EQ(14.489057, _15.y);

    kalman.predict();
    auto _16 = kalman.correct(cv::Point2f(257.0, 153.0), 172.67840735068913, 0.2);
    ASSERT_FLOAT_EQ(26.42391, _16.x);
    ASSERT_FLOAT_EQ(16.957615, _16.y);

    kalman.predict();
    auto _17 = kalman.correct(cv::Point2f(292.0, 93.0), 165.62119205298015, 0.2);
    ASSERT_FLOAT_EQ(31.577442, _17.x);
    ASSERT_FLOAT_EQ(18.433226, _17.y);

    kalman.predict();
    auto _18 = kalman.correct(cv::Point2f(250.0, 148.0), 192.32428115015975, 0.2);
    ASSERT_FLOAT_EQ(35.38698, _18.x);
    ASSERT_FLOAT_EQ(20.693018, _18.y);

    kalman.predict();
    auto _19 = kalman.correct(cv::Point2f(175.0, 148.0), 225.5505617977528, 0.2);
    ASSERT_FLOAT_EQ(37.552948, _19.x);
    ASSERT_FLOAT_EQ(22.66807, _19.y);

    kalman.predict();
    auto _20 = kalman.correct(cv::Point2f(289.0, 83.0), 213.83068783068782, 0.2);
    ASSERT_FLOAT_EQ(41.828938, _20.x);
    ASSERT_FLOAT_EQ(23.694046, _20.y);

    kalman.predict();
    auto _21 = kalman.correct(cv::Point2f(175.0, 140.0), 191.83146067415728, 0.2);
    ASSERT_FLOAT_EQ(44.43992, _21.x);
    ASSERT_FLOAT_EQ(25.974365, _21.y);

    kalman.predict();
    auto _22 = kalman.correct(cv::Point2f(173.0, 101.0), 221.88930581613508, 0.2);
    ASSERT_FLOAT_EQ(46.69468, _22.x);
    ASSERT_FLOAT_EQ(27.290207, _22.y);

    kalman.predict();
    auto _23 = kalman.correct(cv::Point2f(172.0, 134.0), 221.2881773399015, 0.2);
    ASSERT_FLOAT_EQ(48.96951, _23.x);
    ASSERT_FLOAT_EQ(29.227451, _23.y);

    kalman.predict();
    auto _24 = kalman.correct(cv::Point2f(41.0, 127.0), 244.83380281690143, 0.2);
    ASSERT_FLOAT_EQ(48.834557, _24.x);
    ASSERT_FLOAT_EQ(30.883095, _24.y);

    kalman.predict();
    auto _25 = kalman.correct(cv::Point2f(174.0, 71.0), 245.31714285714287, 0.2);
    ASSERT_FLOAT_EQ(51.013332, _25.x);
    ASSERT_FLOAT_EQ(31.581415, _25.y);

    kalman.predict();
    auto _26 = kalman.correct(cv::Point2f(172.0, 124.0), 283.1860465116279, 0.2);
    ASSERT_FLOAT_EQ(52.893505, _26.x);
    ASSERT_FLOAT_EQ(33.017628, _26.y);

    kalman.predict();
    auto _27 = kalman.correct(cv::Point2f(187.0, 96.0), 254.21186440677968, 0.2);
    ASSERT_FLOAT_EQ(55.27746, _27.x);
    ASSERT_FLOAT_EQ(34.13724, _27.y);

    kalman.predict();
    auto _28 = kalman.correct(cv::Point2f(171.0, 77.0), 244.72513089005233, 0.2);
    ASSERT_FLOAT_EQ(57.466713, _28.x);
    ASSERT_FLOAT_EQ(34.948124, _28.y);

    kalman.predict();
    auto _29 = kalman.correct(cv::Point2f(302.0, 11.0), 259.704, 0.2);
    ASSERT_FLOAT_EQ(61.931293, _29.x);
    ASSERT_FLOAT_EQ(34.51089, _29.y);
}

// ============================================= KALMAN SAMPLES
static inline Point calcPoint(Point2f center, double R, double angle)
{
    return center + Point2f((float)cos(angle), (float)-sin(angle))*(float)R;
}

static void help()
{
    printf( "\nExample of c calls to OpenCV's Kalman filter.\n"
                    "   Tracking of rotating point.\n"
                    "   Rotation speed is constant.\n"
                    "   Both state and measurements vectors are 1D (a point angle),\n"
                    "   Measurement is the real point angle + gaussian noise.\n"
                    "   The real and the estimated points are connected with yellow line segment,\n"
                    "   the real and the measured points are connected with red line segment.\n"
                    "   (if Kalman filter works correctly,\n"
                    "    the yellow segment should be shorter than the red one).\n"
                    "\n"
                    "   Pressing any key (except ESC) will reset the tracking with a different speed.\n"
                    "   Pressing ESC will stop the program.\n"
    );
}

// plot points
#define drawCross( center, color, d )                                        \
                line( img, Point( center.x - d, center.y - d ),                          \
                             Point( center.x + d, center.y + d ), color, 1, LINE_AA, 0); \
                line( img, Point( center.x + d, center.y - d ),                          \
                             Point( center.x - d, center.y + d ), color, 1, LINE_AA, 0 )

// taken from https://github.com/Itseez/opencv/blob/master/samples/cpp/kalman.cpp
//TEST(kalman_samples, official_kalman_opencv_samples) {
//    help();
//
//    Mat img(500, 500, CV_8UC3);
//    cv::KalmanFilter KF(2, 1, 0);
//    Mat state(2, 1, CV_32F); /* (phi, delta_phi) */
//    Mat processNoise(2, 1, CV_32F);
//    Mat measurement = Mat::zeros(1, 1, CV_32F);
//    char code = (char)-1;
//
//    for(;;)
//    {
//        randn( state, Scalar::all(0), Scalar::all(0.1) );
//        KF.transitionMatrix = (Mat_<float>(2, 2) << 1, 1, 0, 1);
//
//        setIdentity(KF.measurementMatrix);
//        setIdentity(KF.processNoiseCov, Scalar::all(1e-5));
//        setIdentity(KF.measurementNoiseCov, Scalar::all(1e-1));
//        setIdentity(KF.errorCovPost, Scalar::all(1));
//
//        randn(KF.statePost, Scalar::all(0), Scalar::all(0.1));
//
//        for(;;)
//        {
//            Point2f center(img.cols*0.5f, img.rows*0.5f);
//            float R = img.cols/3.f;
//            double stateAngle = state.at<float>(0);
//            Point statePt = calcPoint(center, R, stateAngle);
//
//            Mat prediction = KF.predict();
//            double predictAngle = prediction.at<float>(0);
//            Point predictPt = calcPoint(center, R, predictAngle);
//
//            randn( measurement, Scalar::all(0), Scalar::all(KF.measurementNoiseCov.at<float>(0)));
//
//            // generate measurement
//            measurement += KF.measurementMatrix*state;
//
//            double measAngle = measurement.at<float>(0);
//            Point measPt = calcPoint(center, R, measAngle);
//
//            img = Scalar::all(0);
//            drawCross( statePt, Scalar(255,255,255), 3 );
//            drawCross( measPt, Scalar(0,0,255), 3 );
//            drawCross( predictPt, Scalar(0,255,0), 3 );
//            line( img, statePt, measPt, Scalar(0,0,255), 3, LINE_AA, 0 );
//            line( img, statePt, predictPt, Scalar(0,255,255), 3, LINE_AA, 0 );
//
//            if(theRNG().uniform(0,4) != 0)
//                KF.correct(measurement);
//
//            randn( processNoise, Scalar(0), Scalar::all(sqrt(KF.processNoiseCov.at<float>(0, 0))));
//            state = KF.transitionMatrix*state + processNoise;
//
//            imshow( "Kalman", img );
//            code = (char)waitKey(100);
//
//            if( code > 0 )
//                break;
//        }
//        if( code == 27 || code == 'q' || code == 'Q' )
//            break;
//    }
//}
//
//TEST(kalman_samples, kalman_cursor) {
//    KalmanFilter KF(4, 2, 0); // HW: state -> 4-vector (x; y; vx; vy;)??; measurement -> 2-vector (x,y)
//    POINT mousePos;
//    GetCursorPos(&mousePos);
//
//// intialization of KF...
//    KF.transitionMatrix = (Mat_<float>(4,4) <<1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1); // HW: TODO: incorporate time evolved between consecutive calls
//    Mat_<float> measurement(2,1);
//    measurement.setTo(Scalar(0));
//
////   KF.statePre.at<float>(0) = mousePos.x;
////   KF.statePre.at<float>(1) = mousePos.y;
////   KF.statePre.at<float>(2) = 0;
////   KF.statePre.at<float>(3) = 0;
//    // KF.statePost.at<float>(0) = mousePos.x;
//    // KF.statePost.at<float>(1) = mousePos.y;
//    // KF.statePost.at<float>(2) = 0;
//    // KF.statePost.at<float>(3) = 0;
//    setIdentity(KF.measurementMatrix);
//    setIdentity(KF.processNoiseCov, Scalar::all(1e-4));
//    setIdentity(KF.measurementNoiseCov, Scalar::all(10));
//    setIdentity(KF.errorCovPost, Scalar::all(.1));
//// Image to show mouse tracking
//    Mat img(600, 800, CV_8UC3);
//    vector<Point> mousev,kalmanv;
//    mousev.clear();
//    kalmanv.clear();
//
//    while(1)
//    {
//        // First predict, to update the internal statePre variable
//        Mat prediction = KF.predict();
//        Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
//
//        // Get mouse point
//        GetCursorPos(&mousePos);
//        measurement(0) = mousePos.x;
//        measurement(1) = mousePos.y;
//
//        // The update phase
//        Mat estimated = KF.correct(measurement);
//
//        Point statePt(estimated.at<float>(0),estimated.at<float>(1));
//        Point measPt(measurement(0),measurement(1));
//        // plot points
//        imshow("mouse kalman", img);
//        img = Scalar::all(0);
//
//        mousev.push_back(measPt);
//        kalmanv.push_back(statePt);
//        drawCross( statePt, Scalar(255,255,255), 5 );
//        drawCross( measPt, Scalar(0,0,255), 5 );
//
//        for (int i = 0; i < mousev.size()-1; i++)
//            line(img, mousev[i], mousev[i+1], Scalar(255,255,0), 1);
//
//        for (int i = 0; i < kalmanv.size()-1; i++)
//            line(img, kalmanv[i], kalmanv[i+1], Scalar(0,155,255), 1);
//
//        waitKey(10);
//    }
//}
//
//TEST(kalman_samples, raw_kalman) {
//
//    POINT mousePos;
//    GetCursorPos(&mousePos);
//// Image to show mouse tracking
//    Mat img(600, 800, CV_8UC3);
//    vector<Point> mousev,kalmanv;
//    mousev.clear();
//    kalmanv.clear();
//
//    cv::KalmanFilter kalman(2, 2, 0);
//
//    kalman.transitionMatrix = (cv::Mat_<float>(2, 2) << 1, 0, 0, 1); // identity
//
//    // >>>>>>>>>>>>>> Kalman
//
//    cv::setIdentity(kalman.measurementMatrix);
//
//    cv::setIdentity(kalman.processNoiseCov, cv::Scalar::all(1e-4)); // HW: ??
//
//    cv::setIdentity(kalman.measurementNoiseCov, cv::Scalar::all(1e-0)); // HW: ??
//
//    cv::setIdentity(kalman.errorCovPost, cv::Scalar::all(.1));
//
//    cv::randn(kalman.statePost, cv::Scalar::all(0), cv::Scalar::all(0.1)); // HW: ??
//
//    // <<<<<<<<<<<<<< Kalman
//
//    while(1)
//    {
//        auto predict = kalman.predict();
//        Point predictPt(predict.at<float>(0),predict.at<float>(1));
//        Mat_<float> measurement(2,1);
//        measurement.setTo(Scalar(0));
//
//        // Get mouse point
//        GetCursorPos(&mousePos);
//        measurement(0) = mousePos.x;
//        measurement(1) = mousePos.y;
//
//        // cv::setIdentity(kalman.measurementNoiseCov, cv::Scalar::all(inliers/covScale)); //!!!!
//
//        Mat estimated = kalman.correct(measurement);
//        Point statePt(estimated.at<float>(0),estimated.at<float>(1));
//        Point measPt(measurement(0),measurement(1));
//        // plot points
//        imshow("mouse kalman", img);
//        img = Scalar::all(0);
//
//        mousev.push_back(measPt);
//        kalmanv.push_back(statePt);
//        drawCross( statePt, Scalar(255,255,255), 5 );
//        drawCross( measPt, Scalar(0,0,255), 5 );
//
//        for (int i = 0; i < mousev.size()-1; i++)
//            line(img, mousev[i], mousev[i+1], Scalar(255,255,0), 1);
//
//        for (int i = 0; i < kalmanv.size()-1; i++)
//            line(img, kalmanv[i], kalmanv[i+1], Scalar(0,155,255), 1);
//
//        waitKey(10);
//    }
//
//}
//
//
//TEST(kalman_samples, my_kalman) {
//
//
//    POINT mousePos;
//    GetCursorPos(&mousePos);
//// Image to show mouse tracking
//    Mat img(600, 800, CV_8UC3);
//    vector<Point> mousev,kalmanv;
//    mousev.clear();
//    kalmanv.clear();
//
//    hw::Kalman kalman;
//
//    while(1)
//    {
//        auto predictPt = kalman.predict();
//
//        Mat_<float> measurement(2,1);
//        measurement.setTo(Scalar(0));
//
//        // Get mouse point
//        GetCursorPos(&mousePos);
//        cv::Point measPt(mousePos.x, mousePos.y);
//        auto statePt = kalman.correct(measPt, 1.0);
//
//        // plot points
//        imshow("mouse kalman", img);
//        img = Scalar::all(0);
//
//        mousev.push_back(measPt);
//        kalmanv.push_back(statePt);
//        drawCross( statePt, Scalar(255,255,255), 5 );
//        drawCross( measPt, Scalar(0,0,255), 5 );
//
//        for (int i = 0; i < mousev.size()-1; i++)
//            line(img, mousev[i], mousev[i+1], Scalar(255,255,0), 1);
//
//        for (int i = 0; i < kalmanv.size()-1; i++)
//            line(img, kalmanv[i], kalmanv[i+1], Scalar(0,155,255), 1);
//
//        waitKey(10);
//    }
//
//}
// ============================================= KALMAN SAMPLES - end