#include "gtest/gtest.h"
#include "../../CircularBuffer.h"
#include "../../RandomCollider.h"
#include "../../helper/HeatMap.h"
#include "../../helper/DrawHelper.h"
#include <iostream>
#include <opencv2/video/tracking.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;
using namespace hw;
using namespace cv;

TEST(random_colider, sample_coliding) {
    cv::Mat flow(10, 20, CV_32FC2);
    for (int y = 0; y < flow.rows; ++y)
    {
        for (int x = 0; x < flow.cols; ++x)
        {
            flow.at<Point2f>(Point(x,y)).x = x*3;
            flow.at<Point2f>(Point(x,y)).y = y*5;
        }
    }

    cout << flow << endl << endl;

    cv::Mat result;
    RandomCollider uut(flow, 200000, result);
    uut.run();

    cout << result;
    cout << endl;
}

TEST(random_colider, two_array_input) {
    cv::Mat flow(1,2,CV_32FC2);
    flow.at<Point2f>(Point(0,0)).x = 0.6; flow.at<Point2f>(Point(0,0)).y = 0.5;
    flow.at<Point2f>(Point(1,0)).x = -0.1; flow.at<Point2f>(Point(1,0)).y = -0.2;

    cout << flow << endl << endl;

    cv::Mat result;
    RandomCollider uut(flow, 200000, result);
    uut.run();

    cout << result;
    cout << endl;
}


//see basic_tests\FullColliderTestData\README.md for more information on this test!
TEST(full_collider, quadratic_collision_cpp_file_generation) {
    const int SUBSAMPLE_AMOUNT = 10;

    auto fst = cv::imread("res/illustrator/go5_0.jpg");
    cv::resize(fst, fst, cv::Size(fst.cols / (2 * SUBSAMPLE_AMOUNT), fst.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(fst, fst, CV_BGR2GRAY);
    cout << "fst:" << endl << fst << endl << endl;
    cv::equalizeHist(fst, fst);
   // cv::imwrite("res/illustrator/fst_gray_cpp.jpg", fst);

    auto snd = cv::imread("res/illustrator/go5_1.jpg");
    cv::resize(snd, snd, cv::Size(snd.cols / (2 * SUBSAMPLE_AMOUNT), snd.rows / (2 * SUBSAMPLE_AMOUNT)), 0, 0, cv::INTER_NEAREST);
    cv::cvtColor(snd, snd, CV_BGR2GRAY);
    cout << "snd:" << endl << snd << endl << endl;
    cv::equalizeHist(snd, snd);

//    cv::imwrite("res/illustrator/snd_gray_cpp.jpg", snd);

    // CPU
    cv::Mat flow;
    cv::calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

//    cout << "C++ - CPU\n=================" << endl;

//    cout << "snd:" << endl << snd << endl << endl;

    for(auto x = 0; x<5; ++x)
        for(auto y=0; y<5; ++y) {
            auto& pt = flow.at<cv::Point2f>(cv::Point(x,y));
            cout << "(" << x << ", " << y << "): ";
            cout << "(" << pt.x << ", " << pt.y << ")" << endl;
        }

//    DrawHelper::drawOpticalFlowMap(flow, snd, cv::Scalar(0,255,0), 10, 1.0, 1.0);
//    cv::imwrite("res/illustrator/flow_md.jpg", snd);

    cv::Mat hist;
    FullCollider uut(flow, hist);
    uut.run();

    cv::FileStorage file("cpp.xml", cv::FileStorage::WRITE);
    file << "hist" <<  hist;
    file << "flow" << flow;
    file.release();

//    cout << hist << endl << tmp << endl;


    std::vector<cv::Mat> stack;
    stack.push_back(HeatMap::createHeatMap(hist));
    stack.push_back(DrawHelper::visualiceFlowAsHsv(flow));

    cv::imshow("UUT", DrawHelper::makeColumnCanvas(stack, cv::Scalar(255,255,255)));
    cv::waitKey(30);
    cout << "Done" << endl;
//    cout << "hsv:" << endl << hsv << endl << endl;
}

//see basic_tests\FullColliderTestData\README.md for more information on this test!
TEST(full_collider, compareCollidingHistogramFiles) {
    cv::FileStorage file("FullColliderTestData/cpp_md_noSubsampling.xml", cv::FileStorage::READ);
    cv::Mat cpp;
    file["hist"] >> cpp;
    file.release();

    cv::FileStorage file2("FullColliderTestData/scala_md_noSubsampling.xml", cv::FileStorage::READ);
    cv::Mat scala;
    file2["hist"] >> scala;
    file2.release();

    ASSERT_EQ(cpp.rows, scala.cols);
    ASSERT_EQ(cpp.cols, scala.rows);

    // cv::compare(cpp, scala);

    // Get a matrix with non-zero values at points where the
    // two matrices have different values
    cv::Mat diff = cpp.t() != scala;
    // Equal if no elements disagree
    bool eq = cv::countNonZero(diff) == 0;

    ASSERT_TRUE(eq);
}

//see basic_tests\FullColliderTestData\README.md for more information on this test!
TEST(full_collider, compareFlowFiles) {
    cv::FileStorage file("FullColliderTestData/tmp/cpp.xml", cv::FileStorage::READ);
    cv::Mat cpp;
    file["flow"] >> cpp;
    file.release();

    cv::FileStorage file2("FullColliderTestData/tmp/scala_flow.xml", cv::FileStorage::READ);
    cv::Mat scala;
    file2["flow"] >> scala;
    file2.release();

    ASSERT_EQ(cpp.rows, scala.rows);
    ASSERT_EQ(cpp.cols, scala.cols);

    for (int y = 0; y < cpp.rows; ++y)
    {
        for (int x = 0; x < cpp.cols; ++x)
        {
            cout << "(" << x << ", " << y << ")" << endl;
            ASSERT_FLOAT_EQ(cpp.at<float>(x,y), scala.at<float>(x,y));
        }
    }

    // cv::compare(cpp, scala);

    // Get a matrix with non-zero values at points where the
    // two matrices have different values
    cv::Mat diff = cpp != scala;
    // Equal if no elements disagree
    bool eq = cv::countNonZero(diff) == 0;

    ASSERT_TRUE(eq);
}