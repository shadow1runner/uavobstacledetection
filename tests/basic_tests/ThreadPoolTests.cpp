//#include "gtest/gtest.h"
//#include <boost/bind.hpp>
//#include <iostream>
//#include <functional>
//#include <typeinfo>
//#include "../../ThreadPool.h"
//#include <unistd.h> // sleep
//
//using namespace std;
//using namespace hw;
//
//int sleep_print(int seconds) {
//   std::cout << "going to sleep (" << seconds << ")" << std::endl;
//   sleep(seconds);
//   std::cout << "wake up (" << seconds << ")" << std::endl;
//   return 0;
//}
//
//class TempWrapper
//{
//public:
//    int sleep_print(int seconds) {
//       std::cout << "going to sleep (" << seconds << ")" << std::endl;
//       sleep(seconds);
//       std::cout << "wake up (" << seconds << ")" << std::endl;
//       return 0;
//    }
//};
//
//TEST(thread_pool, sample_queueing) {
//    hw::ThreadPool<int> threadPool;
//
//    threadPool.push_job(boost::bind(&sleep_print, 3));
//
//    TempWrapper tempWrapper;
//    threadPool.push_job(boost::bind(TempWrapper::sleep_print, tempWrapper, 3));
//    threadPool.push_job(boost::bind(TempWrapper::sleep_print, tempWrapper, 4));
//    threadPool.push_job(boost::bind(TempWrapper::sleep_print, tempWrapper, 5));
//
//    threadPool.waitForAll();
//}
//
