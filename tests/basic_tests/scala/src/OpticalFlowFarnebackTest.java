import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs; // imread since opencv 3.0, see http://stackoverflow.com/a/25943085
import org.opencv.imgproc.Imgproc;
import org.opencv.video.Video;

/**
 * Created by Helmut on 12/31/2015.
 */
public class OpticalFlowFarnebackTest {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }

    public static void main(String[] args) {
        Mat tmp = Imgcodecs.imread("../../../res/illustrator/fst.jpg");
        Mat fst = new Mat();
        Imgproc.cvtColor(tmp, fst, Imgproc.COLOR_BGR2GRAY);
//        Imgcodecs.imwrite("../../../res/illustrator/fst_gray_java.jpg", fst);

        tmp = Imgcodecs.imread("../../../res/illustrator/snd.jpg");
        Mat snd = new Mat();
        Imgproc.cvtColor(tmp, snd, Imgproc.COLOR_BGR2GRAY);
//        Imgcodecs.imwrite("../../../res/illustrator/snd_gray_java.jpg", snd);

        Mat flow = new Mat();
        Video.calcOpticalFlowFarneback(fst, snd, flow, 0.5, 3, 10, 10, 7, 1.5, 0);

        System.out.println("Java\n=================");
        System.out.println("fst:\n" + fst.dump() + "\n");
        System.out.println("snd:\n" + snd.dump() + "\n");

        for(int x = 0; x<snd.cols(); ++x)
            for(int y=0; y<snd.rows(); ++y) {
                double[] pt = flow.get(y, x);
                System.out.print("(" + x + ", " + y + "): ");
                System.out.println("(" + pt[0] + ", " + pt[1] + ")");
            }
    }
}


//package hw
//
//        import java.io.File
//        import java.util.concurrent.ThreadLocalRandom
//        import javax.imageio.ImageIO
//
//        import com.googlecode.javacv.cpp.opencv_core._
//        import com.googlecode.javacv.cpp.opencv_highgui._
//        import com.googlecode.javacv.cpp.opencv_video._
//        import pi23._
//
//        import scalacv.Preamble._
//        import scalacv.{BufferedFrameGrabber, MultipleFOE, _}
//
//        object FlowTest extends OpenCvApp {
//        def calcOpticalFlowFarneback(prevColor: RichIplImage, color: RichIplImage) = {
//        val pyr_scale = 0.5d // 0.5 means a classical pyramid, where each next layer is twice smaller than the previous one
//        val levels = 3 // Number of pyramid levels
//        val winsize = 10
//        val iterations = 10 // Iterations at every pyramid level
//        val poly_n = 7 // larger values mean that the image will be approximated with smoother surfaces, yielding more robust algorithm and more blurred motion field, typically poly_n =5 or 7.
//        val poly_sigma = 1.5d // you can set poly_sigma=1.1, for poly_n=7, a good value would be poly_sigma=1.5
//        val flags = 0//OPTFLOW_FARNEBACK_GAUSSIAN
//        val flow: IplImage = IplImage.create(prevColor.size, IPL_DEPTH_32F, 2)
////    cvCalcOpticalFlowFarneback(prevColor.grayscale, color.grayscale, flow, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags)
//        cvCalcOpticalFlowFarneback(prevColor.grayscale, color.grayscale, flow, 0.5, 3, 10, 10, 7, 1.5, 0)
//        Flow(flow)
//        }
//
//        var img =  ImageIO.read(new File("hw/fst.jpg") )
//        var fst = IplImage.createFrom(img)
//
//        img =  ImageIO.read(new File("hw/snd.jpg") )
//        var snd = IplImage.createFrom(img)
//
//        println(CV_VERSION)
//        println("Scala\n=================")
//        println("fst:")
//        println(fst.grayscale.asCvMat())
//        println("snd:")
//        println(snd.grayscale.asCvMat())
//        println("")
//
//
//        var flow = calcOpticalFlowFarneback(fst, snd)
//        for( x <- (0 until snd.width())) {
//        for( y <- (0 until snd.height())) {
//        val v1Index = flow.calcIndex(x, y)
//
//        val xf = flow.buffer.get(v1Index)
//        val yf = flow.buffer.get(v1Index + 1)
//        println(f"($x,$y): ($xf, $yf)")
//        }
//        }
//
////  cvSaveImage("res/snd.jpg", snd)
//
//        FOE.getParticlesHistogramWithRotation(flow, 200000, 0)
//        }
